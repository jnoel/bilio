# coding: utf-8

class DialTva < Gtk::Dialog

	attr_reader :designation, :taux, :np
	MAX_SPIN = 999999
	
	def initialize window, id=nil
		
		titre = (id.nil? ? "Ajouter un nouveau taux de TVA" : "Modifier un taux de TVA")
		
		super(:title => titre, :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])
		
		table = Gtk::Table.new 2, 2, false
		
		label_designation = Gtk::Alignment.new 0, 0.5, 0, 0
		label_designation.add Gtk::Label.new("Désignation :")
		table.attach( label_designation, 0, 1, 0, 1, :fill, :fill, 2, 2 )
		@designation = Gtk::Entry.new 
		table.attach( @designation, 1, 2, 0, 1, :fill, :fill, 2, 2 )
		
		label_taux = Gtk::Alignment.new 0, 0.5, 0, 0
		label_taux.add Gtk::Label.new("Taux (en %) :")
		table.attach( label_taux, 0, 1, 1, 2, :fill, :fill, 2, 2 )
		@taux = Gtk::SpinButton.new 0, MAX_SPIN, 1
		@taux.digits = 2
		table.attach( @taux, 1, 2, 1, 2, :fill, :fill, 2, 2 )
		
		label_np = Gtk::Alignment.new 0, 0.5, 0, 0
		label_np.add Gtk::Label.new("Non perçue :")
		table.attach( label_np, 0, 1, 2, 3, :fill, :fill, 2, 2 )
		@np = Gtk::CheckButton.new
		table.attach( @np, 1, 2, 2, 3, :fill, :fill, 2, 2 )
		
		if id
			tva = Tva.find(id)
			@designation.text = tva.designation
			@taux.value = tva.taux
			@np.active = tva.np
		else
			@np.active = false
		end
		
		self.child.pack_start table, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all
	
	end

end		

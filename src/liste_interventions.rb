# coding: utf-8

class ListeInterventions_box < Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		
		add_new = Gtk::Button.new
		hboxajout = Gtk::Box.new :horizontal, 2
		hboxajout.add Gtk::Image.new( :file => "./resources/icons/add.png" )
		@label_button = Gtk::Label.new "Nouvelle"
		hboxajout.add @label_button
		add_new.add hboxajout
		
		print = Gtk::Button.new
		hbox_print = Gtk::Box.new :horizontal, 2
		hbox_print.add Gtk::Image.new( :file => "./resources/icons/print.png" )
		label_print = Gtk::Label.new "Imprimer"
		hbox_print.add label_print
		print.add hbox_print
		
		hb = Gtk::Box.new :horizontal, 2
		#hb.add print
		hb.add add_new
		
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add hb
		
		table_pieds = Gtk::Table.new 1, 3, false
		@compteur = Gtk::Label.new
		@total_ht = Gtk::Label.new
		@total_ttc = Gtk::Label.new
		table_pieds.attach( @compteur, 0, 1, 0, 1, :fill, :fill, 5, 5 )
		table_pieds.attach( @total_ht, 1, 2, 0, 1, :fill, :fill, 5, 5 )
		table_pieds.attach( @total_ttc, 2, 3, 0, 1, :fill, :fill, 5, 5 )
		
		hbox = Gtk::Box.new :horizontal, 3
		@search = Gtk::Entry.new
		@search.primary_icon_stock = Gtk::Stock::FIND
		@search.secondary_icon_stock = Gtk::Stock::CLEAR
		hbox.pack_start @search, :expand => true, :fill => true, :padding => 3
		
		hbox.pack_start align_button, :expand => false, :fill => false, :padding => 3
		
		vbox.pack_start hbox, :expand => false, :fill => false, :padding => 3
		vbox.pack_start tableau_documents, :expand => true, :fill => true, :padding => 3
		vbox.pack_start table_pieds, :expand => false, :fill => false, :padding => 3
		@frame = Gtk::Frame.new
		@frame.label = "Interventions"
		@frame.add vbox
		
		add_new.signal_connect( "clicked" ) {
			@window.intervention.refresh 0
			@window.affiche @window.intervention
			@window.intervention.focus
		}
		
		print.signal_connect( "clicked" ) {
			imprimer
		}
		
		@search.signal_connect('activate') {
			#if !@search.text.empty? 
			#	refresh_docs @type_document, @search.text
			#else
			#	refresh_docs @type_document, nil
			#end
			#@filtre.active = -1
		}
		
		@search.signal_connect('icon-press') { |widget, pos, event|
			if !@search.text.empty?
				@search.text = ""
				refresh_docs @type_document, nil
			end
		}
		
		return @frame
	
	end
	
	def tableau_documents
		
		                                 #id   client  date_document date_tri duree utilisateur
		@list_store = Gtk::ListStore.new(Integer, String, String, String, Float, String)
		@view = Gtk::TreeView.new(@list_store)
		
		@view.selection.mode = :multiple
		
		@view.signal_connect ("row-activated") { |view, path, column|
			i = nil
			@view.selection.selected_each {|model, path, iter| i=iter}
			unless i.nil?
				@window.intervention.refresh @view.model.get_value(i, 0)
				@window.affiche @window.intervention		 
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Tiers", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Date du document", renderer_right, :text => 2)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Durée", renderer_right, :text => 4)
		col.sort_column_id = 4
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Utilisateur", renderer_right, :text => 5)
		col.sort_column_id = 5
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
		scroll.set_policy(:automatic, :automatic)
		scroll.add @view
	
	end
	
	def refresh term=nil, where=nil
	
		refresh_interventions term, where
		
	end
	
	def refresh_interventions term=nil, where=nil
		interv = Intervention.includes(:tiers, :utilisateur).order("date DESC")
		interv = interv.where("tiers.nom ILIKE ?", "%#{term}%") if term
		interv = interv.where(where) if where
		
		remplir interv
	end
	
	def remplir interv
		
		@list_store.clear
		
		interv.each do |d|
			iter = @list_store.append
			iter[0] = d.id
			iter[1] = d.tiers.nom
			iter[2] = d.date.strftime("%d/%m/%Y")
			iter[3] = d.date.to_s
			iter[4] = d.duree
			iter[5] = d.utilisateur.firstname
		end
		
		@compteur.text = "Nombre d'éléments dans la liste : #{interv.count}"
			
	end
	
	def imprimer
	
		ids = []
		@view.selection.selected_each { |model, path, iter|
			ids << @view.model.get_value(iter, 0)
		}
		
		if ids.count>0
			chemin = @window.config_db.chemin_temp
			f = Edition.new ids, chemin
			if RUBY_PLATFORM.include?("linux") then 
				system "xdg-open","#{f.fichier}"
			else
				if RUBY_PLATFORM.include?("mingw") then 
					system "start","#{f.fichier}"
				else
				
				end
			end
		end	
	end
	
end

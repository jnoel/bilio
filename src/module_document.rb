# coding: utf-8

module DocumentController
	
	def DocumentController.supprime ids
		ids.each do |id|
			doc = Document.find(id)
			supprime_ligne doc.id, doc.documenttype_id
			doc.suppr = true
			doc.save
		end
		p "Supression du document #{ids}"
	end
	
	def DocumentController.supprime_ligne doc_id, doc_type=nil
		if doc_type.nil?
			doc_type = Document.find(id).documenttype_id
		end
		lignes = Documentligne.where(:document_id => doc_id)
		lignes.each do |l|
			
			# Si on est sur une facture ou un avoir on modifie le stock article
			if [4,5,8].include?(doc_type)
				a = Article.find(l.article_id)
				unless a.service
					qtite = l.qtite
					qtite *= -1 unless doc_type==4 # Si facture fournisseur ou avoir, on décrémente stock
					ArticleController.modifie_stock l.article_id, qtite, lot=l.lot
				end
			end
			l.suppr = true
			l.save
		end
		Documentligne.where(:document_id => doc_id).update_all(:suppr => true)
	end
	
	def DocumentController.modifiable? doc_id
		
	end

end		

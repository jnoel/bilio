# coding: utf-8

class DialCommentaire < Gtk::Dialog

	attr_reader :memo
	
	def initialize window, comm=""
		
		super(:title => "Commentaire", :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])
		
		set_default_size 500, 500
		
		@memo = Gtk::TextView.new
		@memo.buffer.text = comm.nil? ? "" : comm
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	scroll.add @memo
  	
  	frame = Gtk::Frame.new
		frame.label = "Commentaire"
		frame.add scroll
		
		self.child.pack_start frame, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all

	end

end

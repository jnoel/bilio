# coding: utf-8

class RapportExercices < Gtk::TreeView

  attr_reader :list_store
  
  def initialize adapter
       
  	super( Gtk::ListStore.new(String, String, String, String, String, String, String, String, String, String, String, String, String) )
	
		@adapter = adapter
	
		# list_store (mois, depenses_n-3, recettes_n-3, benefices_n-3, depenses_n-2, recettes_n-2, benefices_n-2, depenses_n-1, recettes_n-1, benefices_n-1, depenses_n, recettes_n, benefices_n)
		
		@list_store = self.model
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_green = Gtk::CellRendererText.new
		renderer_green.background = "#e7ffe7"
		renderer_green.xalign = 1
		renderer_green.yalign = 0
		
		# Create a renderer with the background property set
		renderer_red = Gtk::CellRendererText.new
		renderer_red.background = "#ffe7e7"
		renderer_red.xalign = 1
		renderer_red.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right_bold = Gtk::CellRendererText.new
		renderer_right_bold.xalign = 1
		renderer_right_bold.yalign = 0
		renderer_right_bold.background = "#eee"
		
		col = Gtk::TreeViewColumn.new("Mois", renderer_left, :text => 0)
		col.expand = true
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Dép. n-3", renderer_red, :text => 1)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Rec. n-3", renderer_green, :text => 2)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Bén. n-3", renderer_right_bold, :text => 3)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Dép. n-2", renderer_red, :text => 4)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Rec. n-2", renderer_green, :text => 5)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Bén. n-2", renderer_right_bold, :text => 6)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Dép. n-1", renderer_red, :text => 7)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Rec. n-1", renderer_green, :text => 8)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Bén. n-1", renderer_right_bold, :text => 9)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Dép. n", renderer_red, :text => 10)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Rec. n", renderer_green, :text => 11)
		self.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Bén. n", renderer_right_bold, :text => 12)
		self.append_column(col)
		
		refresh
	
	end
	
	def refresh creance=true, an=nil
		
		mois = ["Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Décembre"]
		
		if an.nil?
			today = Date.today		
			annee = today.year
		else
			annee = an
		end
		
		@list_store.clear
		
		d0 = 0
		r0 = 0
		d1 = 0
		r1 = 0
		d2 = 0
		r2 = 0
		d3 = 0
		r3 = 0
		
		date = creance ? "date_document" : "date_paiement"
		montant = creance ? "montant_ttc" : "montant"
		from = creance ? "documents" : "paiements"
		where = creance ? "documenttype_id=4" : "fournisseur=false"
		where2 = creance ? "documenttype_id=8" : "fournisseur=true"
		
		(1..12).to_a.each {|m|
		
			if @adapter.eql?("postgresql")
				
				req = 	"	SELECT EXTRACT(YEAR FROM SUB.#{date}) AS annee, SUM(SUB.recette) AS recette, SUM(SUB.depense) AS depense
							FROM 
							(
								SELECT #{date}, #{montant} as recette, 0 as depense
								FROM #{from}
								WHERE #{where}
								AND EXTRACT(MONTH FROM #{date})=#{m}
								AND suppr=false
								UNION ALL
								SELECT #{date}, 0 as recette, #{montant} as depense
								FROM #{from}
								WHERE #{where2}
								AND EXTRACT(MONTH FROM #{date})=#{m}
								AND suppr=false
							) SUB
							GROUP BY annee
						"
			elsif @adapter.eql?("sqlite3") #PARTIE NON CORRECTE, A REFAIRE !!!
				req = 	"	SELECT strftime('%Y', SUB.date_document) AS annee, SUM(SUB.recette) AS recette, SUM(SUB.depense) AS depense
							FROM 
							(
								SELECT date_document, montant_ttc as recette, 0 as depense
								FROM documents
								WHERE documenttype_id=4
								AND strftime('%m',date_document)=#{m}
								AND suppr=false
								UNION ALL
								SELECT date_document, 0 as recette, montant_ttc as depense
								FROM documents
								WHERE documenttype_id=8
								AND strftime('%m',date_document)=#{m}
								AND suppr=false
							) SUB
							GROUP BY annee
						"
			end

			res = Document.find_by_sql(req)
		
			if res then
				iter = @list_store.append
				iter[0] = mois[m-1]
				res.each {|row|
					diff = annee - row["annee"].to_i
					case diff
						when 0
							iter[10] = "%.2f" % row["depense"] if row["depense"].to_f>0
							iter[11] = "%.2f" % row["recette"] if row["recette"].to_f>0
							iter[12] = "%.2f" % (row["recette"].to_f - row["depense"].to_f)
							d0 += row["depense"].to_f
							r0 += row["recette"].to_f
						when 1
							iter[7] = "%.2f" % row["depense"] if row["depense"].to_f>0
							iter[8] = "%.2f" % row["recette"] if row["recette"].to_f>0
							iter[9] = "%.2f" % (row["recette"].to_f - row["depense"].to_f)
							d1 += row["depense"].to_f
							r1 += row["recette"].to_f
						when 2
							iter[4] = "%.2f" % row["depense"] if row["depense"].to_f>0
							iter[5] = "%.2f" % row["recette"] if row["recette"].to_f>0
							iter[6] = "%.2f" % (row["recette"].to_f - row["depense"].to_f)
							d2 += row["depense"].to_f
							r2 += row["recette"].to_f
						when 3
							iter[1] = "%.2f" % row["depense"] if row["depense"].to_f>0
							iter[2] = "%.2f" % row["recette"] if row["recette"].to_f>0
							iter[3] = "%.2f" % (row["recette"].to_f - row["depense"].to_f)
							d3 += row["depense"].to_f
							r3 += row["recette"].to_f
					end
				}
			else
				@window.message_erreur res.error
				break
			end
			
		}
		
		iter = @list_store.append
		iter[0] = "Total"
		iter[1] = "%.2f" % d3
		iter[2] = "%.2f" % r3
		iter[3] = "%.2f" % (r3-d3)
		iter[4] = "%.2f" % d2
		iter[5] = "%.2f" % r2
		iter[6] = "%.2f" % (r2-d2)
		iter[7] = "%.2f" % d1
		iter[8] = "%.2f" % r1
		iter[9] = "%.2f" % (r1-d1)
		iter[10] = "%.2f" % d0
		iter[11] = "%.2f" % r0
		iter[12] = "%.2f" % (r0-d0)
		
	end
	
	def get_series
	
		series = []
		@list_store.each {|model, path, iter|
			m = @list_store.get_iter(path)
			series << [m[10].to_f,m[11].to_f,m[12].to_f]
		}
		
		series.pop
		series
	
	end
	
end

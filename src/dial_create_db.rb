# coding: utf-8

class DialCreateDb < Gtk::Dialog
	
	def initialize window, connexion_id
		
		super(:title => "Migration de la base de données", :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::OK, :ok ]])
		
		self.set_response_sensitive Gtk::ResponseType::OK, false 
		set_default_size 500, 500
		
		@memo = Gtk::TextView.new
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	scroll.add @memo
  	
  	frame = Gtk::Frame.new
		frame.label = "Mise à jour de la base de données"
		frame.add scroll
		
		self.child.pack_start frame, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all
		
		@memo.buffer.insert(@memo.buffer.start_iter,"=============================================\n")
		@memo.buffer.insert(@memo.buffer.end_iter,"Début de la migration... Veuillez patienter\n")
		@memo.buffer.insert(@memo.buffer.end_iter,"==============================================\n")
		
		migrate connexion_id

	end
	
	def migrate connexion_id
		Thread.new {
			begin
				if @bundle_bool
					@memo.buffer.insert(@memo.buffer.end_iter,`bundle exec rake migrate[#{connexion_id}]`)
				else
					if RUBY_PLATFORM.include?("mingw") then 
						@memo.buffer.insert(@memo.buffer.end_iter,`..\\vendor\\bin\\rake migrate[#{connexion_id}]`)
					else
						@memo.buffer.insert(@memo.buffer.end_iter,`rake migrate[#{connexion_id}]`)
					end
				end
				@memo.buffer.insert(@memo.buffer.end_iter,"\n=====================\n")
				@memo.buffer.insert(@memo.buffer.end_iter,"Fin de la migration\n")
				@memo.buffer.insert(@memo.buffer.end_iter,"=====================")
				self.set_response_sensitive Gtk::ResponseType::OK, true 
			rescue
				memo.buffer.insert(@memo.buffer.end_iter,"\nErreur lors de la préparation de la base de données !!\n")
			end
		}
	end

end

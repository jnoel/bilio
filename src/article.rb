# coding: utf-8

class Article_box < Gtk::Box

	attr_reader :article
	MAX_SPIN = 999999

	def initialize window, dial=false
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@id = 0
		
		@window = window
		@login = window.login
		@dial = dial
		
		@activer_combo = false
		@graph = GraphBar.new absolu=true
		
		@info1 = Gtk::Entry.new
		@info2 = Gtk::Entry.new
		@description = Gtk::TextView.new
		@datecreation = Gtk::Calendar.new
		@valider = Gtk::Button.new
		hboxvalider = Gtk::Box.new :horizontal, 2
		hboxvalider.add Gtk::Image.new( :file => "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new "Valider"
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::Box.new :horizontal, 2
		hboxannuler.add Gtk::Image.new( :file => "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new "Annuler"
		@annuler.add hboxannuler
		@supprimer = Gtk::Button.new
		hboxsupprimer = Gtk::Box.new :horizontal, 2
		hboxsupprimer.add Gtk::Image.new( :file => "./resources/icons/remove.png" )
		hboxsupprimer.add Gtk::Label.new "Supprimer"
		@supprimer.add hboxsupprimer
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
		@supprimer.signal_connect( "clicked" ) { supprimer }
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		@frame = Gtk::Frame.new
		@frame.label = "Nouvel article"
		vbox.pack_start @frame, :expand => true, :fill => true, :padding => 3 
		
		vbox_corps = Gtk::Box.new :vertical, 3
		@frame.add vbox_corps
		vbox_corps.pack_start en_tete, :expand => true, :fill => true, :padding => 3 	
		
		hbox3 = Gtk::Box.new :horizontal, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start @annuler, :expand => true, :fill => true, :padding => 3
		hbox3.pack_start @valider, :expand => true, :fill => true, :padding => 3
		align1.add hbox3
		
		hbox1 = Gtk::Box.new :horizontal, 2
		hbox1.pack_start @supprimer, :expand => false, :fill => false, :padding => 3
		hbox1.pack_start align1, :expand => true, :fill => true, :padding => 3
		
		vbox.pack_start(hbox1, :expand => false, :fill => false, :padding => 3) unless @dial
		
		return vbox
	
	end
	
	def en_tete
	
		# Objets
		@code = Gtk::Entry.new
		@groupe_model = Gtk::TreeStore.new(Integer, String)
		@groupe = Gtk::ComboBox.new :model => @groupe_model
		renderer_left = Gtk::CellRendererText.new
		@rayon_model = Gtk::TreeStore.new(Integer, String)
		@rayon_cb = Gtk::ComboBox.new :model => @rayon_model
		@designation = Gtk::Entry.new		
		@description = Gtk::TextView.new	
		@image = Gtk::Image.new
		
		# Propriétés
		@description.wrap_mode = :word
		@code.width_chars = 8
		renderer_left.xalign = 0
		
		# Agencement		
		vbox = Gtk::Box.new :vertical, 3
		
		table = Gtk::Table.new 3, 9, false
		
		table.attach( Gtk::Label.new("Code :"), 0, 1, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @code, 1, 2, 0, 1, :fill, :fill, 5, 5 )
		
		@groupe.pack_start renderer_left, true
		@groupe.add_attribute renderer_left, "text", 1
		table.attach( Gtk::Label.new("Groupe :"), 2, 3, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @groupe, 3, 4, 0, 1, :fill, :fill, 5, 5 )
		
		@rayon_cb.pack_start renderer_left, true
		@rayon_cb.add_attribute renderer_left, "text", 1
		table.attach( Gtk::Label.new("Rayonnage :"), 4, 5, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @rayon_cb, 5, 6, 0, 1, :fill, :fill, 5, 5 )
		
		table.attach( Gtk::Label.new("Désignation :"), 0, 1, 1, 2, :fill, :fill, 5, 5 )
		table.attach( @designation, 1, 8, 1, 2, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )		
		
		vbox.pack_start table, :expand => false, :fill => false, :padding => 3
		
		hbox = Gtk::Box.new :horizontal, 23
		vbox_note = Gtk::Box.new :vertical, 3
		vbox_tarifs = Gtk::Box.new :vertical, 3
		hbox.pack_start vbox_tarifs, :expand => true, :fill => true, :padding => 3
		hbox.pack_start vbox_note, :expand => true, :fill => true, :padding => 3
		
		align_tarifs = Gtk::Alignment.new 0, 0, 0, 0
		align_tarifs.add Gtk::Label.new( "Tarifs :" )
		vbox_tarifs.pack_start align_tarifs, :expand => false, :fill => false, :padding => 3
		vbox_tarifs.pack_start tableau_tarifs, :expand => true, :fill => true, :padding => 3
		align_stock = Gtk::Alignment.new 0, 0, 0, 0
		align_stock.add Gtk::Label.new( "Stock :" )
		vbox_tarifs.pack_start align_stock, :expand => false, :fill => false, :padding => 3
		vbox_tarifs.pack_start tableau_stock, :expand => true, :fill => true, :padding => 3
		bouton_stock = Gtk::Button.new :label => "Transaction manuelle"
		#vbox_tarifs.pack_start bouton_stock, false, false, 3
		
		scroll_desc = Gtk::ScrolledWindow.new
		scroll_desc.add @description
		scroll_desc.set_policy :never, :automatic
		scroll_desc.shadow_type = :in
		
		align_desc = Gtk::Alignment.new 0, 0, 0, 0
		align_desc.add Gtk::Label.new( "Description :" )
		
		vbox_note.pack_start align_desc, :expand => false, :fill => false, :padding => 3
		vbox_note.pack_start scroll_desc, :expand => true, :fill => true, :padding => 3
		
		vbox_note.pack_start notebook, :expand => true, :fill => true, :padding => 3
		
		vbox.pack_start hbox, :expand => true, :fill => true, :padding => 3
		
		vbox
	
	end
	
	def tableau_tarifs
	
		@star = Gdk::Pixbuf.new("resources/icons/starred.png", 16, 16)
		@empty = Gdk::Pixbuf.new("resources/icons/empty.png", 16, 16)
		@iter_pa = nil
		@iter_cr = nil
		
		# list_store (tarif_id, designation_tarif, tarif, id, Etoile, vente, marge, marge%, marge_defaut)
		@list_store = Gtk::ListStore.new(Integer, String, String, Integer, Gdk::Pixbuf, Integer, String, String, Float)
		@view = Gtk::TreeView.new(@list_store)
		
		@view.signal_connect ("button-release-event") { |tree,event|
			# Si clic gauche et clic sur colonne étoile
			e = @view.get_path_at_pos(event.x, event.y)
			if (e and event.button.eql?(1))
				colonne = e[1]
				if colonne.eql?(@view.get_column(4))
					l = @list_store.get_iter(e[0])
					l[7] = "%.2f" % l[8]
					calcule_pu l, true
					calcule_marge
				end
			end
		}
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		
		col = Gtk::TreeViewColumn.new("Type de tarif", renderer_left, :text => 1)
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		# Colonne pour le PU :
		renderer_pu = Gtk::CellRendererSpin.new
		renderer_pu.adjustment = Gtk::Adjustment.new(0, 0, 999999, 0.1, 10, 0)
		renderer_pu.digits = 2
		renderer_pu.xalign = 1
		renderer_pu.mode = :editable
		renderer_pu.editable = true
		renderer_pu.signal_connect('edited') { |spin, data, text|
			l=@view.model.get_iter(data)
			l[2] = "%.2f" % text
			calcule_cr
			#calcule_pu l, true
			calcule_marge
		} 
		col = Gtk::TreeViewColumn.new("Prix unitaire (€)", renderer_pu, :text => 2)
		col.resizable = true
		@view.append_column(col)
		
		renderer_marge = Gtk::CellRendererSpin.new
		renderer_marge.adjustment = Gtk::Adjustment.new(0, 0, 999999, 0.1, 10, 0)
		renderer_marge.digits = 2
		renderer_marge.xalign = 1
		renderer_marge.mode = :editable
		renderer_marge.editable = true
		renderer_marge.signal_connect('edited') { |spin, data, text|
			l = @view.model.get_iter(data)
			if l[5].eql?(1) then
				l[6] = "%.2f" % text
				calcule_pu l
				calcule_marge
			end
		} 
		col = Gtk::TreeViewColumn.new("Marge (€)", renderer_marge, :text => 6)
		col.resizable = true
		@view.append_column(col)
		
		renderer_margep = Gtk::CellRendererSpin.new
		renderer_margep.adjustment = Gtk::Adjustment.new(0, 0, 999999, 0.1, 10, 0)
		renderer_margep.digits = 2
		renderer_margep.xalign = 1
		renderer_margep.mode = :editable
		renderer_margep.editable = true
		renderer_margep.signal_connect('edited') { |spin, data, text|
			l = @view.model.get_iter(data)
			if l[5].eql?(1) then
				l[7] = "%.2f" % text
				calcule_pu l, true
				calcule_marge
			end
		} 
		col = Gtk::TreeViewColumn.new("Marge (%)", renderer_margep, :text => 7)
		col.resizable = true
		@view.append_column(col)
		
		renderer_pix = Gtk::CellRendererPixbuf.new		
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 4)
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:never, :automatic)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def tableau_stock
	
		# list_stock (id, designation_stock, n°lot, qtite, couleur)
		@list_stock = Gtk::ListStore.new(Integer, String, String, String, Float, String)
		@view_stock = Gtk::TreeView.new(@list_stock)
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		
		col = Gtk::TreeViewColumn.new("Stock", renderer_left, :text => 1, :background => 5)
		col.resizable = true
		col.expand = true
		@view_stock.append_column(col)
		
		col = Gtk::TreeViewColumn.new("N° Lot/Série", renderer_right, :text => 2, :background => 5)
		col.resizable = true
		@view_stock.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Quantité", renderer_right, :text => 3, :background => 5)
		col.resizable = true
		@view_stock.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	scroll.add @view_stock
  	
  	scroll
	
	end
	
	def notebook
	
		# Objets	
		@note = Gtk::TextView.new
		@etiquette = Gtk::CheckButton.new "Etiquette à refaire"
		@lot = Gtk::CheckButton.new "Article géré par lot ou série ?"
		@service = Gtk::CheckButton.new "Service (sinon produit)"
		@en_vente = Gtk::CheckButton.new "Article en vente ?"
		@en_achat = Gtk::CheckButton.new "Article en achat ?"
		@colisage = Gtk::SpinButton.new 0, MAX_SPIN, 1
		renderer_left = Gtk::CellRendererText.new
		@tva_model = Gtk::TreeStore.new(Integer, String)
		@tva_cb = Gtk::ComboBox.new :model => @tva_model
		@gencode = Gtk::Entry.new
		@gencode2 = Gtk::Entry.new
		@ged = Ged_box.new @window, "articles"
		@annee = Gtk::ComboBoxText.new
		image_add = Gtk::Button.new :label => "..."
		image_remove = Gtk::Button.new :label => "X"
		
		# Conteneurs
		notebook = Gtk::Notebook.new
		scroll_note = Gtk::ScrolledWindow.new
		table_carac = Gtk::Table.new 2, 4, false
		table_vente = Gtk::Table.new 2, 4, false
		table_achat = Gtk::Table.new 2, 4, false
		scroll_carac = Gtk::ScrolledWindow.new
		scroll_vente = Gtk::ScrolledWindow.new
		scroll_achat = Gtk::ScrolledWindow.new
		scroll_stat = Gtk::ScrolledWindow.new
		hbox_stat = Gtk::Box.new :horizontal, 2
		vbox_stat = Gtk::Box.new :vertical, 2
		hbox_image = Gtk::Box.new :horizontal, 2
		hbox_image.pack_start image_add, :expand => false, :fill => false, :padding => 3
		hbox_image.pack_start image_remove, :expand => false, :fill => false, :padding => 3

		# Propriétés des objets
		@note.wrap_mode = :word
		scroll_note.set_policy :never, :automatic
		scroll_note.shadow_type = :none
		scroll_carac.set_policy :never, :automatic
		scroll_carac.shadow_type = :none
		scroll_vente.set_policy :never, :automatic
		scroll_vente.shadow_type = :none
		scroll_achat.set_policy :never, :automatic
		scroll_achat.shadow_type = :none
		scroll_stat.set_policy :never, :automatic
		@colisage.width_chars = 8 
		@gencode2.width_chars = 13
		@gencode2.width_chars = 13
		remplir_annee
		@annee.active = 0
		@annee.signal_connect('changed') {
			graphique if @activer_combo
		}
		@service.signal_connect('toggled') {
			if @service.active?
				@lot.active = false 
				@lot.sensitive = false
			else
				@lot.sensitive = true
			end
		}
		
		# Agencement du conteneur Caractéristique
		table_carac.attach( @service, 0, 1, 0, 1, :fill, :fill, 5, 5 )
		table_carac.attach( @lot, 1, 2, 0, 1, :fill, :fill, 5, 5 )
		table_carac.attach( @etiquette, 0, 1, 1, 2, :fill, :fill, 5, 5 )
		table_carac.attach( Gtk::Label.new("Code barre 1 :") , 0, 1, 2, 3, :fill, :fill, 5, 5 )
		table_carac.attach( @gencode, 1, 2, 2, 3, :fill, :fill, 5, 5 )
		table_carac.attach( Gtk::Label.new("Code barre 2 :") , 0, 1, 3, 4, :fill, :fill, 5, 5 )
		table_carac.attach( @gencode2, 1, 2, 3, 4, :fill, :fill, 5, 5 )
		table_carac.attach( Gtk::Label.new("Image :") , 0, 1, 4, 5, :fill, :fill, 5, 5 )
		table_carac.attach( hbox_image, 1, 2, 4, 5, :fill, :fill, 5, 5 )
		table_carac.attach( @image, 0, 2, 5, 6, :fill, :fill, 5, 5 )
		scroll_carac.add_with_viewport table_carac
		
		# Agencement du conteneur Achat
		table_achat.attach( @en_achat , 0, 1, 0, 1, :fill, :fill, 5, 5 )
		scroll_achat.add_with_viewport table_achat
		
		# Agencement du conteneur Vente
		table_vente.attach( @en_vente , 0, 1, 0, 1, :fill, :fill, 5, 5 )
		table_vente.attach( Gtk::Label.new("Colisage lors de la vente :") , 0, 1, 1, 2, :fill, :fill, 5, 5 )
		table_vente.attach( @colisage, 1, 2, 1, 2, :fill, :fill, 5, 5 )
		@tva_cb.pack_start renderer_left, true
		@tva_cb.add_attribute renderer_left, "text", 1
		table_vente.attach( Gtk::Label.new("TVA lors de la vente :") , 0, 1, 2, 3, :fill, :fill, 5, 5 )
		table_vente.attach( @tva_cb, 1, 2, 2, 3, :fill, :fill, 5, 5 )
		scroll_vente.add_with_viewport table_vente
		
		# Agencement du conteneur Note
		scroll_note.add @note
		
		# Agencement du conteneur Stat
		vbox_stat.pack_start hbox_stat, :expand => false, :fill => false, :padding => 3
		vbox_stat.pack_start scroll_stat, :expand => true, :fill => true, :padding => 3
		hbox_stat.pack_start Gtk::Label.new("Annéee :"), :expand => false, :fill => false, :padding => 3
		hbox_stat.pack_start @annee, :expand => false, :fill => false, :padding => 3
		scroll_stat.add_with_viewport @graph
		
		# Agencement du notebook
		notebook.append_page scroll_carac, Gtk::Label.new("Caractéristiques")
		notebook.append_page scroll_achat, Gtk::Label.new("Achat")
		notebook.append_page scroll_vente, Gtk::Label.new("Vente")
		notebook.append_page scroll_note, Gtk::Label.new("Note")
		notebook.append_page @ged, Gtk::Label.new("GED")
		notebook.append_page vbox_stat, Gtk::Label.new("Stats")

		# Renvoie
		notebook
		
	end
	
	def refresh id=0
	
		@id = id
		@code.text = ""
		@gencode.text = ""
		@gencode2.text = ""
		@designation.text = ""
		@colisage.value = 1
		@note.buffer.text = ""
		@article = nil
		@list_stock.clear
		
		if id>0 then
			@frame.label = "Article n° " + id.to_s
			#@lot.sensitive = false
			@article = Article.find(id)
			if @article then
				@code.text = @article.code
				@code.sensitive = false
				@gencode.text = @article.gencode
				@gencode2.text = @article.gencode2
				@designation.text = @article.designation
				@colisage.value = @article.colisage
				@note.buffer.text = @article.note
				@description.buffer.text = @article.description

				remplir_rayons @article.rayon_id
				remplir_tva @article.tva_id
				remplir_groupe @article.articlegroupe_id
				@etiquette.active = @article.refaire_etiquette
				@en_vente.active = @article.vente
				@en_achat.active = @article.achat
				@service.active = @article.service
				@lot.active = @article.lot
				@lot.sensitive = !@article.service
				@service.sensitive = false
			else
				MessageController.erreur @window,  @article.error
			end
			remplir_tarifs
			calcule_marge
			remplir_stock unless @article.service
			@ged.refresh @id
		else
			@frame.label = "Nouvel article"
			@lot.sensitive = true
			@lot.active = false
			@article = Article.new
			@code.sensitive = true
			remplir_rayons
			remplir_tva
			remplir_groupe
			@en_vente.active = true
			@en_achat.active = true
			@service.active = false
			@service.sensitive = true
			@description.buffer.text = ""
			
			tarifs = Tarif.order("id")	
			
			@list_stock.clear
			
			remplir_tarifs
			
		end
		
		graphique
		
		chemin_image = File.join(@window.config_db.conf["chemin_documents"].to_s,"articles","images","#{@id}.jpg")
		if File.exist?(chemin_image) then
			@image.file = chemin_image
		else
			@image.file = "./resources/icons/nophoto.png"
		end
	
	end
	
	def graphique
	
		combo_sensitive false

		serie = graph_donnees
		couleur = [[0.65,1,0.65],[1,0.65,0.65]]
		legende = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
		Thread.new { 
			@graph.refresh(serie, couleur, legende, ["","unité(s)"])
			@activer_combo = true
			combo_sensitive true
		}
	
	end
	
	def graph_donnees
		
		serie0 = []
		serie1 = []
		
		annee = @annee.active_text.to_i
		
		[4,8].each do |type|
		
			req = 	"		SELECT EXTRACT(MONTH FROM t1.date_document) AS mois, SUM(t0.qtite) AS total
									FROM documentlignes t0 INNER JOIN documents t1 ON t0.document_id=t1.id
									WHERE t0.article_id=#{@id} AND t1.documenttype_id=#{type}
									AND EXTRACT(YEAR FROM t1.date_document)=#{annee}
									AND t0.suppr=false
									GROUP BY mois
									ORDER BY mois
							"		
		
			res = Documentligne.find_by_sql(req)

			(1..12).to_a.each { |m|
		
				mois_rempli = false
			
				res.each { |r|
					if r.mois.to_i.eql?(m)
						if type.eql?(4)
							serie0 << r.total.to_f
						else
							serie1 << r.total.to_f
						end
						mois_rempli = true
					end
				}
				
				if type.eql?(4)
					serie0 << 0.0 unless mois_rempli
				else
					serie1 << 0.0 unless mois_rempli
				end
			
			}
		
		end
		
		serie = []
		serie0.count.times do |i|
			serie << [serie0[i],serie1[i]]
		end
		
		return serie

	end
	
	def remplir_annee
		
		doc = Document.find_by_sql("SELECT MIN(date_document) AS min, MAX(date_document) AS max FROM documents").first
		if doc.min and doc.max
			min = doc.min.year
			max = doc.max.year
			(min..max).to_a.reverse.each do |a|
				@annee.append_text a.to_s
			end
		end
		
	end
	
	def remplir_tarifs
		
		tarifs = Tarif.order("vente DESC", :id)
		
		@list_store.clear	
		
		if tarifs then
			tarifs.each { |t|
				
				iter = @list_store.append
				iter[0] = t.id
				iter[1] = t.designation
				iter[4] = t.vente ? @star : @empty
				iter[5] = t.vente ? 1 : 0
				iter[8] = t.vente ? t.marge : 0
				at = Articletarif.where(:article_id => @article.id, :tarif_id => t.id).first
				if at then
					iter[2] = "%.2f" % at.pu
					iter[3] = at.id
					iter[6] = "%.2f" % at.marge.to_f unless at.marge.eql?(0.0)
				else
					iter[2] = "%.2f" % 0.0
					iter[3] = -1
				end		
				@iter_pa = iter if t.pa
				@iter_cr = iter if t.cr
			} 
		else
			MessageController.erreur @window,  tarifs.error
		end

	end
	
	def calcule_cr
		@iter_cr[2] = "%.2f" % @iter_pa[2].to_f if @iter_pa[2].to_f>@iter_cr[2].to_f
	end
	
	def calcule_marge
	
		cr = @iter_cr[2].to_f
		# Pour chaque tarifs
		@list_store.each { |model, path, iter|		
			l = @view.model.get_iter(path)
			l[6] = "%.2f" % (l[2].to_f-cr) if l[5].eql?(1)
			l[7] = "%.2f" % (((l[2].to_f/cr)-1)*100) if l[5].eql?(1)
		}
	
	end
	
	def calcule_pu l, pourcentage=false
		
		# On procède au calcul du PU
		if pourcentage then
			m = (l[7].to_f/100) + 1
			l[2] = "%.2f" % (m*@iter_cr[2].to_f)
		else
			l[2] = "%.2f" % (@iter_cr[2].to_f+l[6].to_f)
		end
	
	end
	
	def remplir_stock
	
		stocks = Articlestock.includes(:stock).where(:article_id => @article.id)
		
		total = 0.0
		
		if stocks then
			stocks.each { |s|
				if s.qtite!=0
					iter = @list_stock.append
					iter[0] = s.id
					iter[1] = s.stock.designation
					iter[2] = s.lot
					iter[3] = "%.3f" % s.qtite
					iter[5] = "#ffffff"
					total += s.qtite
				end
			} 
			iter = @list_stock.append
			iter[1] = "Total"
			iter[2] = ""
			iter[3] = "%.3f" % total
			iter[5] = "#eeeeee"
		else
			MessageController.erreur @window,  stocks.error
		end

	end
	
	def remplir_rayons nom=nil
		
		@rayon_model.clear
		
		rayons = Rayon.order(:nom)
				
		if rayons then
			i=0
			id=-1
			rayons.each do |rayon|
				iter = @rayon_model.append nil
				iter[0] = rayon.id
				iter[1] = rayon.nom
				if !nom.nil? then 
					id = i if nom.eql?(rayon.id) 
				end
				i += 1
			end
			@rayon_cb.active = ( nom.nil? ? -1 : id )
		else
			MessageController.erreur @window,  rayons.error
		end
		
	end
	
	def remplir_tva nom=nil
		
		@tva_model.clear
		
		tvas = Tva.order(:id)
		
		if tvas then
			i=0
			id=-1
			tvas.each do |tva|
				iter = @tva_model.append nil
				iter[0] = tva.id
				iter[1] = tva.designation
				if !nom.nil? then id = i if nom.eql?(tva.id) end
				i += 1
			end
		
			@tva_cb.active = ( nom.nil? ? 0 : id )
		else
			MessageController.erreur @window,  tvas.error
		end
		
	end
	
	def remplir_groupe nom=nil
		
		@groupe.model.clear
		
		groupesp = Articlegroupe.order(:designation)
		
		if groupesp then
			active_iter = nil
			groupesp.each do |gr|
				if gr.parent_id.eql?(0)
					iter = @groupe_model.append nil
					iter[0] = gr.id
					iter[1] = gr.designation	
					if !nom.nil?
						active_iter = iter if nom.eql?(gr.id)		
					end						
					groupesp.each do |gr2|
						if gr2.parent_id.eql?(gr.id)
							iter2 = @groupe_model.append iter
							iter2[0] = gr2.id.to_i
							iter2[1] = gr2.designation
					
							if !nom.nil?
								active_iter = iter2 if nom.eql?(gr2.id)		
							end	
						end
					end
				end
			end
		
			if !active_iter.nil? then @groupe.active_iter = active_iter end
		else
			MessageController.erreur @window,  groupesp.error
		end
		
	end
	
	def validate quitter=true
		
		# Vérification d'erreurs
		res = false
		error = ""
		if !(@id>0) then
			if @code.text.empty? then
				error += "Vous devez saisir un code pour cet article\n"
			else
				error += "Le code article saisi existe déjà. Veuillez en saisir un autre.\n" if Article.where("code ILIKE ?", @code.text).count>0
			end
		end
		error += @designation.text.empty? ? "Vous devez saisir une désignation pour cet article\n" : ""
		
		# Si pas d'erreurs on continue
		if error.empty? then

			date_modif = Date.today.strftime("%Y-%m-%d")
			groupe = @groupe.active_iter.nil? ? 0 : @groupe_model.get_value(@groupe.active_iter,0)
			rayon = @rayon_model.get_value(@rayon_cb.active_iter,0) unless @rayon_cb.active_iter.nil?
			tva = @tva_cb.active_iter.nil? ? 0 : @tva_model.get_value(@tva_cb.active_iter,0)
		
			# sauvegarde des données de l'article
			@article.designation = @designation.text
			@article.colisage = @colisage.value.round
			@article.tva_id = tva
			@article.code = @code.text
			@article.gencode = @gencode.text
			@article.gencode2 = @gencode2.text
			@article.description = @description.buffer.text
			@article.note = @note.buffer.text
			@article.rayon_id = rayon
			@article.refaire_etiquette = @etiquette.active?
			@article.lot = @lot.active?
			@article.date_modif = date_modif
			@article.achat = @en_achat.active?
			@article.vente = @en_vente.active?
			@article.service = @service.active?
			@article.articlegroupe_id = groupe

			res = @article.save
		
			if res then
		
				@list_store.each { |model, path, iter|		
					pu = @view.model.get_iter(path)[2]
					tarif_id = @view.model.get_iter(path)[0]
					id = @view.model.get_iter(path)[3]
					marge = @view.model.get_iter(path)[6].nil? ? 0 : @view.model.get_iter(path)[6]
				
					if id>0 then
						tarif = Articletarif.find(id)
					else
						tarif = Articletarif.new
					end
					tarif.tarif_id = tarif_id
					tarif.article_id = @article.id								
					tarif.pu = pu
					tarif.marge = marge
					tarif.save	
				}
		
			end
		
			quit if (!@dial and quitter)
		
		else
			MessageController.erreur @window,  error
		end
		return res
	
	end
	
	def combo_sensitive status
		@annee.sensitive = status
	end
	
	def focus
		@annuler.grab_focus
	end
	
	def supprimer
	
		if MessageController.question_oui_non @window, "Voulez-vous réellement supprimer cet article ?"
				@article.suppr = true
				res = @article.save
				quit
		end 
	
	end
	
	def quit
	
		@window.liste_articles.refresh
		@window.affiche @window.liste_articles
		@window.liste_articles.focus
	
	end
	
end


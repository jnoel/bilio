# coding: utf-8
		
class About < Gtk::AboutDialog

	def initialize version
		
		super()

		self.version = version
		self.comments = "Logiciel de gestion commerciale écrit en Ruby/GTK3"
		lic = ""
		file = File.open("../LICENCE", "r")
		while (line=file.gets)
			lic += line
		end
		file.close
		self.license = lic
		self.website = "http://bilio.org"
		self.authors = ["Jean-Noël Rouchon"]
		self.artists = ["Logo: OpenClipart", "Icônes: Faenza icon theme"]
		self.logo = Gdk::Pixbuf.new( "./resources/icons/icon.png" )
		self.set_icon( "./resources/icons/icon.png" )
		
		self.show
	
	end

end		

# coding: utf-8

module MessageController
	
	def MessageController.erreur window, message
		message window, Gtk::MessageType::ERROR, message
	end
	
	def MessageController.attention window, message
		message window, Gtk::MessageType::WARNING, message
	end
	
	def MessageController.info window, message
		message window, Gtk::MessageType::INFO, message
	end
	
	def MessageController.message window, type, message
		
		messagetype = ""
		case type
			when Gtk::MessageType::ERROR
				messagetype = "ERREUR"
			when Gtk::MessageType::WARNING
				messagetype = "ATTENTION"
			when Gtk::MessageType::INFO
				messagetype = "INFO"
		end
		text "=== #{messagetype} : #{message}"
		
		dialog = Gtk::MessageDialog.new(:parent =>window, 
                                		:flags => :destroy_with_parent,
                                		:type => type,
                                		:buttons_type => :ok,
                                		:message => message)
		response = dialog.run
		dialog.destroy
		
	end
	
	def MessageController.question_oui_non window, message
	
		dialog = Gtk::MessageDialog.new(:parent => window, 
					                          :flags => :destroy_with_parent,
					                          :type => :question,
					                          :buttons_type => :yes_no,
					                          :message => message)
		response = dialog.run
		dialog.destroy
		return response==Gtk::ResponseType::YES
		
	end
	
	def MessageController.text message
		puts "#{message}\n"
	end

end		

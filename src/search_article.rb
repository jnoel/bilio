# coding: utf-8

class SearchArticles_box < Gtk::Dialog

	attr_reader :liste
	
	def initialize window, term
		
		super(:title => "Recherche article", :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])
		
		set_default_size 900, 500
		
		@liste = ListeArticles_box.new window, true
		
		@liste.view.signal_connect ("row-activated") { |view, path, column|
			# renvoi le signal OK si double clic
			response(Gtk::ResponseType::OK)
		}
		
		@liste.add_new.signal_connect( "clicked" ) {
			dial_article = DialNewArticle.new window
			dial_article.run { |response| 
				code = nil
				if response.eql?(-5)
					dial_article.article_window.validate
					code = dial_article.article_window.article.code
				end
				dial_article.destroy
				@liste.refresh code.to_s
				un_seul?
			}
		}
		
		@liste.refresh term
		
		@liste.search.text = term
		
		self.child.pack_start @liste, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all
		
		Thread.new {
			sleep 0
			un_seul?
		}
	
	end
	
	# Si un seul dans la liste, sélection automatique et fermeture du dialogue.
	def un_seul?
		i = 0
		npath = nil
		@liste.view.model.each { |model, path, iter| 
			i += 1
			npath = path
		}
		if i.eql?(1)
			@liste.view.set_cursor(npath, nil, false)
			response(Gtk::ResponseType::OK)
		end
	end

end		

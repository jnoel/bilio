# coding: utf-8

class SearchTiers < Gtk::Dialog

	attr_reader :liste
	
	def initialize window, term, type
		
		super(:title => "Recherche tiers", :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])
		
		set_default_size 900, 500
		
		@liste = ListeTiers_box.new window, true
		
		@liste.view.signal_connect ("row-activated") { |view, path, column|
			# renvoi le signal OK si double clic
			self.response(Gtk::ResponseType::OK)
		}
		
		@liste.add_new.signal_connect( "clicked" ) {
			dial_tiers = DialNewTiers.new window
			dial_tiers.run { |response| 
				code = nil
				if response.eql?(-5)
					dial_tiers.tiers_window.validate
					code = dial_tiers.tiers_window.tiers.nom
				end
				dial_tiers.destroy
				@liste.refresh type, code.to_s
				un_seul?
			}
		}
		
		@liste.filtre.active = type
		@liste.refresh type, term  # 0=> clients, 1 => fournisseurs
		
		@liste.search.text = term
		
		self.child.pack_start @liste, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all
		
		#Thread.new {
		#	sleep 0
		#	un_seul?
		#}
	
	end
	
	# Si un seul dans la liste, sélection automatique et fermeture du dialogue.
	def un_seul?
		i = 0
		npath = nil
		@liste.view.model.each { |model, path, iter| 
			i += 1
			npath = path
		}
		if i.eql?(1)
			@liste.view.set_cursor(npath, nil, false)
			response(Gtk::ResponseType::OK)
		end
	end


end		

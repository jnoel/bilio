# coding: utf-8

class SearchLots < Gtk::Dialog

	attr_reader :liste
	
	def initialize window, article
		
		super(:title => "Recherche lots", :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])
		
		set_default_size 900, 300
		
		@liste = ListeLots_box.new window
		
		@liste.view.signal_connect ("row-activated") { |view, path, column|
			# renvoi le signal OK si double clic
			self.response(-5)
		}
		
		@liste.refresh article
		
		self.child.pack_start @liste, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all
	
	end

end		


# coding: utf-8

class Saisie_reglement < Gtk::Dialog
	
	def initialize window, type, id_tiers, id_paiement=-1, suppr=false
		
		super(:title => "Saisie de réglements", :parent => window, :flags => :destroy_with_parent)
		
		@type = type
		@id_tiers = id_tiers
		@montant_total = 0
		@id_paiement = id_paiement
		@window = window
		
		set_default_size 900, 500
		
		vbox = Gtk::Box.new :vertical, 3
		
		frame = Gtk::Frame.new
		frame.label = type.eql?(4) ? "Saisie d'un réglement reçu d'un client" : "Saisie d'un paiement envoyé à un fournisseur"
		frame.add vbox
		
		vbox.pack_start( entete(id_tiers), :expand => false, :fill => false, :padding => 3 )
		vbox.pack_start( corps(type,id_tiers), :expand => true, :fill => true, :padding => 3 )
		vbox.pack_start( pieds, :expand => false, :fill => false, :padding => 3 )
		
		self.child.pack_start frame, :expand => true, :fill => true, :padding => 0
		
		self.child.show_all
		
		remplir_compte
	
	end
	
	def entete id_tiers
		tiers = Tiers.find(id_tiers)
		
		@date = Gtk::Entry.new
		@date.text = DateTime.now.strftime("%d/%m/%Y")
		@mode = Gtk::ComboBox.new :model => mode
		renderer_mode = Gtk::CellRendererText.new
		@mode.pack_start(renderer_mode, true)
		@mode.set_attributes(renderer_mode, :text => 1)
		@mode.active = 0
		@compte = Gtk::ComboBox.new :model => Gtk::ListStore.new(Integer, String)
		renderer_compte = Gtk::CellRendererText.new 
		@compte.pack_start(renderer_compte, true)
		@compte.set_attributes(renderer_compte, :text => 1)
		@compte.active = 0
		@num = Gtk::Entry.new
		@emetteur = Gtk::Entry.new
		@banque = Gtk::Entry.new
		@note = Gtk::TextView.new
		scroll_note = Gtk::ScrolledWindow.new
		scroll_note.add @note
		scroll_note.set_policy :never, :automatic
		scroll_note.shadow_type = :in
		
		table = Gtk::Table.new 3, 2, false
		label_tiers = Gtk::Alignment.new 0, 0, 0, 0
		label_tiers.add Gtk::Label.new("Tiers:") 
		table.attach( label_tiers, 0, 1, 0, 1, :fill, :fill, 2, 2 )
		label_tiers_nom = Gtk::Alignment.new 0, 0, 0, 0
		label_tiers_nom.add Gtk::Label.new(tiers.nom)
		table.attach( label_tiers_nom, 1, 2, 0, 1, :fill, :fill, 2, 2 )
		label_date = Gtk::Alignment.new 0, 0, 0, 0
		label_date.add Gtk::Label.new("Date:") 
		table.attach( label_date, 0, 1, 1, 2, :fill, :fill, 2, 2 )
		table.attach( @date, 1, 2, 1, 2, :fill, :fill, 2, 2 )
		label_mode = Gtk::Alignment.new 0, 0, 0, 0
		label_mode.add Gtk::Label.new("Mode de réglement:") 
		table.attach( label_mode, 0, 1, 2, 3, :fill, :fill, 2, 2 )
		table.attach( @mode, 1, 2, 2, 3, :fill, :fill, 2, 2 )
		label_compte = Gtk::Alignment.new 0, 0, 0, 0
		label_compte.add Gtk::Label.new("Compte à #{type.eql?(4) ? 'créditer' : 'débiter'}:")
		table.attach( label_compte, 0, 1, 3, 4, :fill, :fill, 2, 2 )
		table.attach( @compte, 1, 2, 3, 4, :fill, :fill, 2, 2 )
		label_numero = Gtk::Alignment.new 0, 0, 0, 0
		label_numero.add Gtk::Label.new("Numéro chèque:")
		table.attach( label_numero, 0, 1, 4, 5, :fill, :fill, 2, 2 )
		table.attach( @num, 1, 2, 4, 5, :fill, :fill, 2, 2 )
		label_emetteur = Gtk::Alignment.new 0, 0, 0, 0
		label_emetteur.add Gtk::Label.new("Emetteur chèque:")
		table.attach( label_emetteur, 0, 1, 5, 6, :fill, :fill, 2, 2 )
		table.attach( @emetteur, 1, 2, 5, 6, :fill, :fill, 2, 2 )
		label_banque = Gtk::Alignment.new 0, 0, 0, 0
		label_banque.add Gtk::Label.new("Banque:")
		table.attach( label_banque, 0, 1, 6, 7, :fill, :fill, 2, 2 )
		table.attach( @banque, 1, 2, 6, 7, :fill, :fill, 2, 2 )
		label_note = Gtk::Alignment.new 0, 0, 0, 0
		label_note.add Gtk::Label.new("Note:") 
		table.attach( label_note, 2, 3, 1, 2, :fill, :fill, 2, 2 )
		table.attach( scroll_note, 2, 3, 2, 7, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, 2, 2 )
		
		table
	end
	
	def corps type, id_tiers
									# ID     Ref     Date   Montant  Reçu    Reste  Reglement  Etoile
		@liste = Gtk::ListStore.new(Integer, String, String, String, String, String, String, Gdk::Pixbuf)
		table_factures = Gtk::TreeView.new(@liste)
		
		star = Gdk::Pixbuf.new("resources/icons/starred.png", 16, 16)
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_right = Gtk::CellRendererText.new
		renderer_left.background = renderer_right.background = "white"
		renderer_left.yalign = renderer_right.yalign = 0
		renderer_left.xalign = 0
		renderer_right.xalign = 1
		
		col = Gtk::TreeViewColumn.new("ID", renderer_left, :text => 0)
		
		col = Gtk::TreeViewColumn.new("Factures", renderer_left, :text => 1)
		col.expand = true
		table_factures.append_column(col)		
		
		col = Gtk::TreeViewColumn.new("Date", renderer_right, :text => 2)
		table_factures.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Montant TTC", renderer_right, :text => 3)
		table_factures.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Reçu", renderer_right, :text => 4)
		table_factures.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Reste à payer", renderer_right, :text => 5)
		table_factures.append_column(col)
		
		renderer_reglement = Gtk::CellRendererSpin.new
		renderer_reglement.adjustment = Gtk::Adjustment.new(0, 0, 999999, 1, 10, 0)
		renderer_reglement.xalign = 1
		renderer_reglement.digits = 2
		renderer_reglement.yalign = 0
		renderer_reglement.mode = :editable
		renderer_reglement.editable = true
		renderer_reglement.signal_connect('edited') { |combo, data, text|
			change_reglement data, text
		} 
		col = Gtk::TreeViewColumn.new("Montant règlement", renderer_reglement, :text => 6)
		table_factures.append_column(col)
		
		renderer_pix = Gtk::CellRendererPixbuf.new		
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 7)
		table_factures.append_column(col)
		
		factures = Document.where("tiers_id=? AND documenttype_id=? AND montant_regle<montant_ttc AND suppr=?", id_tiers, type, false).order(:date_document)
		factures.each { |f|
			if f.montant_regle<f.montant_ttc
				iter = @liste.append
				iter[0] = f.id
				iter[1] = f.ref
				iter[2] = f.date_document.strftime("%d/%m/%Y")
				iter[3] = "%.2f" % f.montant_ttc	
				iter[4] = "%.2f" % f.montant_regle	
				iter[5] = "%.2f" % (f.montant_ttc-f.montant_regle)
				iter[7] = star
			end
		}
		
		table_factures.signal_connect ("button-release-event") { |tree,event|
			# Si clic gauche et clic sur colonne étoile alors on écrit le reste à payer dans le montant
			e = table_factures.get_path_at_pos(event.x, event.y)
			if (e and event.button.eql?(1))
				colonne = e[1]
				if colonne.eql?(table_factures.get_column(6))
					l = @liste.get_iter(e[0])
					change_reglement e[0], l[5]
				end
			end
		}
		
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(:automatic, :automatic)
    	scroll.add table_factures
    	
    	scroll		
		
	end
	
	def change_reglement data, text
		if text.to_f>@liste.get_iter(data)[3].to_f
			if MessageController.question_oui_non self, "Voulez-vous réellement quitter #{GLib.application_name.capitalize} ?"
				@liste.get_iter(data)[6] = text
				calcule_pieds
			else
				@liste.get_iter(data)[6] = "0"
				calcule_pieds
			end 
		else
			@liste.get_iter(data)[6] = text
			calcule_pieds
		end
	end
	
	def pieds
		
		hbox = Gtk::Box.new :horizontal, 3
		
		align_compteur = Gtk::Alignment.new 0, 0, 0, 0
		@compteur = Gtk::Label.new
		align_compteur.add @compteur
		hbox.pack_start align_compteur, :expand => false, :fill => false, :padding => 3
		
		ok_button = Gtk::Button.new :stock_id => Gtk::Stock::OK
		ok_button.signal_connect ("clicked") { save }
		cancel_button = Gtk::Button.new :stock_id => Gtk::Stock::CANCEL
		cancel_button.signal_connect ("clicked") { self.destroy }
		align_button = Gtk::Alignment.new 1, 1, 0, 0
		hbox_button = Gtk::Box.new :horizontal, 5
		hbox_button.add cancel_button
		hbox_button.add ok_button
		align_button.add hbox_button
		
		hbox.pack_start align_button, :expand => true, :fill => true, :padding => 3
		
	end
	
	def calcule_pieds
		compte = 0
		@liste.each { |model, path, iter|
			compte += @liste.get_iter(path)[6].to_f
		}
		@montant_total = compte.round(2)
		@compteur.text = "Montant total = #{"%.2f €" % compte}"
	end
	
	def mode
		liste = Gtk::ListStore.new(Integer, String)
		modes = Paiementtype.order(:id)
		modes.each { |m|
			iter = liste.append
			iter[0] = m.id
			iter[1] = m.designation			
		}
		liste
	end
	
	def remplir_compte
		iter_default = nil
		iter = nil
		comptes = Compte.order(:id)
		comptes.each { |c|
			iter = @compte.model.append
			iter[0] = c.id
			iter[1] = c.designation
			iter_default = iter if c.defaut
		}
		@compte.active_iter = (iter_default ? iter_default : iter)
	end
		
	def save
		
		if @id_paiement<1
			save_new_paiement
		else
			update_paiement
		end
		
		self.destroy
		
	end
	
	def save_new_paiement
	
		paiement = Paiement.new
		paiement.paiementtype_id = @mode.model.get_value(@mode.active_iter,0)
		paiement.tiers_id = @id_tiers
		paiement.compte_id = @compte.model.get_value(@compte.active_iter,0)
		paiement.montant = @montant_total
		paiement.numero = @num.text
		paiement.emetteur = @emetteur.text
		paiement.banque = @banque.text
		paiement.note = @note.buffer.text
		paiement.date_paiement = Date.parse(@date.text)
		paiement.date_comptable = Date.parse(@date.text)
		paiement.fournisseur = !@type.eql?(4)
		res = paiement.save
	
		if res
			docs = []
			@liste.each { |model, path, iter|
				if @liste.get_iter(path)[6].to_f>0
					paiementdoc = Paiementdocument.new
					paiementdoc.paiement_id = paiement.id
					paiementdoc.document_id = @liste.get_iter(path)[0]
					paiementdoc.montant = @liste.get_iter(path)[6].to_f
					res = paiementdoc.save
					if res 
						docs << paiementdoc.document_id unless docs.include?(paiementdoc.document_id)
					end
				end
			}
			ReglementController.update_montant_regle_docs docs
		end
		
	end

end		

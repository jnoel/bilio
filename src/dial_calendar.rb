# coding: utf-8

class DialCalendar < Gtk::Dialog

	attr_reader :cal
	
	def initialize window
		
		super(:title => "Calendrier", :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])
		
		set_default_size 500, 500
		
		@cal = Gtk::Calendar.new
		
		@cal.signal_connect("day-selected-double-click") {
			self.response(Gtk::ResponseType::OK)
		}
		
		self.child.pack_start @cal, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all

	end

end

# coding: utf-8

class GraphBar < Gtk::DrawingArea
   
	MARGE_HORIZONTALE = 50
	MARGE_VERTICALE = 50
	ESPACE = 0
	ESPACE_GROUPE = 30
	
	def initialize absolu=false
    
    super()
    
    @absolu = absolu
    @active = false
    
    @serie = []
    @dim1 = 0
    @dim2 = 0
    @max = 0    
    
    @couleurs = []
    @legende = []
    @unite = ["",""]
    
    @histo_en_cours = []
    
    @largeur = 0
    @y = 0
    
		signal_connect "draw" do  
      on_expose true if @active
    end
    
    add_events Gdk::Event::POINTER_MOTION_MASK
    signal_connect "motion-notify-event" do |widget, event|
			device = self.window.get_device_position(event.device)
			pointer = [device[1], device[2]]
			new_histo = detect_histo(@y, @largeur, pointer)
			if new_histo!=@histo_en_cours
				@histo_en_cours = new_histo
				self.queue_draw
			end
  	end
    
  end
  
  def on_expose init=false, save_file=nil
   
		cr = window.create_cairo_context  
		
		if @dim1>0
		
			largeur = (self::allocation.width-@dim2*ESPACE-@dim1*ESPACE_GROUPE-MARGE_HORIZONTALE*2)/(@dim2*@dim1)
			if @absolu
				y = (self::allocation.height-MARGE_VERTICALE>MARGE_VERTICALE*1.5 ? self::allocation.height-MARGE_VERTICALE : MARGE_VERTICALE*1.5 )
			else
				y = (self::allocation.height/2>MARGE_VERTICALE*1.5 ? self::allocation.height/2 : MARGE_VERTICALE*1.5 )
			end
		
			@largeur = largeur
			@y = y
		
			draw_background cr, y, largeur if init
			draw_axis cr, y, largeur if init
			draw_graduations_vert cr, y, largeur if init
			draw_graduations_horiz cr, y, largeur if init
			draw_graph cr, y, largeur
			draw_histo cr, y, largeur, @histo_en_cours unless @histo_en_cours.empty?
			draw_legend cr, y, largeur if init

			window.write_to_png(save_file) if save_file
			
		else
			draw_empty_graph cr
		end
		
  end 
  
  def draw_background cr, y, largeur
  	
  	# Fond gris
  	cr.rectangle MARGE_HORIZONTALE, MARGE_VERTICALE, self::allocation.width-2*MARGE_HORIZONTALE, self::allocation.height-2*MARGE_VERTICALE
		cr.set_source_rgb 0.98, 0.98, 0.98
		cr.fill
		
		cr.set_dash [5], 1
		
		unless @absolu
			# ligne pointillée à 0
			cr.set_line_width 1
			cr.set_source_rgb 0.3, 0.3, 0.3
			cr.move_to MARGE_HORIZONTALE, y
			cr.line_to self::allocation.width-MARGE_HORIZONTALE, y
		
			# Tracer les lignes pointillées
			cr.stroke
			
			# ligne pointillée au milieu bas
			cr.move_to MARGE_HORIZONTALE, (3*y-MARGE_HORIZONTALE)/2
			cr.line_to self::allocation.width-MARGE_HORIZONTALE, (3*y-MARGE_HORIZONTALE)/2
			
		end
		
		# ligne pointillée en haut
		cr.set_source_rgb 0.7, 0.7, 0.7
		cr.move_to MARGE_HORIZONTALE, MARGE_VERTICALE
		cr.line_to self::allocation.width-MARGE_HORIZONTALE, MARGE_VERTICALE
		
		# ligne pointillée au milieu haut
		cr.move_to MARGE_HORIZONTALE, (y+MARGE_HORIZONTALE)/2
		cr.line_to self::allocation.width-MARGE_HORIZONTALE, (y+MARGE_HORIZONTALE)/2
		
		# lignes pointillées verticales
		x = MARGE_HORIZONTALE + ESPACE_GROUPE + largeur*@dim2/2 
		@dim1.times do |i|
			x += (largeur*@dim2+ESPACE_GROUPE) unless i.eql?(0)
			cr.move_to x, MARGE_VERTICALE
			cr.line_to x, self::allocation.height-MARGE_VERTICALE
		end
		
		# Tracer les lignes pointillées
		cr.stroke
  end
  
  def draw_axis cr, y, largeur
  
  	# Axe des ordonnées
  	cr.set_dash [], 0
		cr.set_source_rgb 0.2, 0.2, 0.2
		cr.set_line_width 1
		cr.move_to MARGE_HORIZONTALE, MARGE_VERTICALE
		cr.line_to MARGE_HORIZONTALE, self::allocation.height-MARGE_VERTICALE
		
		# Axe des absices
		cr.line_to self::allocation.width-MARGE_HORIZONTALE, self::allocation.height-MARGE_VERTICALE
		
		# Tracer les axes
		cr.stroke
  
  end
  
  # graduations des ordonnées
  def draw_graduations_vert cr, y, largeur
  	# graduation à 0
		font_size = 10
		marge = 5
		cr.set_font_size font_size
		text = "0"
		info = cr.text_extents text
		cr.move_to MARGE_HORIZONTALE-info.width-marge, y+info.height/2
		cr.show_text text
		
		# graduation en haut
		text = @max.round((@max>1 ? 0 : 1)).to_s
		info = cr.text_extents text
		cr.move_to MARGE_HORIZONTALE-info.width-marge, MARGE_VERTICALE+info.height
		cr.show_text text
		
		# graduation au milieu haut
		text = (@max/2).round((@max>1 ? 0 : 1)).to_s
		info = cr.text_extents text
		cr.move_to MARGE_HORIZONTALE-info.width-marge, (y+MARGE_HORIZONTALE)/2+info.height/2
		cr.show_text text
		
		unless @absolu
			# graduation au milieu bas
			text = (-1*@max/2).round((@max>1 ? 0 : 1)).to_s
			info = cr.text_extents text
			cr.move_to MARGE_HORIZONTALE-info.width-marge, (3*y-MARGE_HORIZONTALE)/2+info.height/2
			cr.show_text text
			
			# graduation en bas
			text = (-1*@max).round((@max>1 ? 0 : 1)).to_s
			info = cr.text_extents text
			cr.move_to MARGE_HORIZONTALE-info.width-marge, self::allocation.height-MARGE_VERTICALE
			cr.show_text text
		end
  end
  
  # graduations des absices
  def draw_graduations_horiz cr, y, largeur
  
		font_size = 10
		cr.set_font_size font_size
		x = MARGE_HORIZONTALE + ESPACE_GROUPE + largeur*@dim2/2 
		
		@legende.count.times do |i|
			x += (largeur*@dim2+ESPACE_GROUPE) unless i.eql?(0)
			text = @legende[i]
			info = cr.text_extents text
			cr.move_to x-info.width/2, self::allocation.height-MARGE_VERTICALE+15
			cr.show_text text
		end
		
  end
  
  def draw_graph cr, y, largeur
		
		@dim2.times do |i|
			
			@dim1.times do |j|
				
				x = j*(@dim2*(largeur+ESPACE)+ESPACE_GROUPE)+i*(largeur+ESPACE)+MARGE_HORIZONTALE+ESPACE_GROUPE
				hauteur = -1*@serie[j][i]*(y-MARGE_VERTICALE)/@max
							
				cr.rectangle x, y, largeur, hauteur

				z = (@dim2.eql?(1) ? j : i )
				cr.set_source_rgb @couleurs[z][0], @couleurs[z][1], @couleurs[z][2]
				cr.fill
			
			end
			
		end
		
  end
  
  def draw_histo cr, y, largeur, histo
  	
  	i = histo[1]
  	j = histo[0]
  	x = j*(@dim2*(largeur+ESPACE)+ESPACE_GROUPE)+i*(largeur+ESPACE)+MARGE_HORIZONTALE+ESPACE_GROUPE
		hauteur = -1*@serie[j][i]*(y-MARGE_VERTICALE)/@max
		
		# Affichage de l'info bulle
		if @serie[j][i].to_f!=0.0
			texte = "%.2f #{@unite[1]}" % @serie[j][i]
			info_bulle cr, x+largeur/2, y+hauteur+(hauteur>0 ? 16 : -7), texte, 12
		end
		
		# Trancé de l'histogramme
		cr.rectangle x, y, largeur, hauteur
		z = (@dim2.eql?(1) ? j : i )
		cr.set_source_rgb( (@couleurs[z][0]-0.2>0 ? @couleurs[z][0]-0.2 : 0), (@couleurs[z][1]-0.2>0 ? @couleurs[z][1]-0.2 : 0), (@couleurs[z][2]-0.2>0 ? @couleurs[z][2]-0.2 : 0) )
		cr.fill
  	
  end
  
  def info_bulle cr, x, y, text, text_size
  
  	# Infos sur le texte
  	cr.set_font_size text_size
  	info = cr.text_extents text
  
  	# Tracer de la bulle
		marge = info.height/2
		width = info.width + marge*2
		height = info.height + marge*2
		radius = height/2
		degrees = Math::PI / 180.0
		xrect = x-width/2
		yrect = y-height+marge
  	cr.new_sub_path
		cr.arc xrect + width - radius, yrect + radius, radius, -90 * degrees, 0 * degrees
		cr.arc xrect + width - radius, yrect + height - radius, radius, 0 * degrees, 90 * degrees
		cr.arc xrect + radius, yrect + height - radius, radius, 90 * degrees, 180 * degrees
		cr.arc xrect + radius, yrect + radius, radius, 180 * degrees, 270 * degrees
		cr.close_path
		cr.set_source_rgba 0,0,0,0.6
		cr.fill
  	
  	# Ecriture de l'info
  	cr.set_source_rgb 1,1,1
  	cr.move_to x-info.width/2,y
  	cr.show_text text
  
  end
  
  def detect_histo y, largeur, pointer
  
  	@dim2.times do |i|			
			@dim1.times do |j|				
				x = j*(@dim2*(largeur+ESPACE)+ESPACE_GROUPE)+i*(largeur+ESPACE)+MARGE_HORIZONTALE+ESPACE_GROUPE
				hauteur = -1*@serie[j][i]*(y-MARGE_VERTICALE)/@max
				
				if pointer[0]>x and pointer[0]<x+largeur and ((pointer[1]>=y and pointer[1]<=y+hauteur) or (pointer[1]>=y+hauteur and pointer[1]<=y))
					return [j,i]
				end		
			end		
		end		
		return []
		
  end
  
  def draw_legend cr, y, largeur
  
  end
  
  def draw_empty_graph cr
  	text = "Pas de graphique pour cette sélection"
  	text_size = 18
  	x = self::allocation.width/2
  	y = self::allocation.height/2
  	info_bulle cr, x, y, text, text_size
  end
  
  def refresh serie, couleurs, legende, unite=["",""]

  	@serie = serie
    @dim1 = @serie.count
    if @dim1>0
		  @dim2 = @serie[0].count
		  @max = (@serie.flatten.sort[@serie.flatten.count-1].abs>@serie.flatten.sort[0].abs ? @serie.flatten.sort[@serie.flatten.count-1].abs : @serie.flatten.sort[0].abs )
		else
			@dim2 = 0
			@max = 0
		end
		@couleurs = couleurs
	  @legende = legende
	  @unite = unite
    
    self.queue_draw
    
    @active = true
  
  end
	
	def save fichier
		on_expose init=true, fichier
	end
	
end

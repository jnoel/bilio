# coding: utf-8

class Saisie_virement_interne < Gtk::Dialog
	
	def initialize window
		
		super(:title => "Saisie interne", :parent => window, :flags => :destroy_with_parent)
		
		@window = window
		
		set_default_size 500, 300
		
		vbox = Gtk::Box.new :vertical, 3
		
		frame = Gtk::Frame.new
		frame.label = "Saisie d'un règlement interne"
		frame.add vbox
		
		vbox.pack_start( entete, :expand => true, :fill => true, :padding => 3 )
		vbox.pack_start( pieds, :expand => false, :fill => false, :padding => 3 )
		
		self.child.pack_start frame, :expand => true, :fill => true, :padding => 0
		
		self.child.show_all
	
	end
	
	def entete
				
		@date = Gtk::Entry.new
		@montant = Gtk::SpinButton.new 0, 9999999999, 1
		@montant.digits = 2
		@date.text = DateTime.now.strftime("%d/%m/%Y")
		@mode = Gtk::ComboBox.new :model => mode
		renderer_mode = Gtk::CellRendererText.new
		@mode.pack_start(renderer_mode, true)
		@mode.set_attributes(renderer_mode, :text => 1)
		@mode.active = 3
		@compte = Gtk::ComboBox.new :model => compte
		renderer_compte = Gtk::CellRendererText.new 
		@compte.pack_start(renderer_compte, true)
		@compte.set_attributes(renderer_compte, :text => 1)
		@compte.active = 0
		
		@compte2 = Gtk::ComboBox.new :model => compte
		@compte2.pack_start(renderer_compte, true)
		@compte2.set_attributes(renderer_compte, :text => 1)
		@compte2.active = 1
		
		@num = Gtk::Entry.new
		@note = Gtk::TextView.new
		scroll_note = Gtk::ScrolledWindow.new
		scroll_note.add @note
		scroll_note.set_policy :never, :automatic
		scroll_note.shadow_type = :in
		
		table = Gtk::Table.new 3, 2, false
		label_date = Gtk::Alignment.new 0, 0, 0, 0
		label_date.add Gtk::Label.new("Date:") 
		i=0
		table.attach( label_date, 0, 1, i, i+1, :fill, :fill, 2, 2 )
		table.attach( @date, 1, 2, i, i+1, :fill, :fill, 2, 2 )
		label_mode = Gtk::Alignment.new 0, 0, 0, 0
		label_mode.add Gtk::Label.new("Mode de réglement:") 
		
		i+=1
		table.attach( label_mode, 0, 1, i, i+1, :fill, :fill, 2, 2 )
		table.attach( @mode, 1, 2, i, i+1, :fill, :fill, 2, 2 )
		label_compte = Gtk::Alignment.new 0, 0, 0, 0
		label_compte.add Gtk::Label.new("Compte à déditer:")
		i+=1
		table.attach( label_compte, 0, 1, i, i+1, :fill, :fill, 2, 2 )
		table.attach( @compte, 1, 2, i, i+1, :fill, :fill, 2, 2 )
		label_compte2 = Gtk::Alignment.new 0, 0, 0, 0
		label_compte2.add Gtk::Label.new("Compte à créditer:")
		i+=1
		table.attach( label_compte2, 0, 1, i, i+1, :fill, :fill, 2, 2 )
		table.attach( @compte2, 1, 2, i, i+1, :fill, :fill, 2, 2 )
		label_numero = Gtk::Alignment.new 0, 0, 0, 0
		label_numero.add Gtk::Label.new("Numéro :")
		i+=1
		table.attach( label_numero, 0, 1, i, i+1, :fill, :fill, 2, 2 )
		table.attach( @num, 1, 2, i, i+1, :fill, :fill, 2, 2 )
		i+=1
		label_montant = Gtk::Alignment.new 0, 0, 0, 0
		label_montant.add Gtk::Label.new("Montant:")
		table.attach( label_montant, 0, 1, i, i+1, :fill, :fill, 2, 2 )
		table.attach( @montant, 1, 2, i, i+1, :fill, :fill, 2, 2 )
		
		label_note = Gtk::Alignment.new 0, 0, 0, 0
		label_note.add Gtk::Label.new("Note:") 
		table.attach( label_note, 2, 3, 0, 1, :fill, :fill, 2, 2 )
		table.attach( scroll_note, 2, 3, 1, 6, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, 2, 2 )
		
		table
	end
	
	def pieds
		
		hbox = Gtk::Box.new :horizontal, 3
		
		ok_button = Gtk::Button.new :stock_id => Gtk::Stock::OK
		ok_button.signal_connect ("clicked") { save }
		cancel_button = Gtk::Button.new :stock_id => Gtk::Stock::CANCEL
		cancel_button.signal_connect ("clicked") { self.destroy }
		align_button = Gtk::Alignment.new 1, 1, 0, 0
		hbox_button = Gtk::Box.new :horizontal, 5
		hbox_button.add cancel_button
		hbox_button.add ok_button
		align_button.add hbox_button
		
		hbox.pack_start align_button, :expand => true, :fill => true, :padding => 3
		
	end
	
	def mode
		liste = Gtk::ListStore.new(Integer, String)
		modes = Paiementtype.order(:id)
		modes.each { |m|
			iter = liste.append
			iter[0] = m.id
			iter[1] = m.designation			
		}
		liste
	end
	
	def compte
		liste = Gtk::ListStore.new(Integer, String)
		comptes = Compte.order(:id)
		comptes.each { |c|
			iter = liste.append
			iter[0] = c.id
			iter[1] = c.designation			
		}
		liste
	end
		
	def save
		compte1 = @compte.model.get_value(@compte.active_iter,0)
		compte2 = @compte2.model.get_value(@compte2.active_iter,0)
		
		unless compte1.eql?(compte2)
			unless @montant.value.eql?(0.0)
				paiement = Paiement.new
				paiement.paiementtype_id = @mode.model.get_value(@mode.active_iter,0)
				paiement.tiers_id = @id_tiers
				paiement.compte_id = compte1
				paiement.montant = @montant_total
				paiement.numero = @num.text
				paiement.note = "Virement interne - #{@note.buffer.text}"
				paiement.date_paiement = Date.parse(@date.text)
				paiement.date_comptable = Date.parse(@date.text)
				paiement.fournisseur = true
				paiement.montant = @montant.value
				res = paiement.save
		
				paiement = Paiement.new
				paiement.paiementtype_id = @mode.model.get_value(@mode.active_iter,0)
				paiement.tiers_id = @id_tiers
				paiement.compte_id = compte2
				paiement.montant = @montant_total
				paiement.numero = @num.text
				paiement.note = "Virement interne - #{@note.buffer.text}"
				paiement.date_paiement = Date.parse(@date.text)
				paiement.date_comptable = Date.parse(@date.text)
				paiement.fournisseur = false
				paiement.montant = @montant.value
				res = paiement.save
		
				self.destroy
			else
				@window.message_erreur "Le montant ne peut pas être nul."
			end
		else
			@window.message_erreur "Vous ne pouvez pas choisir le même compte crédité et débité."
		end
	end

end		

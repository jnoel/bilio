# coding: utf-8

module ReglementController
	
	def ReglementController.update_montant_regle_docs docs
		docs.each do |d|
			montant = 0.0
			paiements = Paiementdocument.where(:document_id => d)
			paiements.each do |p|
				montant += p.montant
			end
			doc = Document.find(d)
			doc.montant_regle = montant
			doc.save
		end
	end
	
	def ReglementController.update_paiement
		
	end
	
	def ReglementController.supprime_paiements	paiement_ids
		paiement_ids.each do |id|
			supprime_paiementdoc id
			Paiement.find(id).destroy
		end
		p "Suppression du réglement #{paiement_ids}"
	end
	
	def ReglementController.supprime_paiementdoc paiement_id		
		paiementdoc = Paiementdocument.where(:paiement_id => paiement_id)
		docs = []
		paiementdoc.each do |p|
			if p.destroy
				docs << p.document_id unless docs.include?(p.document_id)
			end
		end
		update_montant_regle_docs docs
	end 

end		

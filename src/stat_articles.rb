# coding: utf-8

class Stat_articles_box < Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
				
		@window = window
		@login = window.login
		
		@style = 1 # 1=pie, 2=bar
		@activer_combo = false
		@chemin = ""
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
		
	end
	
	def framebox
	
		@frame = Gtk::Frame.new
		@frame.label = "Statistiques"
		
		@vbox_corps = Gtk::Box.new :vertical, 3
		@frame.add @vbox_corps
		@vbox_corps.pack_start( en_tete, :expand => false, :fill => false, :padding => 3 )		
		@vbox_corps.pack_start( corps, :expand => true, :fill => true, :padding => 3 )		
	
		return @frame
	
	end
	
	def en_tete
	
		vbox = Gtk::Box.new :vertical, 3
		hbox1 = Gtk::Box.new :horizontal, 3
		hbox2 = Gtk::Box.new :horizontal, 3
		
		@annee = Gtk::ComboBoxText.new
		remplir_annee
		@annee.active = 0
		
		@recette = Gtk::ComboBoxText.new
		@recette.append_text "Recettes"
		@recette.append_text "Dépenses"
		@recette.active = 0
		
		@qtite = Gtk::ComboBoxText.new
		@qtite.append_text "Quantité"
		@qtite.append_text "CA"
		@qtite.active = 1
		
		@groupe_model = Gtk::TreeStore.new(Integer, String)
		@groupe = Gtk::ComboBox.new :model => @groupe_model
		renderer_left = Gtk::CellRendererText.new
		@groupe.pack_start renderer_left, :expand => true, :fill => true, :padding => 3
		@groupe.add_attribute renderer_left, "text", 1
		remplir_groupe
		
		@refresh = Gtk::Button.new
		@refresh.image = Gtk::Image.new :file => "resources/icons/refresh.png"
		
		@graph_style_1 = Gtk::ToggleButton.new
		@graph_style_1.image = Gtk::Image.new :file => "resources/icons/pie_1.png"
		@graph_style_1.tooltip_text = "Camembert"
		@graph_style_2 = Gtk::ToggleButton.new
		@graph_style_2.image = Gtk::Image.new :file => "resources/icons/bar_1.png"
		@graph_style_2.tooltip_text = "Histogramme"
		@graph_style_3 = Gtk::ToggleButton.new
		@graph_style_3.image = Gtk::Image.new :file => "resources/icons/pie_3.png"
		@graph_style_3.tooltip_text = "Graphique éclaté"
		case @style
			when 1
				@graph_style_1.active = true
			when 2
				@graph_style_2.active = true
			when 3
				@graph_style_3.active = true
		end
		
		@imprimer = Gtk::Button.new
		@imprimer.image = Gtk::Image.new :file => "resources/icons/print.png"
		@imprimer.tooltip_text = "Imprimer"
		
		@annee.signal_connect('changed') {
			refresh if @activer_combo
		}
		
		@recette.signal_connect('changed') {
			refresh if @activer_combo
		}
		
		@qtite.signal_connect('changed') {
			refresh if @activer_combo
		}
		
		@groupe.signal_connect('changed') {
			refresh if @activer_combo
		}
		
		@refresh.signal_connect('clicked') {
			refresh if @activer_combo
		}
		
		@graph_style_1.signal_connect('toggled') {
			if @graph_style_1.active?
				@style = 1
				@graph_style_2.active = false
				@graph_style_3.active = false
				refresh if @activer_combo
			end
		}
		
		@graph_style_2.signal_connect('toggled') {
			if @graph_style_2.active?
				@style = 2
				@graph_style_1.active = false
				@graph_style_3.active = false
				refresh if @activer_combo
			end
		}
		
		@graph_style_3.signal_connect('toggled') {
			if @graph_style_3.active?
				@style = 3
				@graph_style_2.active = false
				@graph_style_1.active = false
				refresh if @activer_combo
			end
		}
		
		@imprimer.signal_connect('clicked') {
			imprimer if @activer_combo
		}
		
		combo_sensitive false
		
		hbox1.pack_start( Gtk::Label.new("Année :"), :expand => false, :fill => false, :padding => 3)
		hbox1.pack_start( @annee, :expand => true, :fill => true, :padding => 3 ) 
		hbox1.pack_start( Gtk::Label.new("Recettes ou dépenses :"), :expand => false, :fill => false, :padding => 3)
		hbox1.pack_start( @recette, :expand => true, :fill => true, :padding => 3 ) 
		hbox1.pack_start( Gtk::Label.new("Qtité ou CA :"), :expand => false, :fill => false, :padding => 3)
		hbox1.pack_start( @qtite, :expand => true, :fill => true, :padding => 3 ) 
		hbox1.pack_start( Gtk::Label.new("Groupe :"), :expand => false, :fill => false, :padding => 3)
		hbox1.pack_start( @groupe, :expand => true, :fill => true, :padding => 3 ) 
		hbox1.pack_start( @refresh, :expand => false, :fill => false, :padding => 3)
		
		hbox2.add @graph_style_1
		#hbox2.add @graph_style_3
		hbox2.add @graph_style_2
		
		hbox2.add Gtk::Label.new "-"
		hbox2.add @imprimer
		
		align = Gtk::Alignment.new 1, 0, 0, 0
		align.add hbox2
	
		vbox.add hbox1
		#vbox.add align
		
		vbox
	
	end
	
	def corps
		
		@graph = GraphBar.new absolu=true
		scroll = Gtk::ScrolledWindow.new
		scroll.set_policy(:automatic, :automatic)
		scroll.add_with_viewport @graph
		
		scroll
	
	end
	
	def cacule_graph style=1, groupe=0, annee=nil, recette=true, qtite=true
		
		combo_sensitive false
		
		if groupe.eql?(0)
			select_groupe = "(CASE WHEN T2.parent_id=0 THEN T2.id ELSE T2.parent_id END)"
			where_groupe = "2>1"
		else
			select_groupe = "T2.id"
			where_groupe = "T2.parent_id=#{groupe}"
		end
		
		select_total = (qtite ? "SUM(T0.qtite*T0.colisage)" : "SUM(T0.total_ligne)")
	
		where_type = (recette ? "T3.documenttype_id=4" : "T3.documenttype_id=8")
	
		where_annee = (annee.nil? ? "EXTRACT(YEAR FROM T3.date_document)=#{@annee.active_text}" : "EXTRACT(YEAR FROM T3.date_document)=#{annee}")
	
		req = 	"	
							SELECT #{select_groupe} AS groupe, #{select_total} AS total
							FROM documentlignes T0
							INNER JOIN documents T3
							ON T0.document_id=T3.id
							INNER JOIN articles T1
							ON T0.article_id=T1.id
							LEFT JOIN articlegroupes T2
							ON T1.articlegroupe_id=T2.id
							WHERE #{where_type} AND #{where_groupe} AND #{where_annee}
							GROUP BY groupe
							ORDER BY groupe
						"
	
		res = Documentligne.find_by_sql(req)
	
		if res then
			affiche_graph res, qtite
		end
	
	end
	
	def affiche_graph res, qtite
	
		serie = []
		legende = []
		couleur = []
		
		res.each {|row|	
			if row["groupe"].nil? then
				serie << [0.0]
				legende << "Sans groupe"
				couleur << [rand,rand,rand]
			else
				g = Articlegroupe.find(row["groupe"].to_i)
				serie << [row.total.to_f]
				legende << g.designation
				if !g.couleur.to_s.empty?
					coul = g.couleur.split(',')
				else
					coul = [rand,rand,rand]
				end
				couleur << [coul[0].to_f,coul[1].to_f,coul[2].to_f]
			end
		}
		
		unite = (qtite ? ["","unité(s)"] : ["","€"])
		
		Thread.new { 
			@graph.refresh serie, couleur, legende, unite
			combo_sensitive true
			@activer_combo = true
		}
	
	end
		
	def remplir_groupe nom=nil
		
		@groupe.model.clear
		
		iter = @groupe_model.append nil
		iter[0] = 0
		iter[1] = "Tous"
		active_iter = iter
		
		groupesp = Articlegroupe.where(:parent_id => 0).order(:designation)
		
		if groupesp then
			groupesp.each do |gr|
				gr_enfant = Articlegroupe.where(:parent_id => gr.id)
				if gr_enfant.count>1
					iter = @groupe_model.append nil
					iter[0] = gr.id
					iter[1] = gr.designation	
					if !nom.nil?
						active_iter = iter if nom.eql?(gr.id)		
					end
				end
			end
		
			@groupe.active_iter = active_iter
		else
			@window.message_erreur groupesp.error
		end
		
	end
	
	def refresh
		annee = (@annee.active.eql?(-1) ? DateTime.now.strftime("%Y") : @annee.active_text)
		recette = (@recette.active.eql?(0) or @recette.active.eql?(-1))
		groupe = (@groupe.active_iter ? @groupe.model.get_value(@groupe.active_iter, 0) : 0)
		qtite = (@qtite.active.eql?(0) or @qtite.active.eql?(-1))
		cacule_graph @style, groupe, annee, recette, qtite
	end
	
	def combo_sensitive status
		@annee.sensitive = status
		@recette.sensitive = status
		@groupe.sensitive = status
		@refresh.sensitive = status
		@qtite.sensitive = status
		@imprimer.sensitive = status
		@graph_style_1.sensitive = status unless (status and @graph_style_1.active?)
		@graph_style_2.sensitive = status unless (status and @graph_style_2.active?)
		@graph_style_3.sensitive = status unless (status and @graph_style_3.active?)
	end
	
	def remplir_annee
		
		doc = Document.find_by_sql("SELECT MIN(date_document) AS min, MAX(date_document) AS max FROM documents").first
		if doc.min and doc.max
			min = doc.min.year
			max = doc.max.year
			(min..max).to_a.reverse.each do |a|
				@annee.append_text a.to_s
			end
		end
		
	end
	
	def imprimer
	
		tmp = @window.config_db.chemin_temp+'/stat.pdf'
		EditionGraph.new(@chemin, tmp, @window)	if File.exist?(@chemin)
		
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open","#{tmp}"
		else
			if RUBY_PLATFORM.include?("mingw") then 
				system "start","#{tmp}"
			else
				
			end
		end
	
	end
	
end

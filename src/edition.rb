# coding: utf-8

class Edition < Prawn::Document
	
	attr_reader :fichier
	
	def initialize ids, chemin_temp, window
	
		super(	:page_layout => :portrait,
				:right_margin => 15,
				:page_size => 'A4'
			 )
		@window = window
		@societe = Societe.all.first 
		
		i=0
		ids.each { |id|
			preparation id
			i +=1
			start_new_page unless i.eql?(ids.count)
		}
		
		@fichier = "#{chemin_temp}/#{@document.ref}.pdf"
		rendu @fichier
	end
	
	def preparation id
		@document = Document.includes(:tiers).find(id)
		@documents_lignes = Documentligne.includes(:tva).where(:document_id => id).order(:id)
		@tva = Tva.all
	
		c = en_tete
		corps c
	end
	
	def en_tete
		
		pas = 10
		text_size = 10
		top = bounds.top-10
		
		top -= pas+35
		
		draw_text "Emetteur :", :at => [0,top], :size => 8, :styles => [:bold]
		draw_text "Adressé à :", :at => [250,top], :size => 8, :styles => [:bold]
		
		top -= pas
		
		fill_color "eeeeee"
		stroke_color "aaaaaa"
		rectangle [0,top+5], 230, 110
		fill
		rectangle [250,top+5], 290, 110
		stroke
		
		fill_color "000000"
		
		text_size = 10
		
		#repeat :all do
			# header
			bounding_box [bounds.left, bounds.top+30], :width  => bounds.width do
				societe = Societe.first
				logo = @window.config_db.conf["chemin_documents"].to_s+"/societe/"+societe.logo.to_s
				if File.exist?(logo)
					image logo, :height => 70 
				else
					move_down 70
				end
			end
			formatted_text_box([{ :text => "#{@document.documenttype.designation.capitalize}\n",
							  :size => 	text_size+4,
							  :styles => [:bold]
							},
				            { :text => "Réf. : #{@document.ref}\n",
							  :size => 	text_size+4,
							  :styles => [:bold]
							},
							{ :text => "Date #{@document.documenttype.designation.downcase} : #{@document.date_document.strftime("%d/%m/%Y")}\n",
				              :size => text_size
				            },
				            { :text => "Date échéance : #{@document.date_echeance.strftime("%d/%m/%Y")}\n",
				              :size => text_size
				            }],
				            { :at => [350,bounds.top+20],
				              :width => bounds.right-350,
				              :align => :right }
				            )	
				          	
    	#end
		
		formatted_text_box([{ :text => @societe.nom+"\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{@societe.adresse}\n\n",
				              :size => text_size,
				            },
				            { :text => "Téléphone: #{@societe.tel} - Fax: #{@societe.fax}\n",
				              :size => text_size,
				            },
				            { :text => "Email: #{@societe.mail}\n",
				              :size => text_size,
				            },
				            { :text => "Web: #{@societe.site}\n",
				              :size => text_size,
				            }],
				            {:at => [7,top]}
				            )
		
		formatted_text_box([{ :text => "#{@document.tiers.nom}\n\n",
							  :size => 	text_size+1,
							  :styles => [:bold]
							},
				            { :text => "#{@document.tiers.adresse}\n",
				              :size => text_size,
				            },
				            { :text => "#{@document.tiers.cp} #{@document.tiers.ville}",
				              :size => text_size,
				            }],
				            {:at => [257,top]}
				            )

		top = top-120
		
		if !@document.note.empty? then
			rectangle [0,top+4], 540, 15
			stroke
			draw_text @document.note, :at => [5,top-6], :size => 8
			return true
		else
			return false
		end
	
	end
	
	def corps note=false
		
		res_array = []
		remise = false
		@documents_lignes.each do |ligne|
			remise = true if ligne["remise"].to_i>0
		end
		@documents_lignes.each do |ligne|
			l = []
			des = ""
			des = ligne["designation"].to_s.gsub("\r", "") unless ligne["designation"].to_s.eql?("-")
			des += "\n" unless des.empty? or ligne["commentaires"].to_s.empty?
			des += ligne["commentaires"] unless ligne["commentaires"].to_s.empty?
			des += "\n" unless ligne["lot"].to_s.empty?
			des += "Lot/Série = #{ligne["lot"]}" unless ligne["lot"].to_s.empty?

			l << des
			l << ligne.tva.designation
			l << "%.2f €" % ligne["pu"]
			l << "%.2f" % (ligne["qtite"]*ligne["colisage"]).to_f
			l << (ligne["remise"].to_i>0 ? (ligne["remise"].to_i.eql?(100) ? "Offert !" : "%.2f%" % ligne["remise"]) : "" ) if remise
			l << "%.2f €" % ligne["total_ligne"]
			res_array << l
		end
		i = note ? 12 : 10
		i.times do text("  ") end
		data = [[]]
		#data[0] << "Code"
		data[0] << "Désignation"
		data[0] << "TVA"
		data[0] << "P.U. HT"
		data[0] << "Qté"
		data[0] << "Remise %" if remise
		data[0] << "Total HT"
  		data += res_array
  		
  		taille_colonnes = []
  		taille_colonnes << (remise ? 290 : 340)
  		taille_colonnes << 50
  		taille_colonnes << 50
  		taille_colonnes << 30
  		taille_colonnes << 50 if remise
  		taille_colonnes << 70
  		
  		t = table(data) do |table|
  			table.header = true
  			table.row(0).style :borders => [:top, :bottom, :left, :right]
  			table.row(0).style :align => :center
  			table.row(1..data.count).style :borders => [:left, :right]
  			#table.row(data.count-1).style :borders => [:left, :right, :bottom]
  			table.cell_style = { :size => 8, :border_color => "aaaaaa"}
  			table.column(1..5).align = :right
  			table.row(0).style :align => :center
  			table.column(0).align = :left
  			table.column_widths = taille_colonnes
		end 
		
		top = cursor
		
		left = bounds.left
		bottom = 200
		if top<bottom then
			bottom = 14
		end
		line [left,top], [left,bottom]
		taille_colonnes.each { |i|
			left += i
			line [left,top], [left,bottom]
		}
		line [bounds.left,bottom], [left,bottom]
		stroke
  		
  		if bottom<200 then
  			start_new_page
  		end
  		
  		pieds @documents_lignes
	
	end
	
	def pieds res

		ht = 0.0
		tva = 0.0
		
		tva_tab = []
		@tva.each { |t|
			tva_tab << {:id=>t['id'],:designation=>t['designation'], :taux=>t['taux'].to_f, :np=>t['np'], :ht=>0.0}
		}

		res.each { |ligne|
			ht += ligne["total_ligne"].to_f unless ligne["total_ligne"].nil?
			tva += ligne["total_ligne"].to_f*(ligne.tva.taux.to_f/100) unless ligne["total_ligne"].nil? or ligne.tva_id.nil? or ligne.tva.np
			tva_tab.each { |t|
				t[:ht] += ligne["total_ligne"].to_f if t[:id].eql?(ligne.tva_id)
			}
		}
		
		ht = ht.round(2)
		tva = tva.round(2)	
		ttc = (ht+tva).round(2)	
		
		top = 200
		text_size = 8
		
		data = []
		
		data << ["Total HT", "%.2f €" % ht]
		
		tva_tab.each { |t|
			texte = "Total TVA #{t[:taux]}% "
			texte += (t[:np] ? "(Non perçue réc.)" : "")
			montant = t[:ht]*t[:taux]/100
			data << [texte, "%.2f €" % montant.round(2)] if montant>0
		}		
		
		row_ttc = data.count
		data << ["Total TTC", "%.2f €" % ttc]
		
		if @document.montant_regle>0
			data << ["Payé", "%.2f €" % @document.montant_regle]
			data << ["Reste à payer", "%.2f €" % (ttc-@document.montant_regle)]
		end
		
		bounding_box([310,top], :width => 300, :height => 200) do
			
			table(data, :cell_style => { :borders => [], :padding => 1, :size => text_size+2 }) do |table|
	  			table.column(0).align = :left
	  			table.column(1).align = :right
	  			table.row(row_ttc).style :background_color => "eeeeee"
	  			table.row(data.count-1).style :background_color => "eeeeee"
	  			table.column(0).width = 150
	  			table.column(1).width = 80
			end 
		end
		
		# Liste des paiements déjà effectués
		if @document.montant_regle>0 
			paiements = Paiementdocument.where(:document_id => @document.id)
			data = [["Date", "Mode", "Numéro", "Montant"]]
			paiements.each { |p|
				data << [p.paiement.date_paiement.strftime("%d/%m/%Y"), p.paiement.paiementtype.designation, p.paiement.numero, "%.2f €" % p.montant]
			}

			bounding_box([310,top-90], :width => 300) do	
				text "Règlements déjà effectués :", :size => text_size, :style => :bold
				table(data, :cell_style => { :borders => [], :padding => 1, :size => text_size-2 }) do |table|
					table.row(0).font_style = :bold
					table.column(0).width = 50
					table.column(1).width = 50
					table.column(2).width = 60
					table.column(3).width = 60
				end
			end
		end
		
		# Condition et mode de règlement
		compte = Compte.where(:defaut => true).first
		
		formatted_text_box([{ :text => "Condition de règlement: #{@document.conditionpaiement.designation}\n\n",
							  :size => 	text_size,
							  :styles => [:bold]
							},
				            { :text => "Règlement par chèque à l'ordre de #{@societe.nom}\n",
				              :size => text_size,
				              :styles => [:bold]
				            },
				            { :text => "envoyé au:\n",
				              :size => text_size,
				              :styles => [:bold]
				            },
				            { :text => "#{@societe.adresse}\n\n",
				              :size => text_size
				            },
				            { :text => "Règlement par virement sur le compte bancaire suivant:\n",
				              :size => text_size,
				              :styles => [:bold]
				            },
				            { :text => (compte ? "Banque: #{compte.banque}\n" : ""),
				              :size => text_size-2
				            }],
				            {:at => [0,top-5]}
				            )
		if compte
			data = [["Code Banque", "Code guichet", "Numéro compte", "Clé RIB"]]
			data << [compte.code_banque, compte.code_guichet, compte.num_compte, compte.cle_rib]
		
			top = top - 97
		
			bounding_box([0,top], :width => 250, :height => 50) do
				table(data, :cell_style => { :borders => [:left, :right], :padding => 1, :size => text_size-2 }) do |table|
						table.column(0..3).align = :center
						table.column(0..1).width = 50
						table.column(2).width = 60
						table.column(3).width = 30
				end
			end
		
			formatted_text_box([{ :text => "Domiciliation: #{compte.domiciliation}\n\n",
															:size => 	text_size-2
														},
												    { :text => "Code IBAN: #{compte.iban}\n",
												      :size => text_size-2
												    },
												    { :text => "Code BIC/SWIFT: #{compte.bic}\n",
												      :size => text_size-2
												    }],
												    {:at => [0,top-25]}
												    )
			
			end
		
	end
	
	def rendu fichier
	
		number_pages "#{@societe['status']} - SIRET: #{@societe['siret']}", {:start_count_at => 1, :at => [0, 10], :align => :center, :size => 8}
		number_pages "NAF, ex APE: #{@societe['naf']} - RCS/RM: #{@societe['rcs']}", {:start_count_at => 1, :at => [0, 0], :align => :center, :size => 8}
		#number_pages "<page>/<total>", {:start_count_at => 1, :at => [bounds.right - 100, 0], :align => :right, :size => 8}
		render_file fichier
	
	end
	
end

# coding: utf-8

class DialGestionDb < Gtk::Dialog
	
	def initialize window
		
		super(:title => "Gestion des bases de données", :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::OK, :ok ]])
		
		set_default_size 500, 500
		
		@window = window
		
		hbox = Gtk::Box.new :horizontal, 2
		hbox.pack_start tableau_bases, :expand => true, :fill => true, :padding => 3
		hbox.pack_start boutons, :expand => false, :fill => false, :padding => 3
		self.child.pack_start hbox, :expand => true, :fill => true, :padding => 3
		self.child.show_all
		
		remplir_bases
		
	end
	
	def tableau_bases
		
		@bases = Gtk::TreeView.new Gtk::TreeStore.new(Integer, String, String, String, String, String, String, String)
		@bases.signal_connect ("row-activated") { |view, path, column|
			param_base @bases.model.get_value @bases.selection.selected, 0
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Nom", renderer_left, :text => 7)
		col.sort_column_id = 7
		@bases.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Type de base", renderer_left, :text => 1)
		col.sort_column_id = 1
		#@bases.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Hôte", renderer_left, :text => 2)
		col.sort_column_id = 2
		@bases.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Base", renderer_left, :text => 3)
		col.sort_column_id = 3
		@bases.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	scroll.add @bases
    	
	end
	
	def boutons
		
		vbox = Gtk::Box.new :vertical, 3
		
		add = Gtk::Button.new :label => "Nouvelle base"
		add.signal_connect( "clicked" ) {
			param_base -1
		}
		remove = Gtk::Button.new :label => "Retirer la base"
		remove.signal_connect( "clicked" ) {
			supprimer_base @bases.model.get_value @bases.selection.selected, 0
		}
		
		vbox.pack_start add, :expand => false, :fill => false, :padding => 3
		vbox.pack_start remove, :expand => false, :fill => false, :padding => 3
		
		vbox
		
	end
	
	def supprimer_base id
		if id
			if MessageController.question_oui_non @window, "Voulez-vous réellement retirer cette base ?"
				conf = @window.config_db.bases[id]
				@window.config_db.bases.delete conf
				@window.config_db.save_config global=true
				remplir_bases
				@window.login.refresh
			end
		end
	end
	
	def remplir_bases
		
		@bases.model.clear
		
		bases = @window.config_db.bases
		
		if bases
			i=0
			bases.each do |b|
				iter = @bases.model.append nil
				iter[0] = i
				iter[1] = b["adapter"]
				iter[2] = b["host"]
				iter[3] = b["database"]
				iter[4] = b["port"]
				iter[5] = b["username"]
				iter[6] = b["password"]
				iter[7] = b["nom"].to_s
				i += 1
			end
		end
		
	end
	
	def param_base id
	
		dial_param_db = DialParamDb.new @window, id
		dial_param_db.run { |response| 
			if response==-5
				if id>-1
					base = @window.config_db.bases[id]
					base["nom"] = dial_param_db.nom.text
					base["host"] = dial_param_db.host.text
					base["database"] = dial_param_db.database.text
					base["port"] = dial_param_db.port.text
					base["username"] = dial_param_db.username.text
					base["password"] = dial_param_db.password.text
					base["chemin_documents"] = dial_param_db.chemin_documents.text
					base["default_user"] = ""
					base["default_user_password"] = ""
				else
					id = 0
					if @window.config_db.bases
						id = @window.config_db.bases.count
					else
						@window.config_db.bases = []
					end
					base = {  "nom" => dial_param_db.nom.text,
										"adapter" => "postgresql",
										"host" => dial_param_db.host.text,
										"database" => dial_param_db.database.text,
										"port" => dial_param_db.port.text,
										"username" => dial_param_db.username.text,
										"password" => dial_param_db.password.text,
										"chemin_documents" => dial_param_db.chemin_documents.text,
										"default_user" => "",
										"default_user_password" => "",
										"id" => id
								  }
					@window.config_db.bases << base
				end
				@window.config_db.save_config global=true
				remplir_bases
				@window.login.refresh
			end
			dial_param_db.destroy 			
		}
	
	end

end

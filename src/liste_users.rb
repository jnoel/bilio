# coding: utf-8

class ListeUsers_box < Gtk::Box

	attr_accessor :ajout
	
	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@user = @window.login.user
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
		
		remplir_users
	
	end
	
	def framebox
		
		vboxaction = Gtk::Box.new :vertical, 3
		vboxaction.pack_start( boutons, :expand => false, :fill => false, :padding => 3 ) 
		vboxaction.pack_start( tableau_users, :expand => true, :fill => true, :padding => 3 ) 
		frame = Gtk::Frame.new
		frame.add vboxaction
		frame.label = "Liste des utilisateurs"
		
		frame
		
	end
	
	def boutons
	
		align = Gtk::Alignment.new 1, 1, 0, 0
		
		hbox = Gtk::Box.new :horizontal, 2
		
		@changepassword = Gtk::Button.new
		hboxsuppr = Gtk::Box.new :horizontal, 2
		hboxsuppr.add Gtk::Image.new( :file => "./resources/icons/password.png" )
		hboxsuppr.add Gtk::Label.new "Changer le mot de passe"
		@changepassword.add hboxsuppr	
		@changepassword.sensitive = false
		@changepassword.signal_connect ("clicked") {
			change_password ( @view.model.get_value @view.selection.selected, 0 )
		}
		
		@modif = Gtk::Button.new
		hboxsuppr = Gtk::Box.new :horizontal, 2
		hboxsuppr.add Gtk::Image.new( :file => "./resources/icons/saisie.png" )
		hboxsuppr.add Gtk::Label.new "Mofifier utilisateur"
		@modif.add hboxsuppr	
		@modif.sensitive = false
		@modif.signal_connect ("clicked") {
			modif_user ( @view.model.get_value @view.selection.selected, 0 )
		}
		
		@ajout = Gtk::Button.new
		@ajout.sensitive = false
		hboxajout = Gtk::Box.new :horizontal, 2
		hboxajout.add Gtk::Image.new( :file => "./resources/icons/add.png" )
		hboxajout.add Gtk::Label.new "Nouvel utilisateur"
		@ajout.add hboxajout		
		@ajout.signal_connect ("clicked") {
			modif_user 0
		}
		
		hbox.pack_start(@changepassword, :expand => false, :fill => false, :padding => 3)
		hbox.pack_start(@modif, :expand => false, :fill => false, :padding => 3)
		hbox.pack_start(@ajout, :expand => false, :fill => false, :padding => 32)
		
		align.add hbox
		
		align
	
	end
	
	def tableau_users
	
		@list_store = Gtk::TreeStore.new(Integer, String, String, String, Integer, Integer)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			modif_user ( @view.model.get_value @view.selection.selected, 0 ) if ( @user.admin )
		}
		
		@view.signal_connect ("cursor-changed") {
			@changepassword.sensitive = ( @user.id.eql?( @view.model.get_value( @view.selection.selected, 0 ) ) or @user.admin )
			@modif.sensitive = @user.admin
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0	
		
		renderer_toggle_admin = Gtk::CellRendererToggle.new
		renderer_toggle_active = Gtk::CellRendererToggle.new
		
		col = Gtk::TreeViewColumn.new("Prénom", renderer_left, :text => 1)
		col.sort_column_id = 0
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Nom", renderer_left, :text => 2)
		col.sort_column_id = 1
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Login", renderer_left, :text => 3)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Admin ?", renderer_toggle_admin, :active => 4)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Actif ?", renderer_toggle_active, :active => 5)
		col.sort_column_id = 4
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(:automatic, :automatic)
    	scroll.add @view
    	
    	scroll
	
	end
	
	def remplir_users

		users = Utilisateur.order(:id)
		
		@list_store.clear
		
		users.each do |h| 		
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.firstname
			iter[2] = h.lastname
			iter[3] = h.username
			iter[4] = h.admin ? 1 : 0
			iter[5] = h.active ? 1 : 0
		end
			
	end
	
	def modif_user id
	
		@window.utilisateur.change id
		@window.affiche @window.utilisateur
	
	end
	
	def change_password id
		
		dialog = Gtk::Dialog.new(:title => "Mot de pase",
                             :parent => @window,
                             :flags => :destroy_with_parent,
                             :buttons => [[ Gtk::Stock::OK, :ok ],
                            	 						[ Gtk::Stock::CANCEL, :cancel ]]
                             )

		vbox = Gtk::Box.new :vertical, 2
		hbox1 = Gtk::Box.new :horizontal, 2
		hbox2 = Gtk::Box.new :horizontal, 2
		vbox.add hbox1
		vbox.add hbox2
		hbox1.pack_start( Gtk::Label.new( "Nouveau mot de passe :" ), :expand => false, :fill => false, :padding => 3 )
		motpasse1 = Gtk::Entry.new
		motpasse1.visibility = false
		hbox1.pack_start( motpasse1, :expand => false, :fill => false, :padding => 3 )
		hbox2.pack_start( Gtk::Label.new( "Confirmer mot de passe :" ), :expand => false, :fill => false, :padding => 3 )
		motpasse2 = Gtk::Entry.new
		motpasse2.visibility = false
		hbox2.pack_start( motpasse2, :expand => false, :fill => false, :padding => 3 )
		
		dialog.child.add(vbox)
		vbox.show_all
		dialog.run { |response| 
			if response==Gtk::ResponseType::OK then
				if (motpasse1.text==motpasse2.text and !motpasse1.text.empty?) then
					password = BCrypt::Password.create(motpasse1.text)
					user = Utilisateur.find(id)
					user.password = password
					user.save
					MessageController.info @window, "Le mot de passe a été changé avec succés"
				else
					erreur = ""
					erreur += "Les mots de passe ne correspondent pas !\n" unless motpasse1.text==motpasse2.text
					erreur += "Le mot de passe ne peut pas être vide !\n" if motpasse1.text.empty?
					erreur += "Le mot de passe n'a pas été changé."
					MessageController.erreur @window, erreur
				end
			end
			dialog.destroy
		}	
	
	end
	
end

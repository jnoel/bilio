#!/usr/bin/env ruby
# coding: utf-8

# Utilisation de bundler pour gérer les gems ou pas ?
@bundle_bool = true

# Liste des dépendances externes
require 'rubygems' 
require 'bundler/setup' if @bundle_bool
require 'active_record'
require 'logger'
require 'gtk3'
require 'date'
require 'bcrypt'
require 'yaml'
require 'prawn'
require 'prawn/layout'
require 'ezcrypto'

# Dépendance des fichiers de l'application
require '../db/models.rb'
Dir.foreach('.') { |f|
	require "./#{f}" if File.extname(f).eql?(".rb")
}

# Titre de l'application
GLib.set_application_name "Bilio"

# Lancement de l'interface GTK
Gtk.init
win = MainWindow.new :toplevel
Gtk.main

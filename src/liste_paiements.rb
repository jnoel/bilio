# coding: utf-8

class ListePaiements_box < Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		
		@compteur = Gtk::Label.new
		
		hbox = Gtk::Box.new :horizontal, 3
		@search = Gtk::Entry.new
		@search.primary_icon_stock = Gtk::Stock::FIND
		@search.secondary_icon_stock = Gtk::Stock::CLEAR
		#hbox.pack_start @search, :expand => true, :fill => true, :padding => 3
		
		type_compte = Gtk::ListStore.new(Integer, String)
		@compte = Gtk::ComboBox.new :model => type_compte
		renderer_compte = Gtk::CellRendererText.new 
		@compte.pack_start(renderer_compte, true)
		@compte.set_attributes(renderer_compte, :text => 1)
		@compte.signal_connect("changed") { changement_compte } 
		remplir_compte
		hbox.pack_start Gtk::Label.new("Compte :"), :expand => false, :fill => false, :padding => 3
		hbox.pack_start @compte, :expand => false, :fill => false, :padding => 3
		
		@filtre = Gtk::ComboBoxText.new
		remplir_filtre
		hbox.pack_start Gtk::Label.new("Filtres :"), :expand => false, :fill => false, :padding => 3
		hbox.pack_start @filtre, :expand => false, :fill => false, :padding => 3
		
		type_model = Gtk::ListStore.new(Integer, String)
		@type = Gtk::ComboBox.new :model => type_model
		renderer_type = Gtk::CellRendererText.new 
		@type.pack_start(renderer_type, true)
		@type.set_attributes(renderer_type, :text => 1)
		@type.signal_connect("changed") { changement_type } 
		remplir_type
		hbox.pack_start Gtk::Label.new("Type :"), :expand => false, :fill => false, :padding => 3
		hbox.pack_start @type, :expand => false, :fill => false, :padding => 3
		
		@mois = Gtk::ComboBoxText.new
		@annee = Gtk::SpinButton.new 1900, 2900, 1
		@annee.digits = 0
		@annee.value = DateTime.now.year
		remplir_dates
		
		hbox.pack_start Gtk::Label.new("Mois :"), :expand => false, :fill => false, :padding => 3
		hbox.pack_start @mois, :expand => false, :fill => false, :padding => 3
		
		hbox.pack_start Gtk::Label.new("Année :"), :expand => false, :fill => false, :padding => 3
		hbox.pack_start @annee, :expand => false, :fill => false, :padding => 3
		
		@imprimer = Gtk::Button.new
		hbox_imprimer = Gtk::Box.new :horizontal, 2
		hbox_imprimer.add Gtk::Image.new( :file => "./resources/icons/print.png" )
		hbox_imprimer.add Gtk::Label.new "Imprimer"
		@imprimer.add hbox_imprimer
		
		hbox.pack_start @imprimer, :expand => false, :fill => false, :padding => 3
		
		@saisie_virement_interne = Gtk::Button.new 
		hbox_virement = Gtk::Box.new :horizontal, 2
		hbox_virement.add Gtk::Image.new( :file => "./resources/icons/add.png" )
		hbox_virement.add Gtk::Label.new "Virement interne"
		@saisie_virement_interne.add hbox_virement
		
		hbox.pack_start @saisie_virement_interne, :expand => false, :fill => false, :padding => 3
		
		@saisie_virement_interne.signal_connect("clicked") { saisie_virement_interne }
		
		vbox.pack_start hbox, :expand => false, :fill => false, :padding => 3
		vbox.pack_start tableau_documents, :expand => true, :fill => true, :padding => 3 
		vbox.pack_start @compteur, :expand => false, :fill => false, :padding => 3
		@frame = Gtk::Frame.new
		@frame.label = "Liste de paiements"
		@frame.add vbox
		
		@filtre.signal_connect('changed') {
			refresh nil
		}
		
		@search.signal_connect('activate') {
			if !@search.text.empty? then
				refresh @search.text
				@search.text = ""
			end
		}
		
		@mois.signal_connect('changed') {
			refresh nil
		}
		
		@annee.signal_connect('value-changed') {
			refresh nil
		}
		
		@imprimer.signal_connect('clicked') {
			imprimer
		}
		
		@mois.active = DateTime.now.month-1
		@type.active = 0
		@filtre.active = 0
		@compte.active = 0
		
		return @frame
	
	end
	
	def tableau_documents
		
		# list_store                    (id,       type,  tiers,   date,  datetri,montant,mtt_tri numero banque emetteur  note   compte)
		@list_store = Gtk::ListStore.new(Integer, String, String, String, String, String, Float, String, String, String, String, String)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			 	 
		}
		
		@view.signal_connect ("button-release-event") { |tree,event|
			# Gestion du clic droit sur une ligne
			if event.kind_of? Gdk::EventButton and event.button == 3
				npath = @view.get_path_at_pos(event.x, event.y)
				if !npath.nil? 
					@view.set_cursor(npath[0], nil, false)
					menu_paiement	@view.model.get_value(@view.selection.selected, 0)
				end
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Réf.", renderer_left, :text => 0)
		col.sort_column_id = 0
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Compte", renderer_left, :text => 11)
		col.sort_column_id = 0
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Type", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Tiers", renderer_left, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col)
		
		renderer_note = Gtk::CellRendererText.new
		renderer_note.xalign = 0
		renderer_note.yalign = 0
		renderer_note.mode = :editable
		renderer_note.editable = true
		renderer_note.signal_connect('edited') { |combo, data, text|
			ligne = @view.model.get_iter(data)
			ligne[10] = text
			paiement = Paiement.find(ligne[0])
			paiement.note = ligne[10]
			paiement.save
		} 
		col = Gtk::TreeViewColumn.new("Note", renderer_note, :text => 10)
		col.sort_column_id = 10
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Date", renderer_left, :text => 3)
		col.sort_column_id = 4
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Montant", renderer_right, :text => 5)
		col.sort_column_id = 6
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Numéro", renderer_left, :text => 7)
		col.sort_column_id = 7
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Banque", renderer_left, :text => 8)
		col.sort_column_id = 8
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Emetteur", renderer_left, :text => 9)
		col.sort_column_id = 9
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def menu_paiement id
	
		suppr_tb = Gtk::MenuItem.new "Supprimer la pièce"
		suppr_tb.signal_connect( "activate" ) { 
			supprime_paiement id
		}
		
		menu = Gtk::Menu.new
		menu.append suppr_tb
		menu.show_all
		menu.popup(nil, nil, 0, 0)
		
	end
	
	def refresh term=nil
		
		fourn = @filtre.active.eql?(1)
		
		type = @type.active_iter ? @type.model.get_value(@type.active_iter, 0) : -1
		compte = @compte.active_iter ? @compte.model.get_value(@compte.active_iter, 0) : -1
		
		paiements = Paiement.includes(:tiers, :paiementtype, :compte).order(:date_paiement)
		paiements = paiements.where("tiers.nom ILIKE ?", "%#{term}%") unless term.nil?
		date = "01/#{@mois.active+1}/#{@annee.value_as_int}"
		@datedeb = Date.parse(date)
		@datefin = Date.new(@datedeb.year, @datedeb.month, -1)
		paiements = paiements.where(:fournisseur => fourn, :date_paiement => @datedeb..@datefin)
		paiements = paiements.where(:paiementtype_id => type) unless type.eql?(-1)
		paiements = paiements.where(:compte_id => compte) unless compte.eql?(-1)
		
		remplir paiements
		
		@compteur.text = "Nombre d'éléments dans la liste : #{paiements.count}"
	
	end
	
	def remplir paiements
		# list_store                    (id,       type,  tiers,   date,  datetri,montant,mtt_tri numero banque emetteur note )
		@list_store.clear
		
		paiements.each do |p|
			iter = @list_store.append
			iter[0] = p.id
			iter[1] = p.paiementtype.designation
			iter[2] = p.tiers_id.nil? ? "-" : p.tiers.nom
			iter[3] = p.date_paiement.strftime("%d/%m/%Y")
			iter[4] = p.date_paiement.strftime("%Y-%m-%d")
			iter[5] = "%.2f €" % p.montant
			iter[6] = p.montant
			iter[7] = p.numero
			iter[8] = p.banque
			iter[9] = p.emetteur
			iter[10] = p.note
			iter[11] = p.compte.designation
		end
			
	end
	
	def remplir_filtre
		@filtre.model.clear
		@filtre.append_text "Paiements clients"	#0  
		@filtre.append_text "Paiements fournisseurs"	#1   
	end
	
	def remplir_compte
		@compte.model.clear
		comptes = Compte.order(:id)
		iter = @compte.model.append
		iter[0] = -1
		iter[1] = "Tous"
		comptes.each { |c|
			iter = @compte.model.append
			iter[0] = c.id
			iter[1] = c.designation
		}
	end
	
	def remplir_type
		@type.model.clear
		paiementtypes = Paiementtype.order(:id)
		iter = @type.model.append
		iter[0] = -1
		iter[1] = "Tous"
		paiementtypes.each { |t|
			iter = @type.model.append
			iter[0] = t.id
			iter[1] = t.designation
		}
	end
	
	def remplir_dates
		@mois.model.clear
		mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
		mois.each { |m|
			@mois.append_text m 
		}
	end
	
	def changement_type
		refresh nil
	end
	
	def changement_compte
		refresh nil
	end
	
	def imprimer
		chemin = @window.config_db.chemin_temp
		type = @type.active_iter ? @type.model.get_value(@type.active_iter, 0) : -1
		compte = @compte.active_iter ? @compte.model.get_value(@compte.active_iter, 0) : -1
		EditionPaiements.new @filtre.active, type, compte, @datedeb, @datefin, chemin
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open","#{chemin}/paiements.pdf"
		else
			if RUBY_PLATFORM.include?("mingw") then 
				system "start","#{chemin}/paiements.pdf"
			else
				
			end
		end
	end
	
	def saisie_virement_interne
		popup = Saisie_virement_interne.new @window
	  	popup.run
	  	refresh nil
	end
	
	def supprime_paiement id
		if MessageController.question_oui_non @window, "Voulez-vous réellement supprimer ce paiement ?"
			ReglementController.supprime_paiements [id]
			refresh nil
		end
	end
	
end

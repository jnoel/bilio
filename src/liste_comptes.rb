# coding: utf-8

class ListeComptes_box < Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		
		@add_new = Gtk::Button.new
		hboxajout = Gtk::Box.new :horizontal, 2
		hboxajout.add Gtk::Image.new( :file => "./resources/icons/add.png" )
		hboxajout.add Gtk::Label.new "Nouveau compte"
		@add_new.add hboxajout
		
		@add_new.signal_connect ("clicked") {
			@window.compte.refresh 0
			@window.affiche @window.compte
		}
		
		@compteur = Gtk::Label.new
		
		hbox = Gtk::Box.new :horizontal, 3
		@search = Gtk::Entry.new
		@search.primary_icon_stock = Gtk::Stock::FIND
		@search.secondary_icon_stock = Gtk::Stock::CLEAR
		hbox.pack_start @search, :expand => true, :fill => true, :padding => 3
		hbox.pack_start @add_new, :expand => false, :fill => false, :padding => 3
		
		vbox.pack_start hbox, :expand => false, :fill => false, :padding => 3
		vbox.pack_start tableau_comptes, :expand => true, :fill => true, :padding => 3
		vbox.pack_start @compteur, :expand => false, :fill => false, :padding => 3
		@frame = Gtk::Frame.new
		@frame.label = "Liste des comptes"
		@frame.add vbox
		
		@search.signal_connect('activate') {
			refresh
		}
		
		@search.signal_connect('icon-press') { |widget, pos, event|
			if !@search.text.empty?
				@search.text = ""
				refresh
			end
		}
		
		return @frame
	
	end
	
	def tableau_comptes
		
		# list_store                     (id,     Design,  banque, compte, solde ,soldetri)
		@list_store = Gtk::ListStore.new(Integer, String, String, String, String, Float)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			# TODO ouvrir la fenetre de compte
			@window.compte.refresh @view.model.get_value @view.selection.selected, 0
			@window.affiche @window.compte
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Compte", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Banque", renderer_left, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Numéro", renderer_left, :text => 3)
		col.sort_column_id = 3
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Solde", renderer_right, :text => 4)
		col.sort_column_id = 5
		col.resizable = true
		#@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	scroll.add @view
    	
	end
	
	def refresh term=nil
		
		term = @search.text unless (@search.text.empty? or term)
		comptes = Compte.order(:id)
		comptes = comptes.where("designation ILIKE ?", "%#{term}%") unless term.to_s.empty?
		
		remplir comptes
		
		@compteur.text = "Nombre d'éléments dans la liste : #{comptes.count}"
	
	end
	
	def remplir comptes
		# list_store                     (id,     Design,  banque, compte, solde ,soldetri)
		@list_store.clear
		
		comptes.each do |c|
			iter = @list_store.append
			iter[0] = c.id
			iter[1] = c.designation
			iter[2] = c.banque
			iter[3] = c.num_compte
			iter[4] = "%.2f €" % 0.0
			iter[5] = 0.0
		end
			
	end
	
end

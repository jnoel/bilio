# coding: utf-8

class TypeDocument < Hash
	
	def initialize window
		
		res = Documenttype.all

		res.each { |type|
			self.update Hash[ type.id, {:nom => type.designation, :code => type.code, :vente => type.vente} ]
		}
	
	end
	
end

# coding: utf-8

class DialNewArticle < Gtk::Dialog
	
	attr_reader :article_window
	
	def initialize window, id=0
		
		super(:title => "Nouvel article", :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])
		
		set_default_size 1000, 800
		 
		@article_window = Article_box.new window, true
		@article_window.refresh id
		
		self.child.pack_start @article_window, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all
	
	end

end		

# coding: utf-8

class EditionGraph < Prawn::Document
	
	def initialize graph, tmp, window
	
		super(	:page_layout => :landscape,
						:right_margin => 15,
						:page_size => 'A4'
					)
		
		#draw_text titre, :at => [0,bounds.top-10], :size => 16, :styles => [:bold]
		
		
		bounding_box [bounds.left, bounds.top], :width  => bounds.right do
			societe = Societe.first
			logo = window.config_db.conf["chemin_documents"]+"/societe/"+societe.logo
			image logo, :height => 70 if File.exist?(logo)
			move_down 20
			image graph, :width => bounds.right if File.exist?(graph)
		end
		
		rendu tmp
	
	end
	
	def rendu chemin_temp
	
		render_file "#{chemin_temp}"
	
	end
	
end

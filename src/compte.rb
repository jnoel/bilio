# coding: utf-8

class Compte_box < Gtk::Box

	def initialize window, dial=false
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@id = 0
		
		@window = window
		@login = window.login
		@dial = dial
		
		@valider = Gtk::Button.new
		hboxvalider = Gtk::Box.new :horizontal, 2
		hboxvalider.add Gtk::Image.new( :file => "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new "Valider"
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::Box.new :horizontal, 2
		hboxannuler.add Gtk::Image.new( :file => "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new "Annuler"
		@annuler.add hboxannuler
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		@frame = Gtk::Frame.new
		@frame.label = "Nouveau compte"
		vbox.pack_start( @frame, :expand => true, :fill => true, :padding => 3 )
		
		vbox_corps = Gtk::Box.new :vertical, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( notebook, :expand => true, :fill => true, :padding => 3 )		
		
		hbox3 = Gtk::Box.new :horizontal, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, :expand => true, :fill => true, :padding => 3 )
		hbox3.pack_start( @valider, :expand => true, :fill => true, :padding => 3 )
		align1.add hbox3
		
		vbox.pack_start( align1, :expand => false, :fill => false, :padding => 3 ) unless @dial
		
		return vbox
	
	end
	
	def notebook
	
		@note = Gtk::TextView.new
		@ged = Ged_box.new @window, "comptes"
		
		# Conteneurs
		notebook = Gtk::Notebook.new
		scroll_note = Gtk::ScrolledWindow.new

		# Propriétés des objets
		@note.wrap_mode = :word
		scroll_note.set_policy :never, :automatic
		scroll_note.shadow_type = :none
		
		# Agencement du conteneur Note
		scroll_note.add @note
		
		# Agencement du notebook
		notebook.append_page infos, Gtk::Label.new("Informations")
		notebook.append_page ecritures, Gtk::Label.new("Ecritures")
		notebook.append_page scroll_note, Gtk::Label.new("Notes")
		notebook.append_page @ged, Gtk::Label.new("GED")

		# Renvoie
		notebook
		
	end
	
	def infos
		# Objets	
		@designation = Gtk::Entry.new
		@banque = Gtk::Entry.new
		@code_banque = Gtk::Entry.new
		@code_guichet = Gtk::Entry.new
		@num_compte = Gtk::Entry.new
		@cle_rib = Gtk::Entry.new
		@bic = Gtk::Entry.new
		@iban = Gtk::Entry.new
		@domiciliation = Gtk::Entry.new
		@proprietaire = Gtk::Entry.new
		@adresse_proprietaire = Gtk::TextView.new
		@site = Gtk::Entry.new
		@solde_ouverture = Gtk::SpinButton.new 0, 9999999999, 1
		visite_site = Gtk::Button.new
		debloque_solde = Gtk::Button.new
		@defaut = Gtk::CheckButton.new
		
		# Conteneurs
		table_info = Gtk::Table.new 2, 4, false
		scroll_info = Gtk::ScrolledWindow.new
		hbox_compte = Gtk::Box.new :horizontal, 2
		
		# Les labels
		label_designation = Gtk::Alignment.new 0, 0.5, 0, 0
		label_designation.add Gtk::Label.new "Désignation du compte :"
		label_banque = Gtk::Alignment.new 0, 0.5, 0, 0
		label_banque.add Gtk::Label.new "Nom de la banque :"
		label_site = Gtk::Alignment.new 0, 0.5, 0, 0
		label_site.add Gtk::Label.new "Site web :"
		label_rib = Gtk::Alignment.new 0, 0.5, 0, 0
		label_rib.add Gtk::Label.new "RIB :"
		label_code_banque = Gtk::Alignment.new 0, 0.5, 0, 0
		label_code_banque.add Gtk::Label.new "Code banque :"
		label_code_guichet = Gtk::Alignment.new 0, 0.5, 0, 0
		label_code_guichet.add Gtk::Label.new "Code guichet :"
		label_num_compte = Gtk::Alignment.new 0, 0.5, 0, 0
		label_num_compte.add Gtk::Label.new "Numéro de compte :"
		label_cle_rib = Gtk::Alignment.new 0, 0.5, 0, 0
		label_cle_rib.add Gtk::Label.new "Clé :"
		label_bic = Gtk::Alignment.new 0, 0.5, 0, 0
		label_bic.add Gtk::Label.new "BIC :"
		label_iban = Gtk::Alignment.new 0, 0.5, 0, 0
		label_iban.add Gtk::Label.new "IBAN :"
		label_domiciliation = Gtk::Alignment.new 0, 0.5, 0, 0
		label_domiciliation.add Gtk::Label.new "Domiciliation :"
		label_proprietaire = Gtk::Alignment.new 0, 0.5, 0, 0
		label_proprietaire.add Gtk::Label.new "Propriétaire du compte:"
		label_solde_ouverture = Gtk::Alignment.new 0, 0.5, 0, 0
		label_solde_ouverture.add Gtk::Label.new "Solde à l'ouverture du compte:"
		label_defaut = Gtk::Alignment.new 0, 0.5, 0, 0
		label_defaut.add Gtk::Label.new "Compte par défaut:"
		
		# Propriétés des objets
		scroll_info.set_policy :never, :automatic
		scroll_info.shadow_type = :none
		visite_site.image = Gtk::Image.new :file => "resources/icons/internet.png"
		visite_site.signal_connect("clicked") { naviguer unless @site.text.empty? }
		visite_site.tooltip_text = "Visiter le site"
		debloque_solde.image = Gtk::Image.new :file => "resources/icons/lock.png"
		debloque_solde.signal_connect("clicked") { @solde_ouverture.sensitive = !@solde_ouverture.sensitive? }
		debloque_solde.tooltip_text = "Dé-vérouiller le solde"
		@code_banque.width_chars = 5
		@code_guichet.width_chars = 5
		@num_compte.width_chars = 11
		@cle_rib.width_chars = 2
		@solde_ouverture.digits = 2
		@solde_ouverture.sensitive = false
		
		# Agencement du conteneur Informations
		i=0
		table_info.attach( label_designation , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @designation, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_banque , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @banque, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_site , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @site, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		table_info.attach( visite_site, 2, 3, i, i+1, :fill, :fill, 5, 5 )
		
		i+=1
		hbox_compte.pack_start label_code_banque, :expand => false, :fill => false, :padding => 3
		hbox_compte.pack_start @code_banque, :expand => false, :fill => false, :padding => 3
		hbox_compte.pack_start label_code_guichet, :expand => false, :fill => false, :padding => 3
		hbox_compte.pack_start @code_guichet, :expand => false, :fill => false, :padding => 3
		hbox_compte.pack_start label_num_compte, :expand => false, :fill => false, :padding => 3
		hbox_compte.pack_start @num_compte, :expand => false, :fill => false, :padding => 3
		hbox_compte.pack_start label_cle_rib, :expand => false, :fill => false, :padding => 3
		hbox_compte.pack_start @cle_rib, :expand => false, :fill => false, :padding => 3
		table_info.attach( label_rib , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( hbox_compte , 1, 3, i, i+1, :fill, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_bic , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @bic, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_iban , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @iban, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_domiciliation , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @domiciliation, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_proprietaire , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @proprietaire, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_solde_ouverture , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @solde_ouverture, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		table_info.attach( debloque_solde, 2, 3, i, i+1, :fill, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_defaut , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @defaut, 1, 2, i, i+1, :fill, :fill, 5, 5 )
		
		scroll_info.add_with_viewport table_info
		
		scroll_info
	end
	
	def ecritures
	
		# list_store                    (id,     date, , valeur , type,  numero  note    tiers    débit   credit  solde   pointage)
		@list_store = Gtk::ListStore.new(Integer, String, String, String, String, String, String, String, String, String, String)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			 	 
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Date", renderer_left, :text => 1)
		col.resizable = true
		@view.append_column(col)
		
		renderer_date = Gtk::CellRendererText.new
		renderer_date.xalign = 0
		renderer_date.yalign = 0
		renderer_date.mode = :editable
		renderer_date.editable = true
		renderer_date.signal_connect('edited') { |combo, data, text|
			if (!text.empty? and !text.eql?(@list_store.get_iter(data)[2])) then
				begin
					date = Date.parse(text).strftime("%Y-%m-%d")
					@list_store.get_iter(data)[2] = text
					p = Paiement.find(@list_store.get_iter(data)[0])
					p.date_comptable = date
					p.save
					remplir_ecritures
				rescue
				end
  			end
		}  
		col = Gtk::TreeViewColumn.new("Valeur", renderer_date, :text => 2)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Type", renderer_left, :text => 3)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Numéro", renderer_left, :text => 4)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Note", renderer_left, :text => 5)
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Tiers", renderer_left, :text => 6)
		col.resizable = true
		@view.append_column(col)
		
		renderer_red = Gtk::CellRendererText.new
		renderer_red.background = "#ffe7e7"
		col = Gtk::TreeViewColumn.new("Débit", renderer_red, :text => 7)
		col.resizable = true
		@view.append_column(col)
		
		renderer_green = Gtk::CellRendererText.new
		renderer_green.background = "#e7ffe7"
		col = Gtk::TreeViewColumn.new("Crédit", renderer_green, :text => 8)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Solde", renderer_right, :text => 9)
		col.resizable = true
		@view.append_column(col)
		
		renderer_pointage = Gtk::CellRendererText.new
		renderer_pointage.xalign = 0
		renderer_pointage.yalign = 0
		renderer_pointage.mode = :editable
		renderer_pointage.editable = true
		renderer_pointage.signal_connect('edited') { |combo, data, text|
			if (!text.empty? and !text.eql?(@list_store.get_iter(data)[10])) then
				@list_store.get_iter(data)[10] = text
				p = Paiement.find(@list_store.get_iter(data)[0])
				p.pointage = text
				p.save
  			end
		}
		col = Gtk::TreeViewColumn.new("Pointage", renderer_pointage, :text => 10)
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(:automatic, :automatic)
    	scroll.add @view
    	
    	scroll
	
	end
	
	def refresh id=0
	
		@id = id
		@designation.text = ""
		@banque.text = ""
		@code_banque.text = ""
		@code_guichet.text = ""
		@num_compte.text = ""
		@cle_rib.text = ""
		@bic.text = ""
		@iban.text = ""
		@domiciliation.text = ""
		@proprietaire.text = ""
		@adresse_proprietaire.buffer.text = ""
		@site.text = ""
		
		if id>0 then
			
			@compte = Compte.find(id)
			if @compte then
				@frame.label = @compte.designation.to_s
				@designation.text = @compte.designation.to_s
				@banque.text = @compte.banque.to_s
				@code_banque.text = @compte.code_banque.to_s
				@code_guichet.text = @compte.code_guichet.to_s
				@num_compte.text = @compte.num_compte.to_s
				@cle_rib.text = @compte.cle_rib.to_s
				@bic.text = @compte.bic.to_s
				@iban.text = @compte.iban.to_s
				@domiciliation.text = @compte.domiciliation.to_s
				@proprietaire.text = @compte.proprietaire.to_s
				@adresse_proprietaire.buffer.text = @compte.adresse_proprietaire.to_s
				@site.text = @compte.site.to_s
				@solde_ouverture.value = @compte.solde_ouverture.to_f
				@defaut.active = @compte.defaut
			else
				@window.message_erreur tiers.error
			end
			
		else
			@compte = Compte.new
			@frame.label = "Nouveau Compte"
			@defaut.active = false
		end
		
		@ged.refresh @id
		remplir_ecritures
	
	end
	
	def remplir_ecritures
		# list_store                    (id,     date, , valeur , type,  numero  note    tiers    débit   credit  solde pointage)
		@list_store.clear
		paiements = Paiement.includes(:tiers, :paiementtype).where(:compte_id => @id, :suppr => false).order(:date_comptable)
		solde = @solde_ouverture.value.to_f
		paiements.each do |p|
			iter = @list_store.append
			iter[0] = p.id
			iter[1] = p.date_paiement.strftime("%d/%m/%Y")
			iter[2] = p.date_comptable.strftime("%d/%m/%Y")
			iter[3] = p.paiementtype.designation
			iter[4] = p.numero
			iter[5] = p.note
			iter[6] = p.tiers_id.nil? ? "-" : p.tiers.nom
			iter[7] = p.fournisseur ? ("%.2f €" % p.montant) : ""
			iter[8] = !p.fournisseur ? ("%.2f €" % p.montant) : ""
			solde = p.fournisseur ? solde-p.montant : solde+p.montant
			iter[9] = "%.2f €" % solde
			iter[10] = p.pointage.to_s
		end
			
	end
	
	def validate quitter=true
		
		res = false
		if !@designation.text.empty? then
			@compte.designation = @designation.text
			@compte.banque = @banque.text
			@compte.code_banque = @code_banque.text
			@compte.code_guichet = @code_guichet.text
			@compte.num_compte = @num_compte.text
			@compte.cle_rib = @cle_rib.text
			@compte.bic = @bic.text
			@compte.iban = @iban.text
			@compte.domiciliation = @domiciliation.text
			@compte.proprietaire = @proprietaire.text
			@compte.adresse_proprietaire = @adresse_proprietaire.buffer.text
			@compte.site = @site.text
			@compte.solde_ouverture = @solde_ouverture.value.to_f
			
			if @defaut.active? and !@compte.defaut
				Compte.update_all(:defaut => false)
				@compte.defaut = @defaut.active?
			end
						
			res = @compte.save
		
			if res then
				quit if (!@dial and quitter)
			else
				@window.message_erreur res.error
			end
		else
			@window.message_erreur "Vous devez saisir une désignation pour ce compte !"
			res = false
		end
		return res
	end
	
	def focus
		@annuler.grab_focus
	end
	
	def naviguer
		
		if !@site.text.include?("http://")
			@site.text = "http://" + @site.text
		end
		
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open #{@site.text}" 
		else
			if RUBY_PLATFORM.include?("mingw") then 
				# TODO
			else
				
			end
		end
	end
	
	def quit
	
		@window.liste_compte.refresh
		@window.affiche @window.liste_compte
	
	end
	
end

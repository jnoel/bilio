# coding: utf-8

class ConfigDb

	attr_accessor :fichier, :conf, :bases, :scanner, :chemin_config, :chemin_log, :chemin_temp
	
	def initialize window
	
		@window = window
		@chemin_temp = Dir.home+"/.bilio"
		Dir.mkdir(@chemin_temp) unless Dir.exist?(@chemin_temp)
		@chemin_config = @chemin_temp + "/configuration.yaml"
		@chemin_log = @chemin_temp + "/database.log"
		@fichier = {"connexions" => [], "scanner" => {}}
		@conf = {}
		@scanner = {}
		@bases = []
		charge_fichier @chemin_config
		
	end
	
	def charge_fichier chemin_config
	
		if File.exist?(chemin_config)
			MessageController.text "Ouverture du fichier de configuration #{chemin_config}."
			@fichier = YAML.load_file(@chemin_config) if YAML.load_file(@chemin_config)
			@bases = @fichier["connexions"]
			@scanner = @fichier["scanner"]
		else
			MessageController.text "Le fichier de configuration #{chemin_config} n'existe pas, il doit être créé."
		end
		
	end
	
	def connexion id
	
		@conf = @bases[id]
		@conf["id"] = id
			MessageController.text "Connexion à la base de donnée #{@conf['database']} sur le serveur #{@conf['adapter']} #{@conf['host']}:#{@conf['port']}."
			ActiveRecord::Base.logger = Logger.new(File.open(@chemin_log,'w'))
			ActiveRecord::Base.establish_connection(@conf)
			
			lancement = false
			begin
				last_version = 0
				versions = Schema_migration.order(:version)
				versions.each { |v|
					last_version = v.version.to_i>last_version ? v.version.to_i : last_version
				}
				
				if ActiveRecord::Base.connected?
					MessageController.text "Connexion établie. Version de la base de donnée = #{last_version}"
					last_migrate = 0
					Dir.foreach("../db/migrate") {|x| 
						num = x.to_s[0..2].to_i
						last_migrate=num if num>last_migrate 
					}
					MessageController.text "Dernier numéro de migration = #{last_migrate}"
					if last_version<last_migrate.to_i
						creation_base
					elsif last_version.eql?(last_migrate.to_i)
						lancement = true
					else
						MessageController.erreur @window, "Votre logiciel est trop ancien par rapport à la base de donnée, veuillez le mettre à jour"
						Process.exit
					end
				else
					MessageController.erreur @window, "Connexion impossible!\nVeuillez vérifier les paramètres de la base de données."
				end
			rescue
				begin
					if versions.nil?
						begin
							base = ActiveRecord::Base.connection.current_database
							MessageController.text "La base de données existe mais elle est vide"
							creation_base
						rescue
							@window.message_erreur "Connexion impossible!\nVeuillez vérifier les paramètres de la base de données."
						end
					elsif versions.length.eql?(0)
						MessageController.text "La table Schema_migration existe mais elle est vide."
						creation_base
					end
				rescue
					MessageController.text "La base de données est vide ou innaccessible... Nous allons tenter une migration."
					creation_base
				end
				
			end
		
			return lancement

	end
	
	def creation_base
	
		MessageController.text "Lancement de la migration"
		dial_create_db = DialCreateDb.new @window, @conf["id"]
		dial_create_db.run { |response| 
			dial_create_db.destroy 			
		}
		
		connexion @conf["id"]
	
	end
	
	def save_config global=false
		
		@fichier["connexions"] = @bases
		@fichier["scanner"] = @scanner
		
		File.open(@chemin_config, 'w') do |out|
   			YAML.dump(@fichier, out)
		end

	end
	
end

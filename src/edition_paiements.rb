# coding: utf-8

class EditionPaiements < Prawn::Document
	
	def initialize filtre, type, compte, datedeb, datefin, chemin, type_paiement=nil
	
		super(	:page_layout => :portrait,
				:right_margin => 15,
				:page_size => 'A4'
			 )
		
		@datedeb = datedeb
		@datefin = datefin
		@fourn = filtre.eql?(1)
		
		p = Paiementtype.find(type) if type>-1
		@type_paiement = (p ? p.designation : nil)

		@paiements = Paiement.includes(:tiers, :paiementtype).order(:date_paiement)
		@paiements = @paiements.where(:fournisseur => @fourn, :date_paiement => @datedeb..@datefin)
		@paiements = @paiements.where(:paiementtype_id => type) unless type.eql?(-1)
		@paiements = @paiements.where(:compte_id => compte) unless compte.eql?(-1)
		
		@societe = Societe.find(1)
		@banque = Compte.find(compte) if compte>-1
		
		en_tete
				
		rendu chemin
	
	end
	
	def en_tete
		mois = ["janvier","février","mars","avril","mai","juin","juillet","aout","septembre","octobre","novembre","décembre"]
		top = 730
		draw_text "RELEVE #{@type_paiement} EMIS", :at => [0,top], :size => 16
		top -= 30
		draw_text "DOSSIER N°", :at => [140,top], :size => 12
		draw_text "ENTREPRISE :  #{@societe.nom}", :at => [340,top], :size => 12
		top -= 30
		m = @datedeb.month
		draw_text "MOIS : #{mois[m-1].capitalize} #{@datedeb.year}", :at => [10,top], :size => 12  
		draw_text "BANQUE : #{(@banque ? @banque.banque : '')}", :at => [200,top], :size => 12
		draw_text "CPTE N° : #{(@banque ? @banque.num_compte : '')}", :at => [400,top], :size => 12  
		text("  ")
		text("  ")
		text("  ")
		text("  ")
		text("  ")
		text("  ")
		text("  ")
		text("  ")
		text("  ")
		text("  ")
		top -= 30
		data = [["DATE","N° CHEQUE","BENEFICIAIRE","OBJET DU REGLEMENT","CODE","MONTANT"]]
		@paiements.each do |p|
			tiers = p.tiers_id.nil? ? "-" : p.tiers.nom
			data << [p.date_paiement.strftime("%d/%m/%Y"), p.numero, tiers, p.note, "", "%.2f €" % p.montant]
		end
		table data, :header => true, :cell_style => { :size => 12 }
	end
	
	def rendu chemin_temp
	
		render_file "#{chemin_temp}/paiements.pdf"
	
	end
	
end

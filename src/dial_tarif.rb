# coding: utf-8

class DialTarif < Gtk::Dialog

	attr_reader :designation, :marge
	MAX_SPIN = 999999
	
	def initialize window, id=nil
		
		titre = (id.nil? ? "Ajouter un nouveau tarif" : "Modifier un tarif")
		
		super(:title => titre, :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])
		
		table = Gtk::Table.new 2, 2, false
		
		label_designation = Gtk::Alignment.new 0, 0.5, 0, 0
		label_designation.add Gtk::Label.new("Désignation :")
		table.attach( label_designation, 0, 1, 0, 1, :fill, :fill, 2, 2 )
		@designation = Gtk::Entry.new 
		table.attach( @designation, 1, 2, 0, 1, :fill, :fill, 2, 2 )
		
		label_marge = Gtk::Alignment.new 0, 0.5, 0, 0
		label_marge.add Gtk::Label.new("Marge (en %) :")
		table.attach( label_marge, 0, 1, 2, 3, :fill, :fill, 2, 2 )
		@marge = Gtk::SpinButton.new 0, MAX_SPIN, 1
		table.attach( @marge, 1, 2, 2, 3, :fill, :fill, 2, 2 )
		
		if id
			tarif = Tarif.find(id)
			@designation.text = tarif.designation
			@marge.value = tarif.marge
		end
		
		self.child.pack_start table, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all
	
	end

end		

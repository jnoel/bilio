# coding: utf-8

class SaisieLot < Gtk::Dialog
	
	attr_reader :lot
	
	def initialize window, article_id
	
		super(:title => "Saisie de lot ou de série", :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])

		@article_id = article_id
		
		vbox = Gtk::Box.new :vertical, 3
		
		frame = Gtk::Frame.new
		frame.label = "Saisie d'un numéro de lot ou de série"
		frame.add vbox
		
		vbox.pack_start( entete, :expand => true, :fill => true, :padding => 3 )
		
		self.child.pack_start frame, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all
	
	end
	
	def entete
				
		@lot = Gtk::Entry.new
		@lot.signal_connect( "activate" ) {
			response(Gtk::ResponseType::OK)
		}
				
		table = Gtk::Table.new 3, 2, false
		label_tiers = Gtk::Alignment.new 0, 0, 0, 0
		label_tiers.add Gtk::Label.new("Numéro de lot ou de série:") 
		table.attach( label_tiers, 0, 1, 0, 1, :fill, :fill, 2, 2 )
		table.attach( @lot, 1, 2, 0, 1, :fill, :fill, 2, 2 )
		
		table
	end
	
end		


# coding: utf-8

class TableauBord_box < Gtk::Box
	
	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login	
		@rapport = RapportExercices.new window.config_db.conf["adapter"]
		
		@graph = Gtk::Image.new
		@gr = GraphBar.new
		@couleur_graph = [[1,0.65,0.65],[0.65,1,0.65],[0.596,0.855,0.957]]
		@legende_graph = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"]
		@unite = ["","€"]
		
		@activer_combo = false
		
		self.pack_start frame, :expand => true, :fill => true, :padding => 0
	
	end
	
	def frame
	
		vbox = Gtk::Box.new :vertical, 3
		@frame = Gtk::Frame.new "Tableau de bord - Comptabilité dite d'engagement"
		
		@annee = Gtk::ComboBoxText.new
		remplir_annee
		@annee.active = 0
		
		@annee.signal_connect('changed') {
			refresh @annee.active_text.to_i if @activer_combo
		}
		
		@mode_button = Gtk::Button.new
		boxmode = Gtk::Box.new :horizontal, 2
		@mode_image = Gtk::Image.new( :file => "./resources/icons/wxbanker.png" )
		boxmode.add @mode_image
		@mode_label = Gtk::Label.new "Passer en comptabilité dite de caisse"
		boxmode.add @mode_label
		@mode_button.add boxmode

		@mode_button.signal_connect ("clicked") { changement_mode_compta }
		
		hbox = Gtk::Box.new :horizontal, 2
		hbox.pack_start Gtk::Label.new("Année n :"), :expand => false, :fill => false, :padding =>2 
		hbox.pack_start @annee, :expand => false, :fill => false, :padding =>2 
		hbox.pack_start @mode_button, :expand => false, :fill => false, :padding =>2 
		
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add hbox
		
		vbox.pack_start rapport, :expand => true, :fill => true, :padding =>2 
		
		scroll = Gtk::ScrolledWindow.new
		scroll.set_policy(:automatic, :automatic)
		scroll.add_with_viewport @gr #@graph
		
		@creance = true
		vbox.pack_start scroll, :expand => true, :fill => true, :padding =>2 
		
		box_f = Gtk::Box.new :vertical, 2
		box_f.pack_start align_button, :expand => false, :fill => false, :padding =>2 
		box_f.pack_start vbox, :expand => true, :fill => true, :padding =>2 
		
		@frame.add box_f
		
		@frame
	
	end
	
	def rapport
	
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)    	
  	scroll.add @rapport
  	
  	scroll
	
	end
	
	def changement_mode_compta
		
		if @creance
			@creance = false
			@mode_label.text = "Passer en comptabilité dite d'engagement"
			@frame.label = "Tableau de bord - Comptabilité dite de caisse"
			@mode_image.file = "./resources/icons/saisie.png"
		else
			@creance = true
			@mode_label.text = "Passer en comptabilité dite de caisse"
			@frame.label = "Tableau de bord - Comptabilité dite d'engagement"
			@mode_image.file = "./resources/icons/wxbanker.png"
		end
		an = (@annee.active>-1 ? @annee.active_text.to_i : nil)
		refresh an
	end
	
	def refresh an=nil
		
		combo_sensitive false
		
		if an.nil?
			an = @annee.active_text.to_i unless @annee.active.eql?(-1)
		end
		
		@rapport.refresh @creance, an
		
		Thread.new {
			sleep 0.1
			@gr.refresh @rapport.get_series, @couleur_graph, @legende_graph, @unite
		}
		
		@activer_combo = true
		combo_sensitive true
		
	end
	
	def remplir_annee
		
		doc = Document.find_by_sql("SELECT MIN(date_document) AS min, MAX(date_document) AS max FROM documents").first
		if doc.min and doc.max
			min = doc.min.year
			max = doc.max.year
			(min..max).to_a.reverse.each do |a|
				@annee.append_text a.to_s
			end
		else
			@annee.append_text Date.today.year.to_s
		end
		
	end
	
	def combo_sensitive status
		
		@annee.sensitive = status
		@mode_button.sensitive = status
		
	end
	
	def imprimer
	
		tableau = []
		tableau << ["Mois", "Dép.n-3", "Rec.n-3", "Bén.n-3", "Dép.n-2", "Rec.n-2", "Bén.n-2", "Dép.n-1", "Rec.n-1", "Bén.n-1", "Dép.n", "Rec.n", "Bén.n"]
		@rapport.list_store.each {|model, path, iter|
			m = @rapport.list_store.get_iter(path)
			tableau << [m[0],m[1],m[2],m[3],m[4],m[5],m[6],m[7],m[8],m[9],m[10],m[11],m[12]]
		}
		
		image = @window.config_db.chemin_temp+'/graph.png'
		@gr.save image
		tmp = @window.config_db.chemin_temp+'/stat.pdf'
		if File.exist?(image)
			EditionResultat.new(image, tableau, tmp, @window)	
		
			if RUBY_PLATFORM.include?("linux") then 
				system "xdg-open","#{tmp}"
			else
				if RUBY_PLATFORM.include?("mingw") then 
					system "start","#{tmp}"
				else
				
				end
			end
		end
	
	end

end

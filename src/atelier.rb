# coding: utf-8

class Atelier_box < Gtk::Box
	
	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login	
		
		self.pack_start frame, :expand => true, :fill => true, :padding => 0
	
	end
	
	def frame
	
		framebox = Gtk::Frame.new "Atelier de production"
		framebox.add drawing
		return framebox
	
	end
	
	def drawing
		d = Gtk::DrawingArea.new
		d.signal_connect "draw" do  
      on_expose d
    end
		return d
	end
	
	def on_expose d
		cr = d.window.create_cairo_context 
		
		machine1 = { :nom => "Centrifugeuse",
								 :entrees => ["Oranges", "Eau"],
								 :sorties => ["Jus de tangor", "Pulpe de tangor"]
							 }
	  machine2 = { :nom => "Centrifugeuse",
								 :entrees => ["Citron"],
								 :sorties => ["Jus de citron", "Ecorce"]
							 }
		machine3 = { :nom => "Confiseuse",
								 :entrees => ["Jus de tangor", "Sucre", "Jus de citron", "Bocaux vides"],
								 :sorties => ["Confiture de tangor"]
							 }
	 	machine4 = { :nom => "broyeuse",
								 :entrees => ["Jus de tangor", "Sucre"],
								 :sorties => ["Confiture", "de", "tangor"]
							 }
		niveau1 = [machine1, machine2, machine3, machine4]
		niveau2 = [machine3]
		recette = [niveau1]
				
		@hauteur = 200
		@marge = 25
		largeur_ecran = self::allocation.width
		
		n=0
		recette.each do |niveau|
			largeur_dispo = largeur_ecran/niveau.count if niveau.count>0
			i=0
			deb_x=0
			niveau.each do |machine|
				if i==0
					cr.set_source_rgb 0.5, 1, 1 #cyan
				elsif i==1
					cr.set_source_rgb 0.5, 1, 0.5 #vert
				elsif i==2
					cr.set_source_rgb 1, 0.5, 0.5 #rouge
				elsif i==3
					cr.set_source_rgb 1, 0.5, 1 #mauve
				end
				n_fleche = machine[:entrees].count
				largeur = (largeur_dispo-(n_fleche+1)*@marge)/n_fleche
				fleches cr, deb_x, n, n_fleche, largeur
				rectangle cr, deb_x+@marge, @hauteur+@marge*2, largeur_dispo-2*@marge, @hauteur
				n_fleche = machine[:sorties].count
				largeur = (largeur_dispo-(n_fleche+1)*@marge)/n_fleche
				fleches cr, deb_x, 1, n_fleche, largeur
				i+=1
				deb_x += largeur_dispo
			end
			n+=1
		end
		
	end
	
	def rectangle cr, x, y , w, h
		radius = h/5
		degrees = Math::PI / 180.0
		cr.arc x + w - radius, y + radius, radius, -90 * degrees, 0 * degrees
		cr.arc x + w - radius, y + h - radius, radius, 0 * degrees, 90 * degrees
		cr.arc x + radius, y + h - radius, radius, 90 * degrees, 180 * degrees
		cr.arc x + radius, y + radius, radius, 180 * degrees, 270 * degrees
		cr.close_path
		cr.fill
	end
	
	def fleches cr, deb_x, niveau, nb, largeur
		n_fleche = nb
		(0..n_fleche-1).to_a.each do |i|
			fleche cr, deb_x+(i)*(largeur+@marge)+@marge, niveau*(3*@marge+2*@hauteur)+@marge, largeur, @hauteur
		end
	end
	
	def fleche cr, x, y, w, h
		h_rect = h-h/3
		cr.move_to x, y
		cr.line_to x+w, y
		cr.line_to x+w, y+h_rect
		cr.line_to x+w/2, y+h
		cr.line_to x, y+h_rect
		cr.line_to x, y
		cr.fill
	end
	
end

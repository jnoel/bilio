# coding: utf-8

class Document_box < Gtk::Box

	attr_reader :doc
	
	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		
		@id = 0
		@type = 0
		@mail = ""
		
		@ligne_modif_id = []
		@ligne_supp_id = []
		
		@imprimer = Gtk::Button.new
		hbox_imprimer = Gtk::Box.new :horizontal, 2
		hbox_imprimer.add Gtk::Image.new( :file => "./resources/icons/print.png" )
		hbox_imprimer.add Gtk::Label.new "Imprimer"
		@imprimer.add hbox_imprimer
		
		@valider = Gtk::Button.new
		hbox_valider = Gtk::Box.new :horizontal, 2
		hbox_valider.add Gtk::Image.new( :file => "./resources/icons/ok.png" )
		hbox_valider.add Gtk::Label.new "Valider"
		@valider.add hbox_valider
		
		@annuler = Gtk::Button.new
		hbox_annuler = Gtk::Box.new :horizontal, 2
		hbox_annuler.add Gtk::Image.new( :file => "./resources/icons/cancel.png" )
		hbox_annuler.add Gtk::Label.new "Annuler"
		@annuler.add hbox_annuler
		
		@transfert = Gtk::Button.new
		hbox_transfert = Gtk::Box.new :horizontal, 2
		hbox_transfert.add Gtk::Image.new( :file => "./resources/icons/transfert.png" )
		hbox_transfert.add Gtk::Label.new "Transférer"
		@transfert.add hbox_transfert
		
		@cloner = Gtk::Button.new
		hbox_cloner = Gtk::Box.new :horizontal, 2
		hbox_cloner.add Gtk::Image.new( :file => "./resources/icons/documents.png" )
		hbox_cloner.add Gtk::Label.new "Cloner"
		@cloner.add hbox_cloner
		
		@email = Gtk::Button.new
		hbox_email = Gtk::Box.new :horizontal, 2
		hbox_email.add Gtk::Image.new( :file => "./resources/icons/transfert.png" )
		hbox_email.add Gtk::Label.new "Email"
		@email.add hbox_email
		
		@supprimer = Gtk::Button.new
		hboxsupprimer = Gtk::Box.new :horizontal, 2
		hboxsupprimer.add Gtk::Image.new( :file => "./resources/icons/remove.png" )
		hboxsupprimer.add Gtk::Label.new "Supprimer"
		@supprimer.add hboxsupprimer
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
		@imprimer.signal_connect( "clicked" ) { imprimer }
		@transfert.signal_connect("clicked") { transfert }
		@cloner.signal_connect("clicked") { cloner }
		@email.signal_connect("clicked") { mailer }
		@supprimer.signal_connect( "clicked" ) { supprimer }
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		@frame = Gtk::Frame.new
		@frame.label = "Nouveau document"
		vbox.pack_start( @frame, :expand => true, :fill => true, :padding => 3 )
		
		vbox_corps = Gtk::Box.new :vertical, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( en_tete, :expand => false, :fill => false, :padding => 3)
		vbox_corps.pack_start( lignes, :expand => true, :fill => true, :padding => 3 )
		vbox_corps.pack_start( pieds, :expand => false, :fill => false, :padding => 3 )
		
		hbox_button = Gtk::Box.new :horizontal, 2
		hbox2 = Gtk::Box.new :horizontal, 2
		hbox2.pack_start( @imprimer, :expand => true, :fill => true, :padding => 3 )
		hbox2.pack_start( @email, :expand => true, :fill => true, :padding => 3 )
		hbox2.pack_start( @transfert, :expand => true, :fill => true, :padding => 3 )
		hbox2.pack_start( @cloner, :expand => true, :fill => true, :padding => 3 )
		hbox2.pack_start( @supprimer, :expand => true, :fill => true, :padding => 3 )
		hbox3 = Gtk::Box.new :horizontal, 2
		align0 = Gtk::Alignment.new 0, 0, 0, 0
		align0.add hbox2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, :expand => true, :fill => true, :padding => 3 )
		hbox3.pack_start( @valider, :expand => true, :fill => true, :padding => 3 )
		align1.add hbox3
		hbox_button.pack_start align0, :expand => false, :fill => false, :padding => 3
		hbox_button.pack_start align1, :expand => true, :fill => true, :padding => 3
		
		vbox.pack_start( hbox_button, :expand => false, :fill => false, :padding => 3 )
		
		return vbox
	
	end

	def en_tete
	
		# Objets
		@ref = ""
		@provenance = ""
		@id_client = 0
		@client = Gtk::Entry.new
		@id_tarif = 0
		@date_document = Gtk::Entry.new
		@date_livraison = Gtk::Entry.new
		@date_echeance = Gtk::Entry.new
		@label_livraison = Gtk::Label.new "Date de livraison :"
		@note = Gtk::TextView.new	
		@note_priv = Gtk::TextView.new		
		@tranfere = false
		@status = Gtk::Image.new :file => "resources/icons/empty.png"
		@status.tooltip_text = ""
		
		# Propriétés
		@note.wrap_mode = :word
		@note.set_size_request(10, 60)
		@note_priv.wrap_mode = :word
		@note_priv.set_size_request(10, 60)
		@client.primary_icon_stock = Gtk::Stock::FIND
		
		# Agencement		
		table = Gtk::Table.new 6, 3, false
		
		table.attach( Gtk::Label.new("Client :"), 0, 1, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @client, 1, 4, 0, 1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		table.attach( Gtk::Label.new("Date du document :"), 4, 5, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @date_document, 5, 6, 0, 1, :fill, :fill, 5, 5 )
		
		table.attach( @label_livraison, 6, 7, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @date_livraison, 7, 8, 0, 1, :fill, :fill, 5, 5 )
		
		table.attach( Gtk::Label.new("Date d'échéance :"), 8, 9, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @date_echeance, 9, 10, 0, 1, :fill, :fill, 5, 5 )
		
		table.attach( @status, 10, 11, 0, 1, :fill, :fill, 5, 5 )
		
		@client.signal_connect('activate') { 
			chercher_tiers @client.text
  		}
  		
  		@client.signal_connect('icon-press') { 
			chercher_tiers @client.text
  		}
  		
  		#@date_document.signal_connect ("insert_text") { |entry, text| changement_condition_paiement }
		
		table
	
	end
	
	def chercher_tiers text
		vente = @window.type_doc[@type][:vente] ? 0 : 1
		search = SearchTiers.new @window, @client.text, vente
		search.run { |response| 
			if ( !search.liste.view.selection.selected.nil? ) then
				@client.sensitive = false
				@client.text = search.liste.view.model.get_value( search.liste.view.selection.selected, 1 )
				@id_client = search.liste.view.model.get_value( search.liste.view.selection.selected, 0 )
				@id_tarif = search.liste.view.model.get_value( search.liste.view.selection.selected, 8 ) unless (6..8).include?(@type)
				@mail = search.liste.view.model.get_value( search.liste.view.selection.selected, 9 )
				@email.sensitive = @mail.nil? ? false : !@mail.empty?
			else
				@client.text = ""
			end
			search.destroy
			focus_dernière_ligne unless @client.sensitive?
		}
	end
	
	def lignes
		
		# list_store (id, id_document, id_article, code, designation, qtite, id_tva, colisage, pu, remise, total_ligne, tva, tva_taux, tva_np, commentaire, lot, qtite_orig, trash)
		@list_store = Gtk::ListStore.new(Integer, Integer, Integer, String, String, String, Integer, Integer, String, Integer, String, String, Float, Integer, String, String, Float, Gdk::Pixbuf, Gdk::Pixbuf)
		@view = Gtk::TreeView.new(@list_store)
		
		@view.signal_connect ("button-release-event") { |tree,event|
			# Gestion du clic droit sur une ligne
			if event.button.eql?(3)
				npath = @view.get_path_at_pos(event.x, event.y)
				if !npath.nil? 
					@view.set_cursor(npath[0], nil, false)
					menu_modifie_article
				end
			end
			# Si clic gauche
			e = npath = @view.get_path_at_pos(event.x, event.y)
			if (e and event.button.eql?(1))
				colonne = e[1]
				#si clic sur colonne supprimer
				if colonne.eql?(@view.get_column(9))
					suppr_article @list_store.get_iter(e[0])
				#si clic sur colonne commentaire
				elsif colonne.eql?(@view.get_column(2))
					modif_commentaire @list_store.get_iter(e[0]) if @view.selection.selected
				end
			end
		}
		
		# Create a renderer with the background property set
		renderer_center = Gtk::CellRendererText.new
		renderer_center.xalign = 0.5
		renderer_center.yalign = 0
		renderer_center.mode = :editable
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		renderer_left.mode = :editable
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		renderer_right.mode = :editable
		
		# Create a renderer with the background property set
		renderer_right_bold = Gtk::CellRendererText.new
		renderer_right_bold.xalign = 1
		renderer_right_bold.yalign = 0
		renderer_right_bold.mode = :editable
		renderer_right_bold.background = "#eee"
		
		# Colonne pour le code article :
		@renderer_code = Gtk::CellRendererText.new
		@renderer_code.xalign = 0
		@renderer_code.yalign = 0
		@renderer_code.mode = :editable
		@renderer_code.editable = true
		@renderer_code.signal_connect('edited') { |combo, data, text|
			if !text.eql?(@list_store.get_iter(data)[3])
				#edit_cell(data, text, 3) 
				ajout = add_article text, true
				# on passe ou on reste à la ligne
				focus_dernière_ligne if ajout
			end
		}  
		col = Gtk::TreeViewColumn.new("Code article", @renderer_code, :text => 3)
		col.resizable = true
		@view.append_column(col)
		
		# Colonne pour la quantité :
		@renderer_designation = Gtk::CellRendererText.new
		@renderer_designation.xalign = 0
		@renderer_designation.yalign = 0
		@renderer_designation.mode = :editable
		@renderer_designation.editable = true
		@renderer_designation.signal_connect('edited') { |combo, data, text|
			if (!text.empty? and !text.eql?(@list_store.get_iter(data)[4])) then
				search = SearchArticles_box.new @window, text
	  			search.run { |response| 
					code = nil
					if ( !search.liste.view.selection.selected.nil? and response.eql?(-5) )
						code = search.liste.view.model.get_value( search.liste.view.selection.selected, 1 )
					end
	  				search.destroy
	  				add_article code, true unless code.nil?
	  			}
  			end
		}  
		col = Gtk::TreeViewColumn.new("Désignation", @renderer_designation, :text => 4)
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		@renderer_commentaires = Gtk::CellRendererText.new
		@renderer_commentaires.xalign = 0
		@renderer_commentaires.yalign = 0
		@renderer_commentaires.signal_connect('editing-started') { |cellrenderer, entry, data|
			#edit_cell(data, text, 14)
			
		}  
		col = Gtk::TreeViewColumn.new("Commentaires", @renderer_commentaires, :text => 14)
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		@renderer_lot = Gtk::CellRendererText.new
		@renderer_lot.xalign = 0
		@renderer_lot.yalign = 0
		col = Gtk::TreeViewColumn.new("Lot / Série", @renderer_lot, :text => 15)
		col.resizable = true
		@view.append_column(col)
		
		# Colonne pour la quantité :
		renderer_qtite = Gtk::CellRendererSpin.new
		renderer_qtite.adjustment = Gtk::Adjustment.new(0, 0, 999999, 1, 10, 0)
		renderer_qtite.digits = 2
		renderer_qtite.xalign = 1
		renderer_qtite.yalign = 0
		renderer_qtite.mode = :editable
		renderer_qtite.editable = true
		renderer_qtite.signal_connect('edited') { |combo, data, text|
			edit_cell(data, text, 5)
		} 
		col = Gtk::TreeViewColumn.new("Quantité", renderer_qtite, :text => 5)
		col.resizable = true
		#col.sort_column_id = 5
		@view.append_column(col)
		
		# Colonne pour le colisage :
		renderer_colisage = Gtk::CellRendererSpin.new
		renderer_colisage.adjustment = Gtk::Adjustment.new(0, 0, 999999, 1, 10, 0)
		renderer_colisage.xalign = 1
		renderer_colisage.yalign = 0
		renderer_colisage.mode = :editable
		renderer_colisage.editable = true
		renderer_colisage.signal_connect('edited') { |combo, data, text|
			edit_cell(data, text, 7)
		} 
		col = Gtk::TreeViewColumn.new("Colisage", renderer_colisage, :text => 7)
		col.resizable = true
		#@view.append_column(col)
		
		# Colonne pour le PU :
		renderer_pu = Gtk::CellRendererSpin.new
		renderer_pu.adjustment = Gtk::Adjustment.new(0, 0, 999999, 1, 10, 0)
		renderer_pu.digits = 2
		renderer_pu.xalign = 1
		renderer_pu.yalign = 0
		renderer_pu.mode = :editable
		renderer_pu.editable = true
		renderer_pu.signal_connect('edited') { |combo, data, text|
			edit_cell(data, text, 8)
		} 
		col = Gtk::TreeViewColumn.new("Prix unitaire", renderer_pu, :text => 8)
		col.resizable = true
		#col.sort_column_id = 8
		@view.append_column(col)
		
		# Colonne pour la remise :
		renderer_remise = Gtk::CellRendererSpin.new
		renderer_remise.adjustment = Gtk::Adjustment.new(0, 0, 100, 1, 10, 0)
		renderer_remise.xalign = 1
		renderer_remise.yalign = 0
		renderer_remise.mode = :editable
		renderer_remise.editable = true
		renderer_remise.signal_connect('edited') { |combo, data, text|
			edit_cell(data, text, 9)
		} 
		col = Gtk::TreeViewColumn.new("Remise %", renderer_remise, :text => 9)
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Total ligne", renderer_right_bold, :text => 10)
		col.resizable = true
		@view.append_column(col)
		
		renderer_tva = Gtk::CellRendererCombo.new
		renderer_tva.has_entry = false
		renderer_tva.xalign = 1
		renderer_tva.yalign = 0
		renderer_tva.editable = true		
		renderer_tva.model = modele_tva
		renderer_tva.text_column = 1
		renderer_tva.signal_connect('edited') { |combo, data, text|
			edit_cell(data, text, 11)
		} 
		col = Gtk::TreeViewColumn.new("TVA", renderer_tva, :text => 11)
		col.resizable = true
		@view.append_column(col)
		
		# Colonne pour la corbeille
		renderer_pix = Gtk::CellRendererPixbuf.new	
		renderer_pix.xalign = 0.5
		renderer_pix.yalign = 0
		@pix_trash = Gdk::Pixbuf.new("resources/icons/trash-full.png", 20, 20)
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 17)
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(:automatic, :automatic)
    	scroll.add @view
    	
    	scroll
	
	end
	
	def modif_commentaire ligne
		if possible_modif?
			mod = DialCommentaire.new @window, ligne[14]
			mod.run { |response| 
				text = ""
				modif = false
				if response.eql?(-5)
					text = mod.memo.buffer.text
					modif = true
				end
				mod.destroy
				ajout_commentaire ligne, text if modif
			}
		end
	end
	
	def ajout_commentaire ligne, text
		ligne[14] = text
		@ligne_modif_id << ligne[0] unless @ligne_modif_id.include?(ligne[0])
		ligne[17] = @pix_trash
		ligne[6] = 1 if ligne[6].eql?(0) # taux de tva à 0%
		# On détermine le nombre de lignes
		i = -1
		@list_store.each { i += 1 }
		n = @view.selection.selected.path.to_s.to_i
		# Nouvelle ligne si on est sur une ligne de commentaire seul
		if (ligne[2].eql?(0) and i.eql?(n))
			iter = @list_store.append
			iter[18] = @pix_comm
		end		
		
	end
	
	def modele_tva
		tvas = 
		Tva.order(:id)
		tva_model = Gtk::ListStore.new(Integer, String, Float)
		tvas.each { |tva|
			iter = tva_model.append
			iter[0] = tva.id
			iter[1] = tva.designation
			iter[2] = tva.taux
		}	
		tva_model	
	end
	
	def pieds
	
		@ht = Gtk::Entry.new
		@tva = Gtk::Entry.new
		@ttc = Gtk::Entry.new
		@a_payer = Gtk::Entry.new
		
		@ht.editable = @tva.editable = @ttc.editable = @a_payer.editable = false
		@ht.width_chars = @tva.width_chars = @ttc.width_chars = @a_payer.width_chars = 8
		@ht.xalign = @tva.xalign = @ttc.xalign = @a_payer.xalign = 1
		
		@regler = Gtk::Button.new
		hbox_regler = Gtk::Box.new :horizontal, 2
		hbox_regler.add Gtk::Image.new( :file => "./resources/icons/document-send.png" )
		hbox_regler.add Gtk::Label.new "Régler"
		@regler.add hbox_regler
		@regler.signal_connect("clicked") { regler }
		
		# Les réglements et le montants du pieds de page :
		table_montants = Gtk::Table.new 3, 2, false
		table_montants.attach( notebook, 0, 1, 0, 4, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, 2, 2 )
		table_montants.attach( Gtk::Label.new("Réglements:"), 1, 2, 0, 1, :fill, :fill, 2, 2 )
		table_montants.attach( @regler, 1, 2, 1, 2, :fill, :fill, 2, 2 )
		table_montants.attach( table_reglements, 2, 3, 0, 4, :fill, :fill, 2, 2 )
		table_montants.attach( Gtk::Label.new("HT :"), 3, 4, 0, 1, :fill, :fill, 2, 2 )
		table_montants.attach( @ht, 4, 5, 0, 1, :fill, :fill, 2, 2 )
		table_montants.attach( Gtk::Label.new("TVA :"), 3, 4, 1, 2, :fill, :fill, 2, 2 )
		table_montants.attach( @tva, 4, 5, 1, 2, :fill, :fill, 2, 2 )
		table_montants.attach( Gtk::Label.new("TTC :"), 3, 4, 2, 3, :fill, :fill, 2, 2 )
		table_montants.attach( @ttc, 4, 5, 2, 3, :fill, :fill, 2, 2 )
		table_montants.attach( Gtk::Label.new("A PAYER :"), 3, 4, 3, 4, :fill, :fill, 2, 2 )
		table_montants.attach( @a_payer, 4, 5, 3, 4, :fill, :fill, 2, 2 )
		
		table_montants.set_column_spacing(0, 30)
		table_montants.set_column_spacing(2, 30)
		
		table_montants
	
	end
	
	def regler
		if validate(quit=false)
			popup = Saisie_reglement.new @window, @type, @id_client
		  	popup.run
		  	refresh @id, @type
	  	end
	end 
	
	def table_reglements
											# id     date    type   montant		
		@list_paiement = Gtk::ListStore.new(Integer, String, String, String, Integer)
		table_paiement = Gtk::TreeView.new(@list_paiement)
		
		table_paiement.signal_connect ("button-release-event") { |tree,event|
			# Gestion du clic droit sur une ligne
			if event.kind_of? Gdk::EventButton and event.button == 3
				npath = table_paiement.get_path_at_pos(event.x, event.y)
				if !npath.nil? 
					table_paiement.set_cursor(npath[0], nil, false)
					menu_paiement	table_paiement.model.get_value(table_paiement.selection.selected, 4)
				end
			end
		}
		
		# Create a renderer with the background property set
		renderer_center = Gtk::CellRendererText.new
		renderer_right = Gtk::CellRendererText.new
		renderer_right.background = renderer_center.background = "white"
		renderer_center.xalign = 0.5
		renderer_right.xalign = 1
		renderer_right.yalign = renderer_center.yalign = 0
		
		col = Gtk::TreeViewColumn.new("ID", renderer_center, :text => 0)
		#table_paiement.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Date", renderer_center, :text => 1)
		table_paiement.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Type", renderer_center, :text => 2)
		table_paiement.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Montant", renderer_right, :text => 3)
		table_paiement.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Paiement ID", renderer_right, :text => 4)
		#table_paiement.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:never, :automatic)
  	scroll.add table_paiement
		
		scroll
	end
	
	def ged
		@ged = Ged_box.new @window, type="documents"
	end
	
	def scanner
		if validate(quitter=false)
			# TODO
		end
	end
	
	def remplir_liste_paiements
		@list_paiement.clear
		reglements = Paiementdocument.where(:document_id => @id).order(:id)
		montant_regle = 0
		reglements.each { |r|
			iter = @list_paiement.append
			iter[0] = r.id
			iter[1] = r.paiement.date_paiement.strftime("%d/%m/%Y")
			iter[2] = r.paiement.paiementtype.designation
			iter[3] = "%.2f €" % r.montant
			iter[4] = r.paiement_id
			montant_regle += r.montant
		}
		@doc.montant_regle = montant_regle
		@a_payer.text = "%.2f €" % (@ttc.text.to_f-montant_regle)
	end
	
	def notebook
		
		# Conteneurs
		notebook = Gtk::Notebook.new
		scroll_note1 = Gtk::ScrolledWindow.new
		scroll_note2 = Gtk::ScrolledWindow.new
		scroll_info = Gtk::ScrolledWindow.new
		table_info = Gtk::Table.new 3, 2, false
		
		# Les objets
		@prov = Gtk::Entry.new
		conditionpaiement_model = Gtk::ListStore.new(Integer, String, String, Integer, Integer)
		@conditionpaiement = Gtk::ComboBox.new :model => conditionpaiement_model
		@ref2 = Gtk::Entry.new
		label_provenance = Gtk::Alignment.new 0, 0.5, 0, 0
		label_provenance.add Gtk::Label.new("Provenance:")
		label_conditionpaiement = Gtk::Alignment.new 0, 0.5, 0, 0
		label_conditionpaiement.add Gtk::Label.new("Condition de paiement:")
		label_ref2 = Gtk::Alignment.new 0, 0.5, 0, 0
		label_ref2.add Gtk::Label.new("Référence secondaire:")

		# Propriétés des objets
		scroll_note1.set_policy :never, :automatic
		scroll_note1.shadow_type = :none
		scroll_note2.set_policy :never, :automatic
		scroll_note2.shadow_type = :none
		scroll_info.set_policy :never, :automatic
		scroll_info.shadow_type = :none
		@prov.editable = false
		renderer_conditionpaiement = Gtk::CellRendererText.new 
		@conditionpaiement.pack_start(renderer_conditionpaiement, true)
		@conditionpaiement.set_attributes(renderer_conditionpaiement, :text => 2)
		@conditionpaiement.signal_connect("changed") { changement_condition_paiement } 
		
		# Agencement des conteneurs Note
		scroll_note1.add @note
		scroll_note2.add @note_priv
		
		# Agencement du conteneur Informations
		table_info.attach( label_ref2, 0, 1, 0, 1, :fill, :fill, 2, 2 )
		table_info.attach( @ref2, 1, 2, 0, 1, :fill, :fill, 2, 2 )
		table_info.attach( label_provenance, 2, 3, 0, 1, :fill, :fill, 2, 2 )
		table_info.attach( @prov, 3, 4, 0, 1, :fill, :fill, 2, 2 )
		table_info.attach( label_conditionpaiement, 0, 1, 1, 2, :fill, :fill, 2, 2 )
		table_info.attach( @conditionpaiement, 1, 2, 1, 2, :fill, :fill, 2, 2 )
		scroll_info.add_with_viewport table_info
		
		# Agencement du notebook
		notebook.append_page scroll_info, Gtk::Label.new("Informations")
		notebook.append_page ged, Gtk::Label.new("GED")
		notebook.append_page scroll_note1, Gtk::Label.new("Note publique")
		notebook.append_page scroll_note2, Gtk::Label.new("Note privée")

		# Renvoie
		notebook
		
	end
	
	def changement_condition_paiement
		date_valide = false
		date_document = nil
		begin
			date_document = Date.parse(@date_document.text)
		rescue
			date_valide = false
		else
			date_valide = true
		end
		if date_valide
			begin
				nb_jours = @conditionpaiement.model.get_value(@conditionpaiement.active_iter, 3) if @conditionpaiement.active_iter
				fin_de_mois = @conditionpaiement.model.get_value(@conditionpaiement.active_iter, 4).eql?(1) if @conditionpaiement.active_iter
				date = Date.parse(@date_document.text) + nb_jours
				date = Date.new(date.year, date.month, -1) if fin_de_mois
				@date_echeance.text = date.strftime("%d/%m/%Y")
			rescue
			end
		end
	end
	
	def refresh id, type
	
		@doc = nil
		@id = id	
		@type = type	
		@ligne_modif_id = []
		@ligne_supp_id = []		
		@date_livraison.sensitive = type.eql?(3)
		@supprimer.sensitive = id>0
		
		if id>0 then
			
			@doc = Document.includes(:documenttype, :tiers).where(:id => id).first
			if @doc then
				remplir_en_tete @doc
			end
			
			doclignes = Documentligne.where(:document_id => id).includes(:tva).order(:id)
			if doclignes then
				remplir_lignes doclignes
			end
			
			remplir_pieds
		else
			@doc = Document.new
			@frame.label = nouveau_nouvelle type		
			@frame.label += @window.type_doc[type][:nom].downcase 
			empty_component
			iter = @list_store.append
			iter[18] = @pix_comm
			@doc.montant_ttc = 0.0
			@doc.montant_regle = 0.0
			@mail = ""	
			@list_paiement.clear
		end
		
		refresh_ged
		@regler.sensitive = ([4,8].include?(@type) and (@doc.montant_regle<@doc.montant_ttc))
		remplir_conditionpaiement
		@email.sensitive = !@mail.empty? unless @mail.nil?
		
		@view.columns_autosize
	
	end
	
	def refresh_ged
		@ged.refresh @id
	end
	
	def nouveau_nouvelle num
		texte = ""
		case num
			when 1, 3
				texte = "Nouveau "
			when 5
				texte = "Nouvel "
			else
				texte = "Nouvelle "
		end
		texte
	end
	
	def remplir_conditionpaiement
		@conditionpaiement.model.clear
		cond = Conditionpaiement.where(:actif => true).order(:id)
		active_iter = nil
		cond.each { |c|
			iter = @conditionpaiement.model.append
			iter[0] = c.id
			iter[1] = c.code
			iter[2] = c.designation
			iter[3] = c.nb_jours
			iter[4] = (c.fin_mois ? 1 : 0)
			active_iter = iter if @doc.conditionpaiement_id.eql?(c.id)
		}
		if active_iter
			@conditionpaiement.active_iter = active_iter
		else
			@conditionpaiement.active = 0
		end
	end
	
	def remplir_en_tete doc
	
		date_document = ""
		date_livraison = ""
		@id_client = doc.tiers_id
		@client.text = doc.tiers.nom
		@mail = doc.tiers.mail
		@client.sensitive = false
		@id_tarif = doc.tarif_id
		date_document = doc.date_document.to_s
		date_livraison = doc.date_livraison.to_s
		date_echeance = doc.date_echeance.to_s
		@note.buffer.text = doc.note
		@note_priv.buffer.text = doc.note_priv
		@ref = doc.ref
		@transfere = doc.transfere
		@frame.label = @window.type_doc[@type][:nom] + " #{@ref}" 
		begin
			@date_document.text = Date.parse(date_document).strftime("%d/%m/%Y")
			@date_livraison.text = Date.parse(date_livraison).strftime("%d/%m/%Y")
			@date_echeance.text = Date.parse(date_echeance).strftime("%d/%m/%Y")
		rescue
		end
		@transfert.sensitive = (!@transfere and !@type.eql?(5) and !@type.eql?(8))
		remplir_liste_paiements	
		@provenance = @doc.provenance_ref
		@prov.text = @provenance ? @provenance : ""
		@ref2.text = @doc.ref2 ? @doc.ref2 : ""
	end
	
	def remplir_lignes doclignes
	
		@list_store.clear
		
		doclignes.each { |h| 
			
			iter = @list_store.append
			iter[0] = h.id
			iter[1] = h.document_id
			iter[2] = h.article_id
			iter[3] = h.code
			iter[4] = h.designation
			iter[5] = "%.2f" % h.qtite
			iter[6] = h.tva_id
			iter[7] = h.colisage
			iter[8] = "%.2f" % h.pu
			iter[9] = h.remise
			iter[10] = "%.2f €" % h.total_ligne.to_f
			iter[11] = h.tva.designation.to_s
			iter[12] = h.tva.taux
			iter[13] = (h.tva.np ? 1 : 0)
			iter[14] = h.commentaires
			iter[15] = h.lot
			iter[16] = h.qtite
			iter[17] = @pix_trash
			iter[18] = @pix_comm
		}
		
		iter = @list_store.append
		iter[18] = @pix_comm

	end
	
	def remplir_pieds
		
		ht = 0.0
		tva = 0.0
	
		@list_store.each { |model, path, iter|
			ligne = @view.model.get_iter(path)
			ht += ligne[10].to_f unless ligne[10].nil?
			tva += ligne[10].to_f*(ligne[12].to_f/100) unless ligne[10].nil? or ligne[12].nil? or ligne[13].eql?(1)
		}
		
		ht = ht.round(2)
		tva = tva.round(2)
		
		@ht.text = "%.2f €" % ht
		@tva.text = "%.2f €" % tva
		@ttc.text = "%.2f €" % (ht+tva).round(2)
		
		@a_payer.text = "%.2f €" % (@ttc.text.to_f-@doc.montant_regle)
		@regler.sensitive = true if (@a_payer.text.to_f>0.0 and [4,8].include?(@type))
		
		if @type.eql?(4) or @type.eql?(8)
			if @a_payer.text.to_f<=0.0
				@status.set_file "resources/icons/paye.png"
				@status.tooltip_text = "Payée"
			elsif @a_payer.text.to_f.eql?(@ttc.text.to_f)
				@status.set_file "resources/icons/impaye.png"
				@status.tooltip_text = "Impayée"
			else
				@status.set_file "resources/icons/partiel.png"
				@status.tooltip_text = "Payée partiellement"
			end
		else
			@status.set_file "resources/icons/empty.png"
			@status.tooltip_text = ""
		end
		
	end
	
	def empty_component entierement=true
	
		@ref = ""
		@ref2.text = ""
		@provenance = "" if entierement
		@prov.text = "" if entierement
		@client.text = "" if entierement
		@client.sensitive = true if entierement
		@id_client = 0 if entierement
		if entierement
			if (6..8).include?(@type)
				t = Tarif.where(:pa => true).first
			else
				t = Tarif.where(:vente => true, :default => true).first
			end
			@id_tarif = t.id
		end
		date = DateTime.now
		@date_document.text = date.strftime("%d/%m/%Y")
		date += 1
		@date_livraison.text = ""
		@date_echeance.text = date.strftime("%d/%m/%Y")
		@note.buffer.text = "" if entierement
		@note_priv.buffer.text = "" if entierement
		@transfere = false
		@transfert.sensitive = (!@type.eql?(5) and !@type.eql?(8))
		
		@list_store.clear if entierement
		
		@ht.text = "0 €" if entierement
		@tva.text = "0 €" if entierement
		@ttc.text = "0 €" if entierement
		
		if !entierement #Si on est dans un transfert on vide la qtité initiale pour chaque article
			@list_store.each { |model, path, iter|
				@view.model.get_iter(path)[16]=0
			}
		end
		
		@status.set_file "resources/icons/empty.png"
		@status.tooltip_text = ""
	
	end
	
	def edit_cell data, text, col
	
		if possible_modif?
			ligne = @view.model.get_iter(data)	
			if !text.empty? then
			
				case col
			
					when 3
						# Changement du code article
						add_article text					
					when 4
						ligne[col] = text
					when 5
						# Changement de quantité
						ligne[col] = "%.2f" % text
						# On indique quel est la ligne qui a été modifiée
						@ligne_modif_id << ligne[0] unless @ligne_modif_id.include?(ligne[0])
					when 7
						# Changement de colisage
						ligne[col] = text.to_i
						# On indique quel est la ligne qui a été modifiée
						@ligne_modif_id << ligne[0] unless @ligne_modif_id.include?(ligne[0])
					when 8
						ligne[col] = text if text.to_f>=0.0
						# On indique quel est la ligne qui a été modifiée
						@ligne_modif_id << ligne[0] unless @ligne_modif_id.include?(ligne[0])
					when 9
						ligne[col] = text.to_i if (text.to_i>=0 and text.to_i<=100)
						# On indique quel est la ligne qui a été modifiée
						@ligne_modif_id << ligne[0] unless @ligne_modif_id.include?(ligne[0])
					when 11
						tva = Tva.where(:designation => text).first
						ligne[6] = tva.id
						ligne[11] = tva.designation.to_s
						ligne[12] = tva.taux
						ligne[13] = (tva.np ? 1 : 0)
						@ligne_modif_id << ligne[0] unless @ligne_modif_id.include?(ligne[0])
					when 14
						# Ajout d'un commentaire
						ajout_commentaire ligne, text
			
				end		
				calcule_total_ligne ligne
			else
				# Suppresion de la ligne si le code article est vide
				suppr_article ligne if col.eql?(3)
				if col.eql?(14) then
					ligne[col] = ""
					@ligne_modif_id << ligne[0] unless @ligne_modif_id.include?(ligne[0])
					# Si on est sur une ligne de commentaire libre, on supprime la ligne
					suppr_article ligne if ligne[2].eql?(0)
				end
			end
		end	
		
	end
	
	def add_article code, recalcule=false
		
		ajout = false
		
		if (possible_modif? and !code.empty?)
			ligne = @view.selection.selected
		
			ajout = true
		
			article = Article.includes(:tva).where("(code=? OR gencode=? OR gencode2=?) AND suppr=?", code, code, code, false).first
			
			if article then		
				ajout = true
				if ligne[3].nil?
					iter = @list_store.append
					iter[18] = @pix_comm
				end
				ligne[2] = article.id
				ligne[3] = article.code
				ligne[4] = article.description.to_s.empty? ? article.designation : "#{article.designation}\n#{article.description}"
				ligne[6] = article.tva_id
				ligne[7] = article.colisage
				at = Articletarif.where(:article_id =>  article.id, :tarif_id => @id_tarif).first
				ligne[8] = "%.2f" % at.pu
				ligne[5] = "%.2f" % 1.0 if ligne[5].to_f.eql?(0.0)
				ligne[11] = article.tva.designation
				ligne[12] = article.tva.taux
				ligne[13] = (article.tva.np ? 1 : 0)
				ligne[17] = @pix_trash
			
				# Si l'article est géré par lot, on le demande tout de suite si on est sur un certain type de document
				ajout = verif_lot(article.id, ligne) if article.lot 
		
				if ajout then
					calcule_total_ligne ligne if recalcule
		
					# On indique quels sont les lignes qui ont été modifiées ou ajoutées
					id_ligne = @view.selection.selected[0]
					@ligne_modif_id << id_ligne unless @ligne_modif_id.include?(id_ligne)
				end
			end
		end
		return ajout
	end
	
	# Indique si le document est modifiable ou pas
	def possible_modif?
		if (@doc.montant_regle<@doc.montant_ttc	or @doc.montant_ttc.eql?(0.0))
			return true
		else
			MessageController.erreur @windows, "Le document a déjà été réglé intégralement.\nAucune modification possible"
			return false
		end
	end
	
	def focus_dernière_ligne
		Thread.new { 
			sleep 0
			# on trouve la dernière ligne du tableau
			npath = nil
			@view.model.each {|model, path, iter| 
				npath = path
			}
			# et on met le focus sur la première colonne.
			@view.set_cursor(npath, @view.get_column(0), true)
		}
	end
	
	def verif_lot article_id, ligne
		ajout = true
	
		if [4, 5].include?(@type)
			search = SearchLots.new @window, article_id
  			search.run { |response| 
				if ( !search.liste.view.selection.selected.nil? and response.eql?(-5)) then
					ligne[15] = search.liste.view.model.get_value( search.liste.view.selection.selected, 1 )
				else
					suppr_article ligne, false
					ajout = false
				end
  				search.destroy 			
  			}
  		elsif @type.eql?(8)
  			search = SaisieLot.new @window, article_id
  			search.run { |response| 
				if (!search.lot.text.empty? and response.eql?(-5))
					ligne[15] = search.lot.text
				else
					suppr_article ligne, false
					ajout = false
				end	
				search.destroy 	
  			}
  		end

		return ajout
	end
	
	def suppr_article ligne, conf=true
	
		if possible_modif?
		
			if conf
				suppr = false
			
				if MessageController.question_oui_non @window, "Voulez-vous réellement supprimer cette ligne ?"
					suppr = true
				end 
				
			else
				suppr = true
			end
		
			if suppr
				# Si la ligne était déjà enregistrée dans la base, on la marque à supprimer
				@ligne_supp_id << ligne[0].to_i if ligne[0].to_i>0		

				# On réinitialise les valeurs des champs
				ligne[0] = ligne[1] = ligne[2] = ligne[16] = 0
				ligne[5] = "0.00"
				ligne[3] = nil
				ligne[4] = nil
				ligne[6] = 1
				ligne[8] = nil
				ligne[14] = nil
				ligne[7] = 1
		
				# On détermine le nombre de lignes
				i = -1
				@list_store.each { i += 1 }
				n = @view.selection.selected.path.to_s.to_i
		
				# Puis on retire la ligne du tableau sauf si il s'agit de la dernière ligne
				@list_store.remove @view.selection.selected unless i.eql?(n)
		
				# on recalcule le pieds
				remplir_pieds
			end
		end
	
	end
	
	def calcule_total_ligne ligne
		ligne[7] = 1 # Pour l'instant le colisage est masqué, on ne l'utilise pas.
		ligne[10] = "%.2f €" % ( ligne[5].to_f*ligne[8].to_f*ligne[7]*( 1-ligne[9].to_f/100 ) ).round(2)
		remplir_pieds
	end
	
	def edition
		validate
		chemin = @window.config_db.chemin_temp
		Edition.new [@doc.id], chemin, @window
		return "#{chemin}/#{@doc.ref}.pdf"
	end
	
	def imprimer
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open", edition
		elsif RUBY_PLATFORM.include?("mingw") then 
			system "start", edition
		else
			# Pour MacOS ??
		end
	end
	
	def mailer
		validate
		chemin = @window.config_db.chemin_temp
		Edition.new [@doc.id], chemin, @window
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-email --subject '#{@window.type_doc[@type][:nom].downcase}' --attach #{edition} #{@mail}" 
		elsif RUBY_PLATFORM.include?("mingw") then 
				# TODO
		else
				# Pour MacOS ??
		end
	end
	
	def validate quitter=true
	
		continu = true
		begin
			date_document = Date.parse(@date_document.text).strftime("%Y-%m-%d")
			date_livraison = @date_livraison.text.empty? ? "" : Date.parse(@date_livraison.text).strftime("%Y-%m-%d")
			date_echeance =  Date.parse(@date_echeance.text).strftime("%Y-%m-%d")
		rescue
			continu = false
			MessageController.erreur @window, "Il y a un problème de date. Veuillez les vérifier."
			return false			 
		end
		
		if continu
			tarif = @id_tarif
		
			res = !@id_client.eql?(0)  # Il faut qu'un client soit sélectionné pour pouvoir enregistrer le document !
			
			if res then
				@ref = reference if @ref.empty?
				@doc.tiers_id = @id_client
				@doc.date_document = date_document
				@doc.note = @note.buffer.text
				@doc.note_priv = @note_priv.buffer.text
				@doc.documenttype_id = @type
				@doc.date_livraison = date_livraison
				@doc.date_echeance = date_echeance
				@doc.tarif_id = tarif
				@doc.utilisateur_id = @login.user.id
				@doc.montant_ht = @ht.text
				@doc.montant_ttc = @ttc.text
				@doc.ref = @ref
				@doc.ref2 = @ref2.text
				@doc.transfere = @tranfere
				@doc.provenance_ref = @provenance
				@doc.conditionpaiement_id = @conditionpaiement.model.get_value(@conditionpaiement.active_iter,0)
			
				res = @doc.save
			else
				MessageController.erreur @window, "Vous devez sélectionner un client valide !"		 
			end
		
			if res and !@ligne_modif_id.empty? then
			
				@list_store.each { |model, path, iter|
				
					if !@view.model.get_iter(path)[3].nil? or !@view.model.get_iter(path)[14].nil? then
						id_ligne = @view.model.get_iter(path)[0]
						id_article = @view.model.get_iter(path)[2]
						code = @view.model.get_iter(path)[3]
						designation = @view.model.get_iter(path)[4]
						qtite = @view.model.get_iter(path)[5].to_f
						id_tva = @view.model.get_iter(path)[6].nil? ? "1" :  @view.model.get_iter(path)[6]
						colisage = @view.model.get_iter(path)[7]
						pu = @view.model.get_iter(path)[8].nil? ? 0 : @view.model.get_iter(path)[8]
						remise = @view.model.get_iter(path)[9]
						total_ligne = @view.model.get_iter(path)[10]
						commentaires = @view.model.get_iter(path)[14]
						lot = @view.model.get_iter(path)[15].to_s
						
						dl = nil
						if id_ligne>0 then
							dl = Documentligne.find(id_ligne)
						else
							dl = Documentligne.new
						end
					
						dl.document_id = @doc.id
						dl.article_id = id_article
						dl.qtite = qtite
						dl.tva_id = id_tva
						dl.colisage = colisage
						dl.pu = pu
						dl.remise = remise
						dl.code = code
						dl.designation = designation
						dl.total_ligne = total_ligne
						dl.commentaires = commentaires
						dl.lot = lot
					
						res = dl.save
						
						if res then
							# Gestion des stocks si l'article n'est pas de type service
							if dl.article_id>0
								art = Article.find(dl.article_id)
								stock qtite, @view.model.get_iter(path)[16], id_article, lot unless art.service
							end
						end
				
					end
				
				}
			
			end
		
			if res then 
				# suppression en base des lignes supprimées dans le tableau
				@ligne_supp_id.each { |l|								
					dl = Documentligne.find(l)				
					if dl then
						stock 0, dl.qtite, dl.article_id, dl.lot
						res = dl.destroy
						p "Suppresion de ligne : #{l}" if res
					end				
				}
			end
		
			if res
				if quitter
					quit @type
				else
					refresh @doc.id, @type
				end
			end
			return res
		end
	end
	
	def reference
		num = Documenttype.find(@type)
		if num then
			d = num.dernier_num + 1
			n = "%04d" % d
			date_document = Date.parse(@date_document.text).strftime("%y%m")			
			ref = "#{num.code}#{date_document}-#{n}"
			
			res = num.increment!(:dernier_num)
			
			if res then
				return ref
			else
				return ""
			end
		else
			return ""
		end
	end
	
	def stock qtite, qtite_orig, id_article, lot
			
		if [4,5,8].include?(@type) # Si on est sur une Facture ou un Avoir alors on modifie le stock
			ajoute = [5,8].include?(@type) # Si on est sur un avoir ou une facture fournisseur, on ajoute, sinon on enlève 
			change_stock qtite, qtite_orig, id_article, lot, ajoute
		end	
		
	end
	
	def change_stock qtite, qtite_orig, id_article, lot, ajoute
	
		qtite_delta = qtite-qtite_orig
		
		stock = Articlestock.where(:article_id => id_article, :lot => lot).first
		if stock then 
			id_stock = stock.id
			qtite_finale = ajoute ? stock.qtite+qtite_delta : stock.qtite-qtite_delta
		else
			stock = Articlestock.new
			qtite_finale = ajoute ? qtite_delta : -qtite_delta 
		end
	
		stock.stock_id = 1
		stock.lot = lot
		stock.qtite = qtite_finale
		stock.article_id = id_article
		
		p "nouveau stock = #{qtite_finale}"
		
		res = stock.save
	
	end
	
	def transfert
	
		if MessageController.question_oui_non @window, "Voulez-vous réellement transférer votre document en #{@window.type_doc[@type+1][:nom].downcase} ?"
			@tranfere = true
			if validate(false) then
				@doc = Document.new
				@tranfere = false
				@provenance = @ref
				@prov.text = "Provenance : #{@provenance}" unless @provenance.empty?
				empty_component entierement=false
				@id=0
				@type = @type+1
				@frame.label = nouveau_nouvelle @type
				@frame.label += @window.type_doc[@type][:nom].downcase 
		
				@list_store.each { |model, path, iter|
					ligne = @view.model.get_iter(path)
					ligne[0] = 0
					@ligne_modif_id << ligne[0] unless @ligne_modif_id.include?(ligne[0])
				}
				
				@transfert.sensitive = (!@type.eql?(5) and !@type.eql?(8))
			end			
		end 
	
	end
	
	def cloner
	
		if MessageController.question_oui_non @window, "Voulez-vous réellement cloner votre document ?"
			if validate(false) then
				@doc = Document.new
				@tranfere = false
				@provenance = @ref
				@prov.text = ""
				empty_component entierement=false
				@id=0
				
				@frame.label = nouveau_nouvelle @type
				@frame.label += @window.type_doc[@type][:nom].downcase 
		
				@list_store.each { |model, path, iter|
					ligne = @view.model.get_iter(path)
					ligne[0] = 0
					@ligne_modif_id << ligne[0] unless @ligne_modif_id.include?(ligne[0])
				}
				@list_paiement.clear
			end			
		end 
	
	end
	
	def focus
		@client.grab_focus
	end
	
	def menu_modifie_article
		id = @view.model.get_value( @view.selection.selected, 2 )
		code = @view.model.get_value( @view.selection.selected, 3 )
		if id>0
			modif_tb = Gtk::MenuItem.new "Modifier article #{code}"
			modif_tb.signal_connect( "activate" ) { 
				dial_article = DialNewArticle.new @window, id
				dial_article.run { |response| 
					mod = false
					if response.eql?(-5)
						dial_article.article_window.validate
						mod = true
					end
					dial_article.destroy
					add_article code, true
				}
			}
			menu = Gtk::Menu.new
			menu.append modif_tb
			menu.show_all
			menu.popup(nil, nil, 0, 0)
		end
	end
	
	def menu_paiement id
	
		suppr_tb = Gtk::MenuItem.new "Supprimer la pièce"
		suppr_tb.signal_connect( "activate" ) { 
			supprime_paiement id
		}
		
		menu = Gtk::Menu.new
		menu.append suppr_tb
		menu.show_all
		menu.popup(nil, nil, 0, 0)
		
	end
	
	def supprime_paiement id
		if MessageController.question_oui_non @window, "Voulez-vous réellement supprimer ce paiement ?"
			ReglementController.supprime_paiements [id]
			remplir_liste_paiements
		end
	end
	
	def supprimer
	
		if @doc.montant_regle>0
			MessageController.erreur @window, "Vous ne pouvez pas supprimer ce document tant que des réglements y sont rattachés."
		else
			if MessageController.question_oui_non @window, "Voulez-vous réellement supprimer ce document ?"
				DocumentController.supprime [@doc.id]
				quit @type
			end 
		end
	
	end
	
	def quit statut=nil
		
		@window.liste_documents.refresh statut unless statut.nil?	
		@window.affiche @window.liste_documents
	
	end
	
end

# coding: utf-8

# Fenêtre principale de l'application

class MainWindow < Gtk::Window
	
		attr_reader :type_doc, :login, :tableau_bord, :document, :article, :tiers	
		attr_reader :liste_documents, :liste_users, :liste_articles, :liste_tiers
		attr_reader :version, :utilisateur, :liste_lots, :scan 
		attr_reader :paiement, :liste_compte, :compte, :configuration, :liste_interventions
		attr_reader :intervention, :stat_articles, :atelier, :recette
		attr_accessor :config_db
	
	def initialize level
	
		super
		
		self.signal_connect( "destroy" ) { quitter }
		
		@version = "0.5.2"
		self.set_title GLib.application_name
		
		self.set_window_position :center
		self.set_default_size 800, 300
		self.set_icon( "./resources/icons/icon.png" )		
		
		@config_db = ConfigDb.new self
		
		lancement
	
	end
	
	def lancement
		
		@vbox = Gtk::Box.new :vertical, 2
		@login = Login_box.new self
		@vbox.pack_start(@login, :expand => false, :fill => false, :padding => 2)
		
		self.add @vbox
		
		show_all
		
		@login.refresh
		
	end
	
	def demarre_appli
	
		unless @config_db.conf["commande_lancement"].to_s.empty?
			MessageController.text "Exécution de la commande de lancement."
			puts `#{@config_db.conf["commande_lancement"].to_s}`
		end
	
		@type_doc = TypeDocument.new self
		
		@toolbargen = ToolBarGen_box.new self
	
		@toolbargen.each { |child|
			child.sensitive = true
		}
		
		# If not a admin, config menu is hidden
		@toolbargen.children[14].sensitive = false unless @login.user.admin
		
		@tableau_bord = TableauBord_box.new self
		@document = Document_box.new self
		@article = Article_box.new self
		@tiers = Tiers_box.new self
		@liste_articles = ListeArticles_box.new self
		@liste_documents = ListeDocuments_box.new self
		@liste_users = ListeUsers_box.new self
		@liste_tiers = ListeTiers_box.new self
		@utilisateur = Utilisateur_box.new self
		@liste_lots = ListeLots_box.new self
		@scan = Scan.new @config_db
		@paiement = ListePaiements_box.new self
		@liste_compte = ListeComptes_box.new self
		@compte = Compte_box.new self
		@configuration = Configuration_box.new self
		@liste_interventions = ListeInterventions_box.new self
		@intervention = Intervention_box.new self
		@stat_articles = Stat_articles_box.new self
		@atelier = Atelier_box.new self
		@recette = Recette_box.new self
		
		set_title "#{GLib.application_name} v #{@version} - #{@config_db.conf['nom']}"
		
		@vbox.remove @login
		@vbox.pack_start @toolbargen, :expand => false, :fill => false, :padding => 0
		@vbox.pack_start @tableau_bord, :expand => true, :fill => true, :padding => 0
    self.show_all
		@tableau_bord.refresh
	
	end
	
	def deconnecter
	
		if MessageController.question_oui_non self, "Voulez-vous réellement vous déconnecter ?"
			@vbox.children.each do |c|
				@vbox.remove c
			end
			puts `#{@config_db.conf["commande_fermeture"].to_s}` unless @config_db.conf["commande_fermeture"].to_s.empty?
	
			@vbox.pack_start @login, :expand => false, :fill => false, :padding => 0
			@login.refresh
	
			set_title "#{GLib.application_name} v #{@version}"
	
			self.unmaximize
		end 
	
	end
	
	def efface_vbox_actuelle
	
		@vbox.remove @vbox.children[@vbox.children.count-1] if @vbox.children.count.eql?(2)
	
	end
	
	def affiche vbox
	
		efface_vbox_actuelle
		@vbox.pack_start vbox, :expand => true, :fill => true, :padding => 0
    self.show_all
	
	end	
	
	def quit
		if MessageController.question_oui_non self, "Voulez-vous réellement quitter #{GLib.application_name.capitalize} ?"
			quitter
		end 
	end
	
	def quitter
		unless @config_db.conf["commande_fermeture"].to_s.empty?
			MessageController.text "Exécution de la commande de fermeture."
			puts `#{@config_db.conf["commande_fermeture"].to_s}`
		end
		Gtk.main_quit
	end
	
	def message_erreur message
		MessageController.erreur self, message
	end
	
	def message_attention message
		MessageController.attention self, message
	end

end

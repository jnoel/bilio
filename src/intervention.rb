# coding: utf-8

class Intervention_box < Gtk::Box

	attr_reader :doc
	
	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		
		@id = 0
		@mail = ""
		
		@ligne_modif_id = []
		@ligne_supp_id = []
		
		@imprimer = Gtk::Button.new
		hbox_imprimer = Gtk::Box.new :horizontal, 2
		hbox_imprimer.add Gtk::Image.new( :file => "./resources/icons/print.png" )
		hbox_imprimer.add Gtk::Label.new "Imprimer"
		@imprimer.add hbox_imprimer
		
		@valider = Gtk::Button.new
		hbox_valider = Gtk::Box.new :horizontal, 2
		hbox_valider.add Gtk::Image.new( :file => "./resources/icons/ok.png" )
		hbox_valider.add Gtk::Label.new "Valider"
		@valider.add hbox_valider
		
		@annuler = Gtk::Button.new
		hbox_annuler = Gtk::Box.new :horizontal, 2
		hbox_annuler.add Gtk::Image.new( :file => "./resources/icons/cancel.png" )
		hbox_annuler.add Gtk::Label.new "Annuler"
		@annuler.add hbox_annuler
		
		@transfert = Gtk::Button.new
		@transfert.sensitive = false
		hbox_transfert = Gtk::Box.new :horizontal, 2
		hbox_transfert.add Gtk::Image.new( :file => "./resources/icons/transfert.png" )
		hbox_transfert.add Gtk::Label.new "Transférer"
		@transfert.add hbox_transfert
		
		@email = Gtk::Button.new
		hbox_email = Gtk::Box.new :horizontal, 2
		hbox_email.add Gtk::Image.new( :file => "./resources/icons/transfert.png" )
		hbox_email.add Gtk::Label.new "Email"
		@email.add hbox_email
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
		@imprimer.signal_connect( "clicked" ) { imprimer }
		@transfert.signal_connect("clicked") { transfert }
		@email.signal_connect("clicked") { mailer }
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		@frame = Gtk::Frame.new
		@frame.label = "Nouvelle intervention"
		vbox.pack_start( @frame, :expand => true, :fill => true, :padding => 3 )
		
		vbox_corps = Gtk::Box.new :vertical, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( en_tete, :expand => false, :fill => false, :padding => 3 )
		vbox_corps.pack_start( lignes, :expand => true, :fill => true, :padding => 3 )
		vbox_corps.pack_start( pieds, :expand => false, :fill => false, :padding => 3 )
		
		hbox_button = Gtk::Box.new :horizontal, 2
		hbox2 = Gtk::Box.new :horizontal, 2
		hbox2.pack_start( @imprimer, :expand => true, :fill => true, :padding => 3 )
		hbox2.pack_start( @email, :expand => true, :fill => true, :padding => 3 )
		hbox2.pack_start( @transfert, :expand => true, :fill => true, :padding => 3 )
		hbox3 = Gtk::Box.new :horizontal, 2
		align0 = Gtk::Alignment.new 0, 0, 0, 0
		align0.add hbox2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, :expand => true, :fill => true, :padding => 3 )
		hbox3.pack_start( @valider, :expand => true, :fill => true, :padding => 3 )
		align1.add hbox3
		hbox_button.pack_start align0, :expand => false, :fill => false, :padding => 3
		hbox_button.pack_start align1, :expand => true, :fill => true, :padding => 3
		
		vbox.pack_start( hbox_button, :expand => false, :fill => false, :padding => 3 )
		
		return vbox
	
	end

	def en_tete
	
		# Objets
		@id_client = 0
		@client = Gtk::Entry.new
		@date = Gtk::Entry.new
		@note = Gtk::TextView.new	
		@note_priv = Gtk::TextView.new		
		@tranfere = false
		
		# Propriétés
		@note.wrap_mode = :word
		@note.set_size_request(10, 60)
		@note_priv.wrap_mode = :word
		@note_priv.set_size_request(10, 60)
		@client.primary_icon_stock = Gtk::Stock::FIND
		
		# Agencement		
		table = Gtk::Table.new 6, 3, false
		
		table.attach( Gtk::Label.new("Client :"), 0, 1, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @client, 1, 4, 0, 1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		table.attach( Gtk::Label.new("Date de l'intervention :"), 4, 5, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @date, 5, 6, 0, 1, :fill, :fill, 5, 5 )
		
		@client.signal_connect('activate') { 
			chercher_tiers @client.text
  		}
  		
  		@client.signal_connect('icon-press') { 
			chercher_tiers @client.text
  		}
		
		table
	
	end
	
	def chercher_tiers text
		search = SearchTiers.new @window, @client.text, 0
		search.run { |response| 
			if ( !search.liste.view.selection.selected.nil? ) then
				@client.sensitive = false
				@client.text = search.liste.view.model.get_value( search.liste.view.selection.selected, 1 )
				@id_client = search.liste.view.model.get_value( search.liste.view.selection.selected, 0 )
				@mail = search.liste.view.model.get_value( search.liste.view.selection.selected, 9 )
				@email.sensitive = @mail.nil? ? false : !@mail.empty?
			else
				@client.text = ""
			end
			search.destroy
			focus_dernière_ligne unless @client.sensitive?
		}
	end
	
	def lignes
		
		# list_store (                      id,   id_document, commentaire duree trash)
		@list_store = Gtk::ListStore.new(Integer, Integer, String, String, Gdk::Pixbuf)
		@view = Gtk::TreeView.new(@list_store)	
		
		@view.signal_connect ("button-release-event") { |tree,event|
			# Si clic gauche
			e = npath = @view.get_path_at_pos(event.x, event.y)
			if (e and event.button.eql?(1))
				colonne = e[1]
				#si clic sur colonne supprimer
				if colonne.eql?(@view.get_column(2))
					suppr_ligne @list_store.get_iter(e[0])
				#si clic sur colonne commentaire
				elsif colonne.eql?(@view.get_column(0))
					modif_commentaire @list_store.get_iter(e[0])
				end
			end
		}	
		
		renderer_commentaires = Gtk::CellRendererText.new
		renderer_commentaires.xalign = 0
		renderer_commentaires.yalign = 0
		renderer_commentaires.mode = :editable 
		col = Gtk::TreeViewColumn.new("Commentaires", renderer_commentaires, :text => 2)
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		# Colonne pour la duree :
		renderer_duree = Gtk::CellRendererSpin.new
		renderer_duree.adjustment = Gtk::Adjustment.new(0, 0, 999999, 1, 10, 0)
		renderer_duree.digits = 2
		renderer_duree.xalign = 1
		renderer_duree.yalign = 0
		renderer_duree.mode = :editable
		renderer_duree.editable = true
		renderer_duree.signal_connect('edited') { |combo, data, text|
			ligne = @view.model.get_iter(data)
			ligne[3] = "%.2f" % text.to_f
			remplir_pieds
		} 
		col = Gtk::TreeViewColumn.new("Durée", renderer_duree, :text => 3)
		col.resizable = true
		#col.sort_column_id = 5
		@view.append_column(col)
		
		# Colonne pour la corbeille
		renderer_pix = Gtk::CellRendererPixbuf.new	
		renderer_pix.xalign = 0.5
		renderer_pix.yalign = 0
		@pix_trash = Gdk::Pixbuf.new("resources/icons/trash-full.png", 20, 20)
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 4)
		col.resizable = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(:automatic, :automatic)
    	scroll.add @view
    	
    	scroll
	
	end
	
	def modif_commentaire ligne
		if possible_modif?
			mod = DialCommentaire.new @window, ligne[2]
			mod.run { |response| 
				text = ""
				modif = false
				if response.eql?(-5)
					text = mod.memo.buffer.text
					modif = true
				end
				mod.destroy
				ajout_commentaire ligne, text if modif
			}
		end
	end
	
	def ajout_commentaire ligne, text
		ligne[2] = text
		@ligne_modif_id << ligne[0] unless @ligne_modif_id.include?(ligne[0])
		ligne[4] = @pix_trash
		# On détermine le nombre de lignes
		i = -1
		@list_store.each { i += 1 }
		n = @view.selection.selected.path.to_s.to_i
		# Nouvelle ligne si on est sur une ligne de commentaire seul
		if (ligne[0].eql?(0) and i.eql?(n))
			iter = @list_store.append
			iter[4] = @pix_comm
		end		
		
	end
	
	def suppr_ligne ligne, conf=true
	
		if possible_modif?
		
			if conf
				suppr = false
				if MessageController.question_oui_non @window, "Voulez-vous réellement supprimer cette ligne ?"
					suppr = true
				end 
			else
				suppr = true
			end
		
			if suppr
				# Si la ligne était déjà enregistrée dans la base, on la marque à supprimer
				@ligne_supp_id << ligne[0].to_i if ligne[0].to_i>0		

				# On réinitialise les valeurs des champs
				ligne[0] = ligne[1] = 0
				ligne[2] = ""
				ligne[3] = ""
		
				# On détermine le nombre de lignes
				i = -1
				@list_store.each { i += 1 }
				n = @view.selection.selected.path.to_s.to_i
		
				# Puis on retire la ligne du tableau sauf si il s'agit de la dernière ligne
				@list_store.remove @view.selection.selected unless i.eql?(n)
		
				# on recalcule le pieds
				remplir_pieds
			end
		end
	
	end
	
	def pieds
	
		@duree = Gtk::Entry.new
		
		@duree.editable = false
		@duree.width_chars = 8
		@duree.xalign = 1
		
		table_montants = Gtk::Table.new 1,1,false
		
		table_montants.attach( Gtk::Label.new("Durée totale :"), 0, 1, 0, 1, :fill, :fill, 2, 2 )
		table_montants.attach( @duree, 1, 2, 0, 1, :fill, :fill, 2, 2 )
		
		table_montants
	
	end
	
	def ged
		@ged = Ged_box.new @window, type="documents"
	end
	
	def refresh id
	
		@id = id		
		@ligne_modif_id = []
		@ligne_supp_id = []		
		
		if id>0 then
			
			@intervention = Intervention.includes(:tiers).where(:id => id).first
			if @intervention then
				remplir_en_tete @intervention
			end
			
			interventionlignes = Interventionligne.where(:intervention_id => id).order(:id)
			if interventionlignes then
				remplir_lignes interventionlignes
			end
			
			remplir_pieds
		else
			@intervention = Intervention.new
			empty_component
			iter = @list_store.append
			iter[4] = @pix_trash
			@intervention.duree = 0.0
			@mail = ""	
		end
		
		@email.sensitive = !@mail.empty? unless @mail.nil?
	
	end
	
	def remplir_en_tete intervention

		@id_client = intervention.tiers_id
		@client.text = intervention.tiers.nom
		@mail = intervention.tiers.mail
		@client.sensitive = false
		date = intervention.date.to_s
		begin
			@date.text = Date.parse(date).strftime("%d/%m/%Y")
		rescue
		end
		#@transfert.sensitive = (!@transfere and !@type.eql?(5) and !@type.eql?(8))
	end
	
	def remplir_lignes interventionlignes
	
		@list_store.clear
		
		interventionlignes.each { |h| 
			
			iter = @list_store.append
			iter[0] = h.id
			iter[1] = h.intervention_id
			iter[2] = h.commentaire
			iter[3] = "%.2f" % h.duree
			iter[4] = @pix_trash
		}
		
		iter = @list_store.append
		iter[4] = @pix_trash

	end
	
	def remplir_pieds
		
		duree = 0.0
	
		@list_store.each { |model, path, iter|
			ligne = @view.model.get_iter(path)
			duree += ligne[3].to_f unless ligne[3].nil?
		}
		
		duree = duree.round(2)
		
		@duree.text = "%.2f h" % duree
		
	end
	
	def empty_component entierement=true
	
		@client.text = "" if entierement
		@client.sensitive = true if entierement
		@id_client = 0 if entierement
		date = DateTime.now
		@date.text = date.strftime("%d/%m/%Y")
		
		@list_store.clear if entierement
		
		@duree.text = "0 €" if entierement
	
	end
	
	# Indique si le document est modifiable ou pas
	def possible_modif?
		return true
	end
	
	def focus_dernière_ligne
		Thread.new { 
			sleep 0
			# on trouve la dernière ligne du tableau
			npath = nil
			@view.model.each {|model, path, iter| 
				npath = path
			}
			# et on met le focus sur la première colonne.
			@view.set_cursor(npath, @view.get_column(0), true)
		}
	end
	
	def imprimer
		validate
		chemin = @window.config_db.chemin_temp
		Edition.new [@doc.id], chemin
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open","#{chemin}/#{@doc.ref}.pdf"
		else
			if RUBY_PLATFORM.include?("mingw") then 
				system "start","#{chemin}/#{@doc.ref}.pdf"
			else
				
			end
		end
	end
	
	def mailer
		validate
		chemin = @window.config_db.chemin_temp
		Edition.new [@doc.id], chemin
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-email --subject '#{@window.type_doc[@type][:nom].downcase}' --attach #{chemin}/#{@doc.ref}.pdf #{@mail}" 
		else
			if RUBY_PLATFORM.include?("mingw") then 
				# TODO
			else
				
			end
		end
	end
	
	def validate quitter=true
	
		continu = true
		begin
			date = Date.parse(@date.text).strftime("%Y-%m-%d")
		rescue
			continu = false
			erreur = "Il y a un problème de date. Veuillez les vérifier."
			dialog = Gtk::MessageDialog.new @window, Gtk::Dialog::MODAL, Gtk::MessageDialog::ERROR, Gtk::MessageDialog::BUTTONS_CLOSE, erreur
			dialog.title = "Erreur"
			dialog.signal_connect('response') { dialog.destroy }
			dialog.run
			return false			 
		end
		
		if continu
			
			res = !@id_client.eql?(0)  # Il faut qu'un client soit sélectionné pour pouvoir enregistrer le document !
			
			if res then
				
				@intervention.tiers_id = @id_client
				@intervention.date = date
				@intervention.utilisateur_id = @login.user.id
				@intervention.duree = @duree.text.to_f
			
				res = @intervention.save
			else
				erreur = "Vous devez sélectionner un client valide !"
				dialog = Gtk::MessageDialog.new @window, Gtk::Dialog::MODAL, Gtk::MessageDialog::ERROR, Gtk::MessageDialog::BUTTONS_CLOSE, erreur
				dialog.title = "Erreur"
				dialog.signal_connect('response') { dialog.destroy }
				dialog.run				 
			end
		
			if res and !@ligne_modif_id.empty? then
			
				@list_store.each { |model, path, iter|
				
					if !@view.model.get_iter(path)[2].nil?
						id_ligne = @view.model.get_iter(path)[0]
						commentaire = @view.model.get_iter(path)[2]
						duree = @view.model.get_iter(path)[3]
						
						dl = nil
						if id_ligne>0 then
							dl = Interventionligne.find(id_ligne)
						else
							dl = Interventionligne.new
						end
					
						dl.intervention_id = @intervention.id
						dl.commentaire = commentaire
						dl.duree = duree
					
						res = dl.save
				
					end
				
				}
			
			end
		
			if res then 
				# suppression en base des lignes supprimées dans le tableau
				@ligne_supp_id.each { |l|								
					dl = Interventionligne.find(l)				
					if dl then
						res = dl.destroy
						p "Suppresion de ligne : #{l}" if res
					end				
				}
			end
		
			if res
				if quitter
					quit @type
				else
					refresh @intervention.id
				end
			end
			return res
		end
	end
	
	def focus
		@client.grab_focus
	end
	
	def quit statut=nil
		
		@window.liste_interventions.refresh
		@window.affiche @window.liste_interventions
	
	end
	
end

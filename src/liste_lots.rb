# coding: utf-8

class ListeLots_box < Gtk::Box

	attr_reader :view
	
	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		@article = nil
		 
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		@add_new = Gtk::Button.new
		hboxajout = Gtk::Box.new :horizontal, 2
		hboxajout.add Gtk::Image.new( :file => "./resources/icons/add.png" )
		label_button = Gtk::Label.new "Lots"
		
		hboxajout.add label_button
		@add_new.add hboxajout
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add @add_new
		frame = Gtk::Frame.new "Lots"
		
		hbox = Gtk::Box.new :horizontal, 3
		@search = Gtk::Entry.new
		@search.primary_icon_stock = Gtk::Stock::FIND
		@search.secondary_icon_stock = Gtk::Stock::CLEAR
		#hbox.add @search
		
		vbox.pack_start hbox, :expand => false, :fill => false, :padding => 3 
		vbox.pack_start tableau_lots, :expand => true, :fill => true, :padding => 3 
		
		frame.add vbox
		
		@add_new.signal_connect( "clicked" ) {
			@window.article.refresh
			@window.affiche @window.article
		}
		
		@search.signal_connect('activate') {
			if !@search.text.empty? then
				refresh @search.text
				@search.text = ""
			end
		}
		
		return frame
	
	end
	
	def tableau_lots
	
		# list_store (article, lot, qtite)
		@list_store = Gtk::TreeStore.new(String, String, Float)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			#
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Article", renderer_left, :text => 0)
		col.sort_column_id = 0
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Lot", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Stock", renderer_right, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
	  	scroll.set_policy(:automatic, :automatic)
	  	scroll.add @view
	  	
	  	scroll
	
	end
	
	def refresh article=nil
		
		@search.text = ""
		
		res = Articlestock.includes(:article).order(:lot)
		res = res.where(:article_id => article) if article
		res = res.where("qtite>0") if article
		
		if res then		
			remplir res
		end	
	
	end
	
	def remplir res

		@list_store.clear
		
		res.each { |h| 
			
			iter = @list_store.append nil
			iter[0] = h.article.code
			iter[1] = h.lot
			iter[2] = h.qtite
		}
			
	end
	
	def focus
		@search.grab_focus
	end
	
end

# coding: utf-8

class Configuration_box < Gtk::Box

	def initialize window, dial=false
	
		super(:vertical, 3)
		
		set_border_width 10
				
		@window = window
		@login = window.login
		@dial = dial
		@chemin_config = window.config_db.chemin_config
		
		@valider = Gtk::Button.new
		hboxvalider = Gtk::Box.new :horizontal, 2
		hboxvalider.add Gtk::Image.new( :file => "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new "Valider"
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::Box.new :horizontal, 2
		hboxannuler.add Gtk::Image.new( :file => "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new "Annuler"
		@annuler.add hboxannuler
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		@frame = Gtk::Frame.new
		@frame.label = "Configuration"
		vbox.pack_start( @frame, :expand => true, :fill => true, :padding => 3 )
		
		vbox_corps = Gtk::Box.new :vertical, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( notebook, :expand => true, :fill => true, :padding => 3 )		
		
		hbox3 = Gtk::Box.new :horizontal, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, :expand => true, :fill => true, :padding => 3 )
		hbox3.pack_start( @valider, :expand => true, :fill => true, :padding => 3 )
		align1.add hbox3
		
		vbox.pack_start( align1, :expand => false, :fill => false, :padding => 3 ) unless @dial
		
		return vbox
	
	end
	
	def notebook
		
		# Conteneurs
		notebook = Gtk::Notebook.new
		
		# Agencement du notebook
		notebook.append_page societe, Gtk::Label.new("Société")
		notebook.append_page groupe_article, Gtk::Label.new("Groupes d'articles")
		notebook.append_page tarifs, Gtk::Label.new("Tarifs")
		notebook.append_page tvas, Gtk::Label.new("TVA")
		notebook.append_page ged, Gtk::Label.new("GED")

		# Renvoie
		notebook
		
	end
	
	def societe
		# Objets	
		@soc_nom = Gtk::Entry.new
		@soc_adresse = Gtk::TextView.new
		@soc_capital = Gtk::Entry.new
		@soc_tel = Gtk::Entry.new
		@soc_fax = Gtk::Entry.new
		@soc_mail = Gtk::Entry.new
		@soc_site = Gtk::Entry.new
		@soc_siret = Gtk::Entry.new
		@soc_rcs = Gtk::Entry.new
		@soc_naf = Gtk::Entry.new
		@soc_status = Gtk::Entry.new
		@soc_logo = Gtk::Image.new
		visite_site = Gtk::Button.new
		logo_btn = Gtk::Button.new :label => "..."
		
		# Conteneurs
		table_info = Gtk::Table.new 2, 4, false
		scroll_info = Gtk::ScrolledWindow.new
		hbox_compte = Gtk::Box.new :horizontal, 2
		
		# Les labels
		label_nom = Gtk::Alignment.new 0, 0.5, 0, 0
		label_nom.add Gtk::Label.new "Nom de la société :"
		label_adresse = Gtk::Alignment.new 0, 0.5, 0, 0
		label_adresse.add Gtk::Label.new "Adresse :"
		label_capital = Gtk::Alignment.new 0, 0.5, 0, 0
		label_capital.add Gtk::Label.new "Capital de :"
		label_tel = Gtk::Alignment.new 0, 0.5, 0, 0
		label_tel.add Gtk::Label.new "Tél. :"
		label_fax = Gtk::Alignment.new 0, 0.5, 0, 0
		label_fax.add Gtk::Label.new "Fax. :"
		label_mail = Gtk::Alignment.new 0, 0.5, 0, 0
		label_mail.add Gtk::Label.new "Email :"
		label_site = Gtk::Alignment.new 0, 0.5, 0, 0
		label_site.add Gtk::Label.new "Site web :"
		label_siret = Gtk::Alignment.new 0, 0.5, 0, 0
		label_siret.add Gtk::Label.new "SIRET :"
		label_rcs = Gtk::Alignment.new 0, 0.5, 0, 0
		label_rcs.add Gtk::Label.new "RCS :"
		label_naf = Gtk::Alignment.new 0, 0.5, 0, 0
		label_naf.add Gtk::Label.new "NAF :"
		label_status = Gtk::Alignment.new 0, 0.5, 0, 0
		label_status.add Gtk::Label.new "Status :"
		label_logo = Gtk::Alignment.new 0, 0.5, 0, 0
		label_logo.add Gtk::Label.new "Logo :"
		
		# Propriétés des objets
		scroll_info.set_policy :never, :automatic
		scroll_info.shadow_type = :none
		visite_site.image = Gtk::Image.new :file => "resources/icons/internet.png"
		visite_site.signal_connect("clicked") { naviguer unless @soc_site.text.empty? }
		visite_site.tooltip_text = "Visiter le site"
		logo_btn.signal_connect("clicked") { nouveau_logo }
		
		# Agencement du conteneur Informations
		i=0
		table_info.attach( label_status , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_status, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_nom , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_nom, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_adresse , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_adresse, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_tel , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_tel, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_fax , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_fax, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_mail , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_mail, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_site , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_site, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		table_info.attach( visite_site, 2, 3, i, i+1, :fill, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_siret , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_siret, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_rcs , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_rcs, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_naf , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_naf, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_capital , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_capital, 1, 3, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_logo , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @soc_logo, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		table_info.attach( logo_btn, 2, 3, i, i+1, :fill, :fill, 5, 5 )
		
		scroll_info.add_with_viewport table_info
		
		scroll_info
	end
	
	def groupe_article
		
		groupe_model = Gtk::TreeStore.new(Integer, String, Integer)
		@groupe = Gtk::TreeView.new groupe_model
		
		@groupe.signal_connect ("button-release-event") { |tree,event|
			# Gestion du clic droit sur une ligne
			if event.kind_of? Gdk::EventButton and event.button == 3
				npath = @groupe.get_path_at_pos(event.x, event.y)
				if !npath.nil? 
					@groupe.set_cursor(npath[0], nil, false)
					menu_groupe					
				else
					menu_groupe true
				end
			end
		}
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		
		col = Gtk::TreeViewColumn.new("Groupes", renderer_left, :text => 1)
		col.resizable = true
		col.expand = true
		@groupe.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Nb articles", renderer_left, :text => 2)
		col.resizable = true
		@groupe.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	
  	scroll.add_with_viewport @groupe
  	
  	scroll
	
	end
	
	def tarifs
		
		groupe_model = Gtk::TreeStore.new(Integer, String, Integer, String)
		@tarif = Gtk::TreeView.new groupe_model
		
		@tarif.signal_connect ("button-release-event") { |tree,event|
			# Gestion du clic droit sur une ligne
			if event.kind_of? Gdk::EventButton and event.button == 3
				npath = @tarif.get_path_at_pos(event.x, event.y)
				if !npath.nil? 
					@tarif.set_cursor(npath[0], nil, false)
					menu_tarif					
				else
					menu_tarif true
				end
			end
		}
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		
		col = Gtk::TreeViewColumn.new("Tarifs", renderer_left, :text => 1)
		col.resizable = true
		col.expand = true
		col.sort_column_id = 1
		@tarif.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Marge (%)", renderer_left, :text => 3)
		col.resizable = true
		col.sort_column_id = 3
		@tarif.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Nb tiers", renderer_left, :text => 2)
		col.resizable = true
		col.sort_column_id = 2
		@tarif.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	
  	scroll.add_with_viewport @tarif
  	
  	scroll
	
	end
	
	def tvas
		
		@tva = Gtk::TreeView.new Gtk::TreeStore.new(Integer, String, Float, String)
		
		@tva.signal_connect ("button-release-event") { |tree,event|
			# Gestion du clic droit sur une ligne
			if event.kind_of? Gdk::EventButton and event.button == 3
				npath = @tva.get_path_at_pos(event.x, event.y)
				if !npath.nil? 
					@tva.set_cursor(npath[0], nil, false)
					menu_tva					
				else
					menu_tva true
				end
			end
		}
		
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		
		col = Gtk::TreeViewColumn.new("TVA", renderer_left, :text => 1)
		col.resizable = true
		col.expand = true
		col.sort_column_id = 1
		@tva.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Taux (%)", renderer_left, :text => 2)
		col.resizable = true
		col.sort_column_id = 2
		@tva.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Non perçue ?", renderer_left, :text => 3)
		col.resizable = true
		col.sort_column_id = 3
		@tva.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	
  	scroll.add_with_viewport @tva
  	
  	scroll
	
	end
	
	def ged
		
		conf = @window.config_db.conf
		scanner = @window.config_db.scanner
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	
  	table = Gtk::Table.new 10, 2, false
		
		i = 0
		# Le chemin vers les documents
		label_chemin_documents = Gtk::Alignment.new 0, 0.5, 0, 0
		label_chemin_documents.add Gtk::Label.new("Chemin vers les documents:")
		@chemin_documents = Gtk::Entry.new
		@chemin_documents.text = conf["chemin_documents"].to_s unless conf.nil?
		table.attach( label_chemin_documents, 0, 1, i, i+1, :fill, :fill, 2, 2 )	
		table.attach( @chemin_documents, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 2, 2 )
		filechooserbt = Gtk::Button.new
		filechooserbt.image = Gtk::Image.new( :file => "./resources/icons/add.png" )
		filechooserbt.signal_connect("clicked"){ 
			dialog = Gtk::FileChooserDialog.new(:title => "Chemin vers les documents",
						                         			:parent => @window,
						                         			:action => :select_folder,
						                         			:buttons => [[Gtk::Stock::CANCEL, :cancel],[Gtk::Stock::OPEN, :accept]])

			if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
				@chemin_documents.text = dialog.filename
			end
			dialog.destroy
		}
		
		table.attach( filechooserbt, 2, 3, i, i+1, :fill, :fill, 2, 2 )
		
		
		i += 1
		# Le scanner
		label_scanner = Gtk::Alignment.new 0, 0.5, 0, 0
		label_scanner.add Gtk::Label.new("Adresse du scanner:")
		@scanner = Gtk::Entry.new
		@scanner.text = scanner["chemin"].to_s if scanner
		table.attach( label_scanner, 0, 1, i, i+1, :fill, :fill, 2, 2 )	
		table.attach( @scanner, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 2, 2 )
		
		i += 1
		# La résolution du scanner
		label_resolution = Gtk::Alignment.new 0, 0.5, 0, 0
		label_resolution.add Gtk::Label.new("Résolution du scanner (en dpi):")
		@resolution = Gtk::Entry.new
		@resolution.text = scanner["resolution"].to_s if scanner
		table.attach( label_resolution, 0, 1, i, i+1, :fill, :fill, 2, 2 )	
		table.attach( @resolution, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 2, 2 )
		
		i += 1
		# Commande à lancer au démarrage
		label_commande_lancement = Gtk::Alignment.new 0, 0.5, 0, 0
		label_commande_lancement.add Gtk::Label.new("Commande à lancer au démarrage")
		@commande_lancement = Gtk::Entry.new
		@commande_lancement.text = conf["commande_lancement"].to_s unless conf.nil?
		table.attach( label_commande_lancement, 0, 1, i, i+1, :fill, :fill, 2, 2 )	
		table.attach( @commande_lancement, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 2, 2 )
		
		i += 1
		# Commande à lancer a la fermeture
		label_commande_fermeture = Gtk::Alignment.new 0, 0.5, 0, 0
		label_commande_fermeture.add Gtk::Label.new("Commande à lancer à la fermeture")
		@commande_fermeture = Gtk::Entry.new
		@commande_fermeture.text = conf["commande_fermeture"].to_s unless conf.nil?
		table.attach( label_commande_fermeture, 0, 1, i, i+1, :fill, :fill, 2, 2 )
		table.attach( @commande_fermeture, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 2, 2 )
		
		scroll.add_with_viewport table
  	
  	scroll
	
	end
	
	def refresh
	
		refresh_societe
		refresh_ged
		refresh_groupe
		refresh_tarif
		refresh_tva
	
	end
	
	def refresh_societe
		@societe = Societe.first
		@soc_nom.text = @societe.nom.to_s
		@soc_adresse.buffer.text = @societe.adresse.to_s
		@soc_capital.text = @societe.capital.to_s
		@soc_tel.text = @societe.tel.to_s
		@soc_fax.text = @societe.fax.to_s
		@soc_mail.text = @societe.mail.to_s
		@soc_site.text = @societe.site.to_s
		@soc_siret.text = @societe.siret.to_s
		@soc_rcs.text = @societe.rcs.to_s
		@soc_naf.text = @societe.naf.to_s
		@soc_status.text = @societe.status.to_s
		@soc_capital.text = @societe.capital.to_s
		societe = Societe.first
		logo = @window.config_db.conf["chemin_documents"]+"/societe/"+societe.logo.to_s
		@soc_logo.file = logo if File.exist?(logo)
	end
	
	def refresh_groupe
		
		@groupe.model.clear
		
		groupesp = Articlegroupe.where(:parent_id => 0).order(:designation)
		
		if groupesp then
			active_iter = nil
			groupesp.each do |gr|
				iter = @groupe.model.append nil
				iter[0] = gr.id
				iter[1] = gr.designation	
				articles = Article.where(:articlegroupe_id => gr.id)	
				iter[2] = articles.count
				groupese = Articlegroupe.where("parent_id=? AND id<>?", gr.id, 0).order(:designation) 						
				if groupese then
					groupese.each do |gr2|
						iter2 = @groupe.model.append iter
						iter2[0] = gr2['id'].to_i
						iter2[1] = gr2['designation']
						articles = Article.where(:articlegroupe_id => gr2.id)	
						iter2[2] = articles.count
					end
				else
					@window.message_erreur groupese.error
				end
			end
		else
			@window.message_erreur groupesp.error
		end
		
	end
	
	def refresh_tarif
		
		@tarif.model.clear
		
		tarifs = Tarif.where(:vente => true).order(:id)
		
		if tarifs then
			active_iter = nil
			tarifs.each do |t|
				iter = @tarif.model.append nil
				iter[0] = t.id
				iter[1] = t.designation	
				iter[3] = "%.2f" % t.marge
				tiers = Tiers.where(:tarif_id => t.id)	
				iter[2] = tiers.count
			end
		else
			@window.message_erreur tarifs.error
		end
		
	end
	
	def refresh_tva
		
		@tva.model.clear
		
		tvas = Tva.all
		
		if tvas then
			tvas.each do |t|
				iter = @tva.model.append nil
				iter[0] = t.id
				iter[1] = t.designation	
				iter[2] = t.taux
				iter[3] = (t.np ? "Oui" : "Non")
			end
		else
			@window.message_erreur tvas.error
		end
		
	end
	
	def refresh_ged
		@chemin_documents.text = @window.config_db.conf["chemin_documents"].to_s
		@scanner.text = @window.config_db.scanner["chemin"].to_s
		@resolution.text = @window.config_db.scanner["resolution"].to_s
	end
	
	def menu_tarif vide=false
	
		menu = Gtk::Menu.new
		
		add_tb = Gtk::MenuItem.new "Ajouter un tarif"
		add_tb.signal_connect( "activate" ) { 
			dial_add = DialTarif.new @window
			dial_add.run { |response| 
				if response.eql?(-5)
					if !dial_add.designation.text.empty?
						designation = dial_add.designation.text
						marge = dial_add.marge.value
						tarif = Tarif.create :designation => designation, :marge => marge
						tarif.create_articletarif
						refresh_tarif
					end
				end
				dial_add.destroy 			
			}		
		}
		
		menu.append add_tb
		
		if !vide
			modifier_tb = Gtk::MenuItem.new "Modifier le tarif"
			modifier_tb.signal_connect( "activate" ) { 
				id = @tarif.model.get_value( @tarif.selection.selected, 0 )
				dial_add = DialTarif.new @window, id
				dial_add.run { |response| 
					if response.eql?(-5)
						if !dial_add.designation.text.empty?
							designation = dial_add.designation.text
							marge = dial_add.marge.value
							tarif = Tarif.find(id) 
							tarif.maj designation, marge
							refresh_tarif
						end
					end
					dial_add.destroy 			
				}		
			}
			
			menu.append modifier_tb
		
			suppr_tb = Gtk::MenuItem.new "Supprimer le tarif"
			suppr_tb.signal_connect( "activate" ) { 
				if Tarif.where(:vente => true).count > 1
					if MessageController.question_oui_non @window, "Voulez-vous réellement supprimer ce tarif ?"
						id = @tarif.model.get_value( @tarif.selection.selected, 0 )
						tarif = Tarif.find(id)
						tarif.supprimer
						refresh_tarif
					end
				else
					MessageController.erreur @window, "Impossible de supprimer ce tarif, il en faut au moins un !"
				end
			}
		
			menu.append suppr_tb
		end
		
		menu.show_all
		menu.popup(nil, nil, 0, 0)
		
	end
	
	def menu_tva vide=false
	
		menu = Gtk::Menu.new
		
		add_tb = Gtk::MenuItem.new "Ajouter un taux de TVA"
		add_tb.signal_connect( "activate" ) { 
			dial_add = DialTva.new @window
			dial_add.run { |response| 
				if response.eql?(-5)
					if !dial_add.designation.text.empty?
						designation = dial_add.designation.text
						taux = dial_add.taux.value
						np = dial_add.np.active?
						Tva.create :designation => designation, :taux => taux, :np => np
						refresh_tva
					end
				end
				dial_add.destroy 			
			}		
		}
		
		menu.append add_tb
		
		if !vide
			modifier_tb = Gtk::MenuItem.new "Modifier le taux de tva"
			modifier_tb.signal_connect( "activate" ) { 
				id = @tva.model.get_value( @tva.selection.selected, 0 )
				dial_add = DialTva.new @window, id
				dial_add.run { |response| 
					if response.eql?(-5)
						if !dial_add.designation.text.empty?
							designation = dial_add.designation.text
							taux = dial_add.taux.value
							np = dial_add.np.active?
							tva = Tva.find(id) 
							tva.maj designation, taux, np
							refresh_tva
						end
					end
					dial_add.destroy 			
				}		
			}
			
			menu.append modifier_tb
		
		end
		
		menu.show_all
		menu.popup(nil, nil, 0, 0)
		
	end
	
	def menu_groupe vide=false
	
		add_tb = Gtk::MenuItem.new "Ajouter un groupe"
		add_tb.signal_connect( "activate" ) { 
			dial_add = DialGroupe.new @window
			dial_add.run { |response| 
				if response.eql?(-5)
					if !dial_add.designation.text.empty?
						designation = dial_add.designation.text
						parent_id = dial_add.parent.model.get_value(dial_add.parent.active_iter,0)
						couleur = dial_add.couleur.text
						Articlegroupe.create :designation => designation, :parent_id => parent_id, :couleur => couleur
						refresh_groupe
						@window.liste_articles.remplir_groupe
					end
				end
				dial_add.destroy 			
			}		
		}
		
		menu = Gtk::Menu.new
		menu.append add_tb
		
		if !vide
			modifier_tb = Gtk::MenuItem.new "Modifier le groupe"
			modifier_tb.signal_connect( "activate" ) { 
				id = @groupe.model.get_value( @groupe.selection.selected, 0 )
				dial_add = DialGroupe.new @window, id
				dial_add.run { |response| 
					if response.eql?(-5)
						if !dial_add.designation.text.empty?
							designation = dial_add.designation.text
							parent_id = dial_add.parent.model.get_value(dial_add.parent.active_iter,0)
							couleur = dial_add.couleur.text
							gr = Articlegroupe.find(id) 
							gr.designation = designation
							gr.parent_id = parent_id
							gr.couleur = couleur
							gr.save
							refresh_groupe
							@window.liste_articles.remplir_groupe
						end
					end
					dial_add.destroy 			
				}		
			}
		
			suppr_tb = Gtk::MenuItem.new "Supprimer le groupe"
			suppr_tb.signal_connect( "activate" ) { 
				suppr_groupe				
			}
		
			menu.append modifier_tb
			menu.append suppr_tb
		end
		
		menu.show_all
		menu.popup(nil, nil, 0, 0)
		
	end
	
	def suppr_groupe
		if MessageController.question_oui_non @window, "Voulez-vous réellement supprimer ce groupe ?"
			id = @groupe.model.get_value( @groupe.selection.selected, 0 )
			articles = Article.where(:articlegroupe_id => id)
			if articles.count.eql?(0)
				gr = Articlegroupe.where(:parent_id => id)
				if gr.count.eql?(0)
					Articlegroupe.delete(id)
					refresh_groupe
					@window.liste_articles.remplir_groupe
				else
					@window.message_erreur "Ce groupe ne peut pas être supprimé car au moins un sous-groupe y est rattaché."
				end
			else
				@window.message_erreur "Ce groupe ne peut pas être supprimé car au moins un article y est rattaché."
			end
		end
	end
	
	def validate quitter=true
		
		res = false
		if !@soc_nom.text.empty? then
			
			res = save_societe
			
			save_ged if res
			
			if res then
				quit if quitter
			else
				@window.message_erreur res.error
			end
		else
			@window.message_erreur "Vous devez saisir au moins un nom pour votre société !"
			res = false
		end
		return res
	end
	
	def save_societe
		
		@societe = Societe.first
		@societe.nom = @soc_nom.text
		@societe.adresse = @soc_adresse.buffer.text
		@societe.capital = @soc_capital.text
		@societe.tel = @soc_tel.text
		@societe.fax = @soc_fax.text
		@societe.mail = @soc_mail.text
		@societe.site = @soc_site.text
		@societe.siret = @soc_siret.text
		@societe.rcs = @soc_rcs.text
		@societe.naf = @soc_naf.text
		@societe.status = @soc_status.text
		@societe.capital = @soc_capital.text.to_i
		
		res = @societe.save
		
		res
	end
	
	def save_ged
		
		if @window.config_db.scanner.nil?
			@window.config_db.scanner = {"chemin" => "", "resolution" => ""}
		end
		
		@window.config_db.conf["chemin_documents"] = @chemin_documents.text.to_s
		@window.config_db.scanner["chemin"] = @scanner.text.to_s
		@window.config_db.scanner["resolution"] = @resolution.text.to_s
		@window.config_db.conf["commande_lancement"] = @commande_lancement.text.to_s
		@window.config_db.conf["commande_fermeture"] = @commande_fermeture.text.to_s
		@window.config_db.save_config
	end
	
	def naviguer
		
		if !@soc_site.text.include?("http://")
			@soc_site.text = "http://" + @soc_site.text
		end
		
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open #{@soc_site.text}" 
		else
			if RUBY_PLATFORM.include?("mingw") then 
				# TODO
			else
				
			end
		end
	end
	
	def nouveau_logo
		dialog = Gtk::FileChooserDialog.new("Open File",
					                         @window,
					                         Gtk::FileChooser::ACTION_OPEN,
					                         nil,
					                         [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
					                         [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
		filtre = Gtk::FileFilter.new
		filtre.add_pattern("*.png")
		dialog.filter = filtre
		if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
			fichier = dialog.filename
			chemin_dossier = "#{@window.config_db.conf["chemin_documents"]}/societe/"
			chemin = "#{chemin_dossier}#{File.basename(fichier)}"
			# on créer les dossiers si ils n'existent pas déjà
			Dir.mkdir(chemin_dossier) unless File.directory?(chemin_dossier)
			# on fait la copie de fichier
			FileUtils.cp fichier, chemin
			# Puis on créé une entrée en base de donnée pour la ged
			@soc_logo.file = chemin
			# Enfin on enregistre le choix dans la base de données
			societe = Societe.first
			societe.logo = File.basename(fichier)
			societe.save
		end
		dialog.destroy
	end
	
	def quit
	
		@window.tableau_bord.refresh
  		@window.affiche @window.tableau_bord
	
	end
	
end

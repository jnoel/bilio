# coding: utf-8

class ListeArticles_box < Gtk::Box

	attr_reader :view, :search, :add_new
	
	def initialize window, research = false
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		@research = research
		
		@position = [0,0]
		 
		pack_start framebox, :expand => true, :fill => true, :padding => 0
		
		@refresh_active = false
		@filtre.active = 0
		@refresh_active = true
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		@add_new = Gtk::Button.new
		hboxajout = Gtk::Box.new :horizontal, 2
		hboxajout.add Gtk::Image.new( :file => "./resources/icons/add.png" )
		hboxajout.add Gtk::Label.new "Nouvel article"
		@add_new.add hboxajout
		
		# ComboBox pour les rayon
		@rayon_model = Gtk::TreeStore.new(Integer, String)
		@rayon_cb = Gtk::ComboBox.new :model => @rayon_model
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		@rayon_cb.pack_start renderer_left, true
		@rayon_cb.add_attribute renderer_left, "text", 1
		remplir_rayons
		
		# ComboBox pour les filtres
		@filtre = Gtk::ComboBoxText.new
		remplir_filtre
		
		# ComboBox pour le groupe articles
		@groupe_model = Gtk::TreeStore.new(Integer, String)
		@groupe = Gtk::ComboBox.new :model => @groupe_model
		@groupe.pack_start renderer_left, true
		@groupe.add_attribute renderer_left, "text", 1
		remplir_groupe
		
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add @add_new
		frame = Gtk::Frame.new "Articles"
		@compteur = Gtk::Label.new 
		
		hbox = Gtk::Box.new :horizontal, 3
		@search = Gtk::Entry.new
		@search.primary_icon_stock = Gtk::Stock::FIND
		@search.secondary_icon_stock = Gtk::Stock::CLEAR
		
		# Agencement
		hbox.pack_start @search, :expand => true, :fill => true, :padding => 3
		
		hbox.pack_start Gtk::Label.new("Groupe :"), :expand => false, :fill => false, :padding => 3 unless @research
		hbox.pack_start @groupe, :expand => false, :fill => false, :padding => 3 unless @research
		
		hbox.pack_start Gtk::Label.new("Rayon :"), :expand => false, :fill => false, :padding => 3 unless @research
		hbox.pack_start @rayon_cb, :expand => false, :fill => false, :padding => 3 unless @research
			
		hbox.pack_start Gtk::Label.new("Filtres :"), :expand => false, :fill => false, :padding => 3 unless @research
		hbox.pack_start @filtre, :expand => false, :fill => false, :padding => 3 unless @research
		
		hbox.pack_start align_button, :expand => false, :fill => false, :padding => 3
		
		vbox.pack_start hbox, :expand => false, :fill => false, :padding => 3
		vbox.pack_start tableau_articles, :expand => true, :fill => true, :padding => 3
		vbox.pack_start @compteur, :expand => false, :fill => false, :padding => 3 
		@next = Gtk::Button.new :label => ">"
		align_next = Gtk::Alignment.new 1, 1, 0, 0
		align_next.add @next
		frame.add vbox
		
		if !@research
			@add_new.signal_connect( "clicked" ) {
				@window.article.refresh
				@window.affiche @window.article
			}
		end
		
		@search.signal_connect('activate') {
			if !@search.text.empty? then
				refresh @search.text, [0,0]
				#@search.text = ""
				@rayon_cb.active = -1
				@filtre.active = -1
				@groupe.active = -1
			else
				#refresh nil, [0,0]
				@rayon_cb.active = -1
				@filtre.active = 0
				@groupe.active = -1
			end
		}
		
		@search.signal_connect('icon-press') { |widget, pos, event|
			if !@search.text.empty?
				@search.text = ""
				refresh nil, [0,0]
			end
		}
		
		@rayon_cb.signal_connect('changed') {
			changement_rayon
		}
		
		@filtre.signal_connect('changed') {
			changement_filtre
		}
		
		@groupe.signal_connect('changed') {
			changement_groupe
		}
		
		frame
	
	end
	
	def tableau_articles
	
		# list_store (id, code, designation, gencode,  colisage, tva, pu, stock, etiquette, date_modif, gencode2, groupe)
		@list_store = Gtk::TreeStore.new(Integer, String, String, String, Integer, String, Float, Integer, String, String, String, String)
		@view = Gtk::TreeView.new(@list_store)
		@view.signal_connect ("row-activated") { |view, path, column|
			if !@research then
				@window.article.refresh @view.model.get_value @view.selection.selected, 0
				@window.affiche @window.article
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_center = Gtk::CellRendererText.new
		renderer_center.xalign = 0.5
		renderer_center.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("id", renderer_left, :text => 0)
		col.sort_column_id = 0
		col.resizable = true
		#@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Groupe", renderer_left, :text => 11)
		col.sort_column_id = 11
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Code", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Désignation", renderer_left, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Code barre", renderer_left, :text => 3)
		col.sort_column_id = 3
		col.resizable = true
		#@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Code barre 2", renderer_left, :text => 10)
		col.sort_column_id = 10
		col.resizable = true
		#@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Tarif comptant", renderer_right, :text => 6)
		col.sort_column_id = 6
		col.resizable = true
		#@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("TVA", renderer_right, :text => 5)
		col.sort_column_id = 5
		col.resizable = true
		#@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Colisage", renderer_right, :text => 4)
		col.sort_column_id = 4
		col.resizable = true
		#@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Etiquette", renderer_right, :text => 8)
		col.sort_column_id = 8
		col.resizable = true
		#@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Dern. modif", renderer_right, :text => 9)
		col.sort_column_id = 9
		col.resizable = true
		#@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:automatic, :automatic)
  	scroll.add @view
  	
  	scroll
	
	end
	
	def refresh search=nil, limit=[0,100], rayon=nil, filtre=-1, groupe=nil
		
		search = @search.text unless @search.text.empty?
		@position = limit
		
		#articles = Article.includes(:articlegroupe, :tva).order("code")
		articles = Article.joins('LEFT JOIN articlegroupes ON articles.articlegroupe_id = articlegroupes.id').order("code")
		articles = articles.where("articles.designation ILIKE ? OR gencode=? OR gencode2=? OR code ILIKE ?", "%#{search}%", search, search, "%#{search}%") if search
		articles = articles.where(:rayon_id => rayon) if rayon
		articles = articles.where("date_modif IS NULL") if filtre.eql?(1)
		articles = articles.where("gencode IS NULL AND gencode2 IS NULL") if filtre.eql?(2)
		articles = articles.where("note IS NOT NULL AND note<>''") if filtre.eql?(3)
		articles = articles.where(:refaire_etiquette => true) if filtre.eql?(4)
		articles = articles.where(:service => true) if filtre.eql?(5)
		articles = articles.where(:service => false) if filtre.eql?(6)
		articles = articles.where(:achat => true, :vente => false) if filtre.eql?(7)
		articles = articles.where(:achat => false, :vente => true) if filtre.eql?(8)
		articles = articles.where(:suppr => filtre.eql?(9)) 
		articles = articles.where("articlegroupe_id=? OR articlegroupes.parent_id=?", groupe, groupe) if groupe
		
		@compteur.text = "Nombre d'éléments dans la liste : #{articles.count}"
		#remplir_groupe
		
		if articles then
			if articles.count.eql?(1) and !@research and rayon.nil? and filtre<0 and groupe.nil? then
				@window.article.refresh articles.first.id
				@window.affiche @window.article
				@window.article.focus
			else		
				remplir articles
			end
		end	
		
	end
	
	def remplir res
		@list_store.clear
		res.each { |h| 
			iter = @list_store.append nil
			iter[0] = h.id
			iter[1] = h.code
			iter[2] = h.designation
			iter[3] = h.gencode
			#iter[4] = h.colisage
			#iter[5] = h.tva.designation
			#iter[8] = (h.refaire_etiquette ? "A refaire" : "")
			#iter[9] = h.date_modif.to_s
			iter[10] = h.gencode2
			iter[11] = h.articlegroupe.designation unless h.articlegroupe.nil?
		}
	end
	
	def focus
		@search.grab_focus
	end
	
	def remplir_rayons
		
		@rayon_model.clear
		
		rayons = Rayon.order("nom")
				
		if rayons then
			rayons.each do |rayon|
				iter = @rayon_model.append nil
				iter[0] = rayon.id
				iter[1] = rayon.nom
				@rayon_cb.active = -1
			end
		else
			MessageController.erreur @window, rayons.error
		end
		
	end
	
	def remplir_filtre
	
	  @filtre.model.clear
	  @filtre.append_text "Tous les articles"	#0  
	  @filtre.append_text "Articles non modifiés" #1
 	  @filtre.append_text "Articles sans code barre" #2
 	  @filtre.append_text "Articles avec commentaires"	#3
 	  @filtre.append_text "Etiquettes à refaire" #4
 	  @filtre.append_text "Les Services"	#5
 	  @filtre.append_text "Les Produits"	#6
 	  @filtre.append_text "Que les articles en achat"	#7
 	  @filtre.append_text "Que les articles en vente"	#8
 	  @filtre.append_text "Articles supprimés"	#9
 	  
	end
	
	def remplir_groupe nom=nil
		
		@groupe.model.clear
		
		groupesp = Articlegroupe.order(:designation)
		
		if groupesp then
			active_iter = nil
			groupesp.each do |gr|
				if gr.parent_id.eql?(0)
					iter = @groupe_model.append nil
					iter[0] = gr.id
					iter[1] = gr.designation	
					if !nom.nil?
						active_iter = iter if nom.eql?(gr.id)		
					end						
					groupesp.each do |gr2|
						if gr2.parent_id.eql?(gr.id)
							iter2 = @groupe_model.append iter
							iter2[0] = gr2.id.to_i
							iter2[1] = gr2.designation
					
							if !nom.nil?
								active_iter = iter2 if nom.eql?(gr2.id)		
							end	
						end
					end
				end
			end
		
			if !active_iter.nil? then @groupe.active_iter = active_iter end
		else
			MessageController.erreur @window, groupesp.error
		end
		
	end
	
	def changement_rayon
		rayon_id = (@rayon_model.get_value(@rayon_cb.active_iter,0).nil? ? -1 : @rayon_model.get_value(@rayon_cb.active_iter,0))
		refresh nil, 0, rayon_id unless rayon_id.eql?(-1)
		@filtre.active = -1 unless rayon_id.eql?(-1)
		@groupe.active = -1 unless rayon_id.eql?(-1)
	end
	
	def changement_filtre
		refresh search=nil, limit=[0,0], rayon=nil, filtre=@filtre.active unless @filtre.active.eql?(-1) or !@refresh_active
		@rayon_cb.active = -1 unless @filtre.active.eql?(-1)
		@groupe.active = -1 unless @filtre.active.eql?(-1)
	end
	
	def changement_groupe
		groupe_id = (@groupe.active_iter.nil? ? -1 : @groupe.model.get_value(@groupe.active_iter,0))
		refresh search=nil, limit=[0,0], rayon=nil, filtre=-1, groupe=groupe_id unless groupe_id.eql?(-1)
		@rayon_cb.active = -1 unless groupe_id.eql?(-1)
		@filtre.active = -1 unless groupe_id.eql?(-1)
	end
	
end


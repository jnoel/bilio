# coding: utf-8

class Tiers_box < Gtk::Box

	attr_reader :tiers

	def initialize window, dial=false
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@id = 0
		
		@window = window
		@login = window.login
		@dial = dial
		
		@valider = Gtk::Button.new
		hboxvalider = Gtk::Box.new :horizontal, 2
		hboxvalider.add Gtk::Image.new( :file => "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new "Valider"
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::Box.new :horizontal, 2
		hboxannuler.add Gtk::Image.new( :file => "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new "Annuler"
		@annuler.add hboxannuler
		@supprimer = Gtk::Button.new
		hboxsupprimer = Gtk::Box.new :horizontal, 2
		hboxsupprimer.add Gtk::Image.new( :file => "./resources/icons/remove.png" )
		hboxsupprimer.add Gtk::Label.new "Supprimer"
		@supprimer.add hboxsupprimer
		
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
		
		@valider.signal_connect( "clicked" ) { validate true }
		@annuler.signal_connect( "clicked" ) { quit }
		@supprimer.signal_connect( "clicked" ) { supprimer }
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		@frame = Gtk::Frame.new
		@frame.label = "Nouveau tiers"
		vbox.pack_start( @frame, :expand => true, :fill => true, :padding => 3 )
		
		vbox_corps = Gtk::Box.new :vertical, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( en_tete, :expand => true, :fill => true, :padding => 3 )		
		
		hbox3 = Gtk::Box.new :horizontal, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, :expand => true, :fill => true, :padding => 3)
		hbox3.pack_start( @valider,:expand => true, :fill => true, :padding => 3 )
		align1.add hbox3
		
		hbox1 = Gtk::Box.new :horizontal, 2
		hbox1.pack_start @supprimer, :expand => false, :fill => false, :padding => 3
		hbox1.pack_start align1, :expand => true, :fill => true, :padding => 3
		
		vbox.pack_start( hbox1, :expand => false, :fill => false, :padding => 3 ) unless @dial
		
		return vbox
	
	end
	
	def en_tete
	
		# Objets
		@nom = Gtk::Entry.new
		@type_model = Gtk::TreeStore.new(Integer, String)
		@type_cb = Gtk::ComboBox.new :model => @type_model
		@tarif_model = Gtk::TreeStore.new(Integer, String)
		@tarif_cb = Gtk::ComboBox.new :model => @tarif_model
		@adresse = Gtk::TextView.new
		@cp = Gtk::Entry.new
		@ville = Gtk::Entry.new
		@tel = Gtk::Entry.new
		@fax = Gtk::Entry.new
		renderer_left = Gtk::CellRendererText.new
		
		# Propriétés
		@adresse.wrap_mode = :word
		@adresse.set_size_request(10, 60)
		renderer_left.xalign = 0
		
		# Agencement		
		vbox = Gtk::Box.new :vertical, 3
		
		table = Gtk::Table.new 3, 9, false
		
		table.attach( Gtk::Label.new("Nom :"), 0, 1, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @nom, 1, 4, 0, 1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		@type_cb.pack_start renderer_left, true
		@type_cb.add_attribute renderer_left, "text", 1
		table.attach( Gtk::Label.new("Client/Fournisseur :"), 4, 5, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @type_cb, 5, 6, 0, 1, :fill, :fill, 5, 5 )
		
		@tarif_cb.pack_start renderer_left, true
		@tarif_cb.add_attribute renderer_left, "text", 1
		table.attach( Gtk::Label.new("Tarif :"), 6, 7, 0, 1, :fill, :fill, 5, 5 )
		table.attach( @tarif_cb, 7, 8, 0, 1, :fill, :fill, 5, 5 )
		
		scroll_adresse = Gtk::ScrolledWindow.new
		scroll_adresse.add @adresse
		scroll_adresse.set_policy :never, :automatic
		scroll_adresse.shadow_type = :in
		
		table.attach( Gtk::Label.new("Adresse :"), 0, 1, 1, 2, :fill, :fill, 5, 5 )
		table.attach( scroll_adresse, 1, 8, 1, 2, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		table.attach( Gtk::Label.new("Code postal :"), 0, 1, 2, 3, :fill, :fill, 5, 5 )
		table.attach( @cp, 1, 2, 2, 3, :fill, :fill, 2, 3 )
		
		table.attach( Gtk::Label.new("Ville :"), 2, 3, 2, 3, :fill, :fill, 5, 5 )
		table.attach( @ville, 3, 4, 2, 3, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		
		table.attach( Gtk::Label.new("Tél :"), 4, 5, 2, 3, :fill, :fill, 5, 5 )
		table.attach( @tel, 5, 6, 2, 3, :fill, :fill, 2, 3 )
		
		table.attach( Gtk::Label.new("Fax :"), 6, 7, 2, 3, :fill, :fill, 5, 5 )
		table.attach( @fax, 7, 8, 2, 3, :fill, :fill, 2, 3 )
		
		vbox.pack_start table, :expand => false, :fill => false, :padding => 3
		
		vbox.pack_start notebook, :expand => true, :fill => true, :padding => 3
		
		vbox
	
	end
	
	def notebook
	
		# Objets	
		@portable = Gtk::Entry.new
		@mail = Gtk::Entry.new
		@mail2 = Gtk::Entry.new
		@site = Gtk::Entry.new
		@note = Gtk::TextView.new
		envoi_mail = Gtk::Button.new
		envoi_mail2 = Gtk::Button.new
		visite_site = Gtk::Button.new
		@ged = Ged_box.new @window, "tiers"
		
		# Conteneurs
		notebook = Gtk::Notebook.new
		scroll_note = Gtk::ScrolledWindow.new
		table_info = Gtk::Table.new 2, 4, false
		scroll_info = Gtk::ScrolledWindow.new

		# Propriétés des objets
		@note.wrap_mode = :word
		scroll_note.set_policy :never, :automatic
		scroll_note.shadow_type = :none
		scroll_info.set_policy :never, :automatic
		scroll_info.shadow_type = :none
		envoi_mail.image = Gtk::Image.new :file => "resources/icons/saisie.png"
		envoi_mail.signal_connect("clicked") { mailer(@mail.text) unless @mail.text.empty? }
		envoi_mail.tooltip_text = "Ecrire un mail"
		envoi_mail2.image = Gtk::Image.new :file => "resources/icons/saisie.png"
		envoi_mail2.signal_connect("clicked") { mailer(@mail2.text) unless @mail2.text.empty? }
		envoi_mail2.tooltip_text = "Ecrire un mail"
		visite_site.image = Gtk::Image.new :file => "resources/icons/internet.png"
		visite_site.signal_connect("clicked") { naviguer unless @site.text.empty? }
		visite_site.tooltip_text = "Visiter le site"
		
		# Les labels
		label_mail = Gtk::Alignment.new 0, 0.5, 0, 0
		label_mail.add Gtk::Label.new "Email :"
		label_mail2 = Gtk::Alignment.new 0, 0.5, 0, 0
		label_mail2.add Gtk::Label.new "Email secondaire :"
		label_site = Gtk::Alignment.new 0, 0.5, 0, 0
		label_site.add Gtk::Label.new "Site web :"
		label_portable = Gtk::Alignment.new 0, 0.5, 0, 0
		label_portable.add Gtk::Label.new "Tél. portable :"
		
		# Agencement du conteneur Informations
		i=0
		table_info.attach( label_mail , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @mail, 1, 2, i, i+1, Gtk::AttachOptions::EXPAND|Gtk::AttachOptions::FILL, :fill, 5, 5 )
		table_info.attach( envoi_mail, 2, 3, i, i+1, :fill, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_mail2 , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @mail2, 1, 2, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( envoi_mail2, 2, 3, i, i+1, :fill, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_site , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @site, 1, 2, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( visite_site, 2, 3, i, i+1, :fill, :fill, 5, 5 )
		
		i+=1
		table_info.attach( label_portable , 0, 1, i, i+1, :fill, :fill, 5, 5 )
		table_info.attach( @portable, 1, 2, i, i+1, :fill, :fill, 5, 5 )
		
		scroll_info.add_with_viewport table_info
		
		# Agencement du conteneur Note
		scroll_note.add @note
		
		# Agencement du notebook
		notebook.append_page scroll_info, Gtk::Label.new("Informations")
		notebook.append_page scroll_note, Gtk::Label.new("Notes")
		notebook.append_page @ged, Gtk::Label.new("GED")

		# Renvoie
		notebook
		
	end
	
	def refresh id=0
	
		@id = id
		@nom.text = ""
		@type_cb.active = 0
		@adresse.buffer.text = ""
		@cp.text = ""
		@ville.text = ""
		@note.buffer.text = ""
		@tel.text = ""
		@fax.text = ""
		@tiers = nil
		@mail.text = ""
		@mail2.text = ""
		@site.text = ""
		@portable.text = ""
		@supprimer.sensitive = id>0
		
		if id>0 then
			@frame.label = "Tiers n° #{id}"
			
			@tiers = Tiers.find(id)
			
			if @tiers then
				@nom.text = @tiers.nom
				@type_cb.active = -1
				@adresse.buffer.text = @tiers.adresse
				@cp.text = @tiers.cp
				@ville.text = @tiers.ville
				@note.buffer.text = @tiers.note
				remplir_type @tiers.typetiers.id
				remplir_tarif @tiers.tarif_id
				@tel.text = @tiers.tel
				@fax.text = @tiers.fax
				@mail.text = @tiers.mail
				@mail2.text = @tiers.mail2
				@site.text = @tiers.site
				@portable.text = @tiers.portable
			else
				@window.message_erreur tiers.error
			end
			
		else
			@tiers = Tiers.new
			@frame.label = "Nouveau Tiers"
			remplir_type
			remplir_tarif
		end
		
		@ged.refresh @id, @mail.text
	
	end
	
	def validate quitter=false
		
		res = false
		if !@nom.text.empty? then
			tarif_id = @tarif_model.get_value(@tarif_cb.active_iter,0).nil? ? 1 : @tarif_model.get_value(@tarif_cb.active_iter,0)
			type_id = @type_model.get_value(@type_cb.active_iter,0).nil? ? 1 : @type_model.get_value(@type_cb.active_iter,0)
		
			@tiers.nom = @nom.text
			@tiers.typetiers_id = type_id
			@tiers.adresse = @adresse.buffer.text
			@tiers.cp = @cp.text
			@tiers.ville = @ville.text
			@tiers.note = @note.buffer.text
			@tiers.tel = @tel.text
			@tiers.fax = @fax.text
			@tiers.tarif_id = tarif_id
			@tiers.mail = @mail.text
			@tiers.mail2 = @mail2.text
			@tiers.site = @site.text
			@tiers.portable = @portable.text
						
			res = @tiers.save
		
			if res then
				quit if (!@dial and quitter)
			else
				@window.message_erreur res.error
			end
		else
			@window.message_erreur "Vous devez saisir un nom pour ce tiers !"
			res = false
		end
		return res
	end
	
	def focus
		@annuler.grab_focus
	end
	
	def remplir_type type=nil
		
		@type_cb.model.clear
		
		typetiers = Typetiers.order(:id)
		
		if typetiers then
			i=0
			id=-1
			typetiers.each do |t|
				iter = @type_model.append nil
				iter[0] = t.id
				iter[1] = t.designation
				if !type.nil? then 
					id = i if type.eql?(t.id) 
				end
				i += 1
			end
			@type_cb.active = ( type.nil? ? -1 : id )
		else
			@window.message_erreur typetiers.error
		end
		
	end
	
	def remplir_tarif tarif=nil
		
		@tarif_cb.model.clear
		
		tarifs = Tarif.where(:vente => true).order(:id)
				
		if tarifs then
			i=0
			id=-1
			tarifs.each do |t|
				iter = @tarif_model.append nil
				iter[0] = t.id
				iter[1] = t.designation
				if !tarif.nil? then 
					id = i if tarif.eql?(t.id) 
				end
				i += 1
			end
		
			@tarif_cb.active = ( tarif.nil? ? -1 : id )
		else
			@window.message_erreur tarifs.error
		end
		
	end
	
	def mailer mail
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-email #{mail}" 
		else
			if RUBY_PLATFORM.include?("mingw") then 
				# TODO
			else
				
			end
		end
	end
	
	def naviguer
		
		if !@site.text.include?("http://")
			@site.text = "http://" + @site.text
		end
		
		if RUBY_PLATFORM.include?("linux") then 
			system "xdg-open #{@site.text}" 
		else
			if RUBY_PLATFORM.include?("mingw") then 
				# TODO
			else
				
			end
		end
	end
	
	def supprimer
		if MessageController.question_oui_non @window, "Voulez-vous réellement supprimer ce tier ?"
			@tiers.suppr = true
			res = @tiers.save
			quit
		end 
	end
	
	def quit statut=nil
		@window.liste_tiers.refresh #unless statut.nil?
		@window.affiche @window.liste_tiers
		@window.liste_tiers.focus
	end
	
end

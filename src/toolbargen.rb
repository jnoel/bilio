# coding: utf-8

class ToolBarGen_box < Gtk::Toolbar

	def initialize window
	
		super()
		
    @window = window
    
    @user = window.login.user
    
    set_toolbar_style Gtk::Toolbar::Style::BOTH
		
		tableau_bord = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/tb.png" ), :label => "Tableau de bord" )
		article_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/articles.png" ), :label => "Articles" )
		liste_article_tb = Gtk::MenuItem.new "Liste d'article"
		stat_article_tb = Gtk::MenuItem.new "Statistiques"
		tiers_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/tiers.png" ), :label => "Tiers" )
		intervention_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/interventions.png" ), :label => "Interventions" )
    achat_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/documents.png" ), :label => "Achats" )
    devis_four_tb = Gtk::MenuItem.new @window.type_doc[6][:nom]
    commande_four_tb = Gtk::MenuItem.new @window.type_doc[7][:nom]
    facture_four_tb = Gtk::MenuItem.new @window.type_doc[8][:nom]
    vente_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/documents.png" ), :label => "Ventes" )
    devis_tb = Gtk::MenuItem.new @window.type_doc[1][:nom]
    commande_tb = Gtk::MenuItem.new @window.type_doc[2][:nom]
    bl_tb = Gtk::MenuItem.new @window.type_doc[3][:nom]
    facture_tb = Gtk::MenuItem.new @window.type_doc[4][:nom]
    avoir_tb = Gtk::MenuItem.new @window.type_doc[5][:nom]
    banques_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/wxbanker.png" ), :label => "Financier" )
    comptes_tb = Gtk::MenuItem.new "Comptes bancaires"
    assistant_tb = Gtk::MenuItem.new "Assistant de connexion"
    configuration_tb = Gtk::MenuItem.new "Configuration"
    paiements_tb = Gtk::MenuItem.new "Liste des paiements"
    usertb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/users22.png" ), :label => "Utilisateurs" )
    configtb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/config.png" ), :label => "Configuration" )
    requetetb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/config.png" ), :label => "Requêtes" )
    abouttb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/help22.png" ), :label => "A Propos" )
    deconnectertb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/deconnecter.png" ), :label => "Se déconnecter" )
    quittb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/exit.png" ), :label => "Quitter" )
    production_tb = Gtk::ToolButton.new( :icon_widget => Gtk::Image.new( :file => "./resources/icons/production.png" ), :label => "Production (en développement)" )
    machines_tb = Gtk::MenuItem.new "Machines"
    recette_tb = Gtk::MenuItem.new "Recettes"
    atelier_tb = Gtk::MenuItem.new "Atelier de production"
    agenda_tb = Gtk::MenuItem.new "Agenda"
    
    menu_achat = Gtk::Menu.new
		menu_achat.append devis_four_tb
		menu_achat.append commande_four_tb
		menu_achat.append facture_four_tb
		menu_achat.show_all
        
    menu_vente = Gtk::Menu.new
		menu_vente.append devis_tb
		menu_vente.append commande_tb
		menu_vente.append bl_tb
		menu_vente.append facture_tb
		menu_vente.append avoir_tb
		menu_vente.show_all
		
		menu_article = Gtk::Menu.new
		menu_article.append liste_article_tb
		menu_article.append stat_article_tb		
		menu_article.show_all
		
		menu_banques = Gtk::Menu.new
		menu_banques.append comptes_tb	
		menu_banques.append paiements_tb		
		menu_banques.show_all
		
		menu_config = Gtk::Menu.new
		menu_config.append assistant_tb	
		menu_config.append configuration_tb		
		menu_config.show_all
		
		menu_production = Gtk::Menu.new
		menu_production.append machines_tb	
		menu_production.append recette_tb
		menu_production.append atelier_tb	
		menu_production.append agenda_tb	
		menu_production.show_all
  		
		tableau_bord.signal_connect( "clicked" ) {
			window.affiche window.tableau_bord
			window.tableau_bord.refresh
		}
	
		achat_tb.signal_connect( "clicked" ) {
			menu_achat.popup(nil, nil, 0, 0)
		}
					
		vente_tb.signal_connect( "clicked" ) {
			menu_vente.popup(nil, nil, 0, 0)
		}
	
		article_tb.signal_connect( "clicked" ) {
			#window.affiche window.liste_articles
			#window.liste_articles.focus
			menu_article.popup(nil, nil, 0, 0)
		}
	
		intervention_tb.signal_connect( "clicked" ) {
			window.liste_interventions.refresh
			window.affiche window.liste_interventions
		}
	
		banques_tb.signal_connect( "clicked" ) {
			menu_banques.popup(nil, nil, 0, 0)
		}
	
		paiements_tb.signal_connect( "activate" ) { 
			window.affiche window.paiement
			window.paiement.refresh
	  }
		  
	  comptes_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_compte
			window.liste_compte.refresh
	  }
	
		configtb.signal_connect( "clicked" ) {
			#menu_config.popup(nil, nil, 0, 0)
			window.configuration.refresh
			window.affiche window.configuration
		}
	
		assistant_tb.signal_connect( "activate" ) { 
			Assistant.new @window, first=false
		}
	
		configuration_tb.signal_connect( "activate" ) { 
			window.configuration.refresh
			window.affiche window.configuration
		}
	
		requetetb.signal_connect( "clicked" ) {
			#window.datas.create ["stock", "articles_stock"]
		}
	
		abouttb.signal_connect( "clicked" ) { 
			about = About.new window.version
			about.signal_connect('response') { about.destroy }
		}
		  
	  deconnectertb.signal_connect( "clicked" ) { 
	  	window.deconnecter
	  }
	  
	  quittb.signal_connect( "clicked" ) { 
	  	window.quit
	  }
	  
	  usertb.signal_connect( "clicked" ) { 
			window.affiche window.liste_users
    }
        
    tiers_tb.signal_connect( "clicked" ) { 
			window.affiche window.liste_tiers
			window.liste_tiers.refresh
			window.liste_tiers.focus
    }
        
    liste_article_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_articles
			window.liste_articles.refresh 
			window.liste_articles.focus
    }
    
    stat_article_tb.signal_connect( "activate" ) { 
			window.affiche window.stat_articles
			window.stat_articles.refresh 
    }

    devis_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_documents
			window.liste_documents.refresh 1
    }

    commande_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_documents
			window.liste_documents.refresh 2
    }

    bl_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_documents
			window.liste_documents.refresh 3
    }

    facture_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_documents
			window.liste_documents.refresh 4
    }

    avoir_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_documents
			window.liste_documents.refresh 5
    }
        
    devis_four_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_documents
			window.liste_documents.refresh 6
    }
   
    commande_four_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_documents
			window.liste_documents.refresh 7
    }
   
    facture_four_tb.signal_connect( "activate" ) { 
			window.affiche window.liste_documents
			window.liste_documents.refresh 8
    }
    
    production_tb.signal_connect( "clicked" ) { 
			menu_production.popup(nil, nil, 0, 0)
    }
    
    atelier_tb.signal_connect( "activate" ) { 
			window.affiche window.atelier
    }
    
    recette_tb.signal_connect( "activate" ) { 
			window.affiche window.recette
    }
    
    tool = []    
    tool += [tableau_bord, Gtk::SeparatorToolItem.new] if @user.droit?("Tableau de bord")
    tool += [article_tb, Gtk::SeparatorToolItem.new] if @user.droit?("Articles")
    tool += [tiers_tb, Gtk::SeparatorToolItem.new] if @user.droit?("Tiers")
    tool += [production_tb, Gtk::SeparatorToolItem.new] if @user.droit?("Production")
    tool += [intervention_tb, Gtk::SeparatorToolItem.new] if @user.droit?("Interventions")
    tool << achat_tb if @user.droit?("Achats")
    tool << vente_tb if @user.droit?("Ventes")
    tool += [Gtk::SeparatorToolItem.new, banques_tb] if @user.droit?("Financier")
    tool += [Gtk::SeparatorToolItem.new, usertb, configtb, Gtk::SeparatorToolItem.new, abouttb, deconnectertb, quittb]
        
    tool.each_index { |i| 
    	self.insert tool[i], i
    	tool[i].sensitive = false unless ( i.eql?( tool.count-1 ) or i.eql?( tool.count-2 ) )
    }
	
	end

end

#!/usr/bin/env ruby
# coding: utf-8

#
# Need of scanimage (sane), and convert (imagemagik) installed on system
#

class Scan
	
	def initialize conf
		@conf = conf.conf
		@temp = conf.chemin_temp
		@chemin_documents = @conf["chemin_documents"] if @conf["chemin_documents"]
		@scanner = conf.scanner["chemin"] if conf.scanner
		@resolution = conf.scanner["resolution"].to_i if conf.scanner
	end

	def scanner mode, page=1
		image = "scan#{page}.pnm"
		puts `scanimage -d "#{@scanner}" --resolution #{@resolution}dpi -l 0mm -t 0mm -x 210mm -y 297mm --mode #{mode} > "#{File.join(@temp, image)}"`
	end
	
	def creer_dossier id, type
		if File.directory?(@chemin_documents)
			chemin = "#{@chemin_documents}/#{type}/"
			Dir.mkdir(chemin) unless File.directory?(chemin)
			chemin += "#{id}/"
			Dir.mkdir(chemin) unless File.directory?(chemin)
			return chemin
		else
			puts "Erreur du chemin de base"
			return nil
		end
	end

	def convertir chemin, page=1
		liste = ""
		(1..page).each do |p|
			# Utilisation de imagemagick pour convertir les images
			image = File.join(@temp, "scan#{p}.pnm")
			image_conv = File.join(@temp, "scan#{p}.jpg")
			puts `convert #{image} #{image_conv}`
			liste += "#{image_conv} "
		end
		puts `convert -depth #{@resolution} -resize 1000x1500 #{liste} #{chemin}`

	end

	def nettoyer page=1
		(1..page).each do |p|
			image = File.join(@temp, "scan#{p}.pnm")
			image_conv = File.join(@temp, "scan#{p}.jpg")
			File.delete("image") if File.exist?("image")
			File.delete("image_conv") if File.exist?("image_conv")
		end
	end
	
	def renommer nom, chemin, ext
		nom_def = nom
		i=0
		while File.exist?("#{chemin}#{nom_def}#{ext}") do
			i += 1
			nom_def = nom + i.to_s
		end
		nom_def
	end

	def acquisition nom, id, type, ext=".pdf", mode="Gray"
		@chemin_documents = @conf["chemin_documents"] if @conf["chemin_documents"]
		@scanner = @conf["scanner"] if @conf["scanner"]
		@resolution = (@conf["resolution"].to_i>0 ? @conf["resolution"].to_i : 300 ) if @conf["resolution"]
		chemin = creer_dossier id, type
		if chemin
			nom_def = renommer nom, chemin, ext
			page = 1
			nouvelle_page = true
			while nouvelle_page do
				scanner mode, page
				if MessageController.question_oui_non @window, "Voulez-vous ajouter une autre page à ce document ?"
					page += 1
				else
					nouvelle_page = false
				end 
			end
			
			convertir "#{chemin}#{nom_def}#{ext}", page
			nettoyer page
			if type.eql?("tiers")
				ged = Gedtiers.new
				ged.tiers_id = id
			elsif type.eql?("documents")
				ged = Geddocument.new
				ged.document_id = id
			elsif type.eql?("articles")
				ged = Gedarticle.new
				ged.article_id = id
			elsif type.eql?("comptes")
				ged = Gedcompte.new
				ged.compte_id = id
			end
			ged.fichier = nom_def+ext
			ged.save
		end
	end
	
end

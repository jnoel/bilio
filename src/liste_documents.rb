# coding: utf-8

class ListeDocuments_box < Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		
		@type_document = 1
		@activer_combo = false
		self.pack_start framebox, :expand => true, :fill => true, :padding => 0
		@activer_combo = true
	
	end
	
	def framebox
	
		vbox = Gtk::Box.new :vertical, 3
		
		add_new = Gtk::Button.new
		hboxajout = Gtk::Box.new :horizontal, 2
		hboxajout.add Gtk::Image.new( :file => "./resources/icons/add.png" )
		@label_button = Gtk::Label.new "Nouveau"
		hboxajout.add @label_button
		add_new.add hboxajout
		
		print = Gtk::Button.new
		hbox_print = Gtk::Box.new :horizontal, 2
		hbox_print.add Gtk::Image.new( :file => "./resources/icons/print.png" )
		label_print = Gtk::Label.new "Imprimer"
		hbox_print.add label_print
		print.add hbox_print
		
		hb = Gtk::Box.new :horizontal, 2
		hb.add print
		hb.add add_new
		
		align_button = Gtk::Alignment.new 1, 0, 0, 0
		align_button.add hb
		
		table_pieds = Gtk::Table.new 1, 3, false
		@compteur = Gtk::Label.new
		@total_ht = Gtk::Label.new
		@total_ttc = Gtk::Label.new
		table_pieds.attach( @compteur, 0, 1, 0, 1, :fill, :fill, 5, 5 )
		table_pieds.attach( @total_ht, 1, 2, 0, 1, :fill, :fill, 5, 5 )
		table_pieds.attach( @total_ttc, 2, 3, 0, 1, :fill, :fill, 5, 5 )
		
		hbox = Gtk::Box.new :horizontal, 3
		@search = Gtk::Entry.new
		@search.primary_icon_stock = Gtk::Stock::FIND
		@search.secondary_icon_stock = Gtk::Stock::CLEAR
		hbox.pack_start @search, :expand => true, :fill => true, :padding => 3
		
		@filtre = Gtk::ComboBoxText.new
		hbox.pack_start Gtk::Label.new("Filtres :"), :expand => false, :fill => false, :padding => 3
		hbox.pack_start @filtre, :expand => false, :fill => false, :padding => 3
		
		@annee = Gtk::ComboBoxText.new
		remplir_annee
		@annee.active = 0
		
		@annee.signal_connect('changed') {
			refresh_docs if @activer_combo
		}
		
		hbox.pack_start Gtk::Label.new("Année :"), :expand => false, :fill => false, :padding => 3
		hbox.pack_start @annee, :expand => false, :fill => false, :padding => 3
		
		hbox.pack_start align_button, :expand => false, :fill => false, :padding => 3
		
		vbox.pack_start hbox, :expand => false, :fill => false, :padding => 3
		vbox.pack_start tableau_documents, :expand => true, :fill => true, :padding => 3
		vbox.pack_start table_pieds, :expand => false, :fill => false, :padding => 3
		@frame = Gtk::Frame.new
		@frame.add vbox
		
		add_new.signal_connect( "clicked" ) {
			@window.document.refresh 0, @type_document
			@window.affiche @window.document
			@window.document.focus
		}
		
		print.signal_connect( "clicked" ) {
			imprimer
		}
		
		@filtre.signal_connect('changed') {
			refresh_docs if @activer_combo
		}
		
		@search.signal_connect('activate') {
			if !@search.text.empty? 
				refresh_docs
			else
				refresh_docs @type_document, nil
			end
		}
		
		@search.signal_connect('icon-press') { |widget, pos, event|
			if !@search.text.empty?
				@search.text = ""
				refresh_docs @type_document, nil
			end
		}
		
		@frame
	
	end
	
	def tableau_documents
	
		@pix_paye = Gdk::Pixbuf.new("resources/icons/paye.png", 16, 16)
		@pix_partiel = Gdk::Pixbuf.new("resources/icons/partiel.png", 16, 16)
		@pix_impaye = Gdk::Pixbuf.new("resources/icons/impaye.png", 16, 16)
		@pix_vide = Gdk::Pixbuf.new("resources/icons/empty.png", 16, 16)
		
		# list_store (id, client, date_document, note, type_document, date_livraison, auteur, montant_ht, montant_ttc, ref, date francaise, image, status, httri, ttctri, ref2)
		@list_store = Gtk::ListStore.new(Integer, String, String, String, String, String, String, String, String, String, String, Gdk::Pixbuf, Integer, Float, Float, String)
		@view = Gtk::TreeView.new(@list_store)
		
		@view.selection.mode = :multiple
		
		@view.signal_connect ("row-activated") { |view, path, column|
			i = nil
			@view.selection.selected_each {|model, path, iter| i=iter}
			unless i.nil?
				@window.document.refresh @view.model.get_value(i, 0), @type_document
				@window.affiche @window.document		 
			end
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Référence", renderer_left, :text => 9)
		col.sort_column_id = 9
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Tiers", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		col.expand = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Référence secondaire", renderer_left, :text => 15)
		col.sort_column_id = 15
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Date du document", renderer_right, :text => 10)
		col.sort_column_id = 2
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Montant HT", renderer_right, :text => 7)
		col.sort_column_id = 13
		col.resizable = true
		@view.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Montant TTC", renderer_right, :text => 8)
		col.sort_column_id = 14
		col.resizable = true
		@view.append_column(col)
		
		renderer_pix = Gtk::CellRendererPixbuf.new		
		col = Gtk::TreeViewColumn.new("", renderer_pix, :pixbuf => 11)
		col.sort_column_id = 12
		col.resizable = false
		@view.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
    	scroll.set_policy(:automatic, :automatic)
    	scroll.add @view
    	
	
	end
	
	def refresh type_document, term=nil, where=nil
	
		@type_document = type_document
		
		@activer_combo = false
		remplir_filtre type_document
		@filtre.active = 0
		@activer_combo = true
		
		@frame.label = @window.type_doc[@type_document][:nom]
		
		case type_document
			when 1, 3, 6  # Devis et BL
				@label_button.text = "Nouveau "
			when 5  # Avoir
				@label_button.text = "Nouvel "
			else # Facure et Commande
				@label_button.text = "Nouvelle "
		end
		
		@label_button.text += @window.type_doc[type_document][:nom].downcase 
		
		refresh_docs type_document, term, where
		
	end
	
	def refresh_docs type_document=nil, term=nil, filtre=nil, annee=nil
		
		type_document = @type_document unless type_document
		annee = @annee.active_text.to_i unless annee
		filtre = clause_filtre unless filtre
		term = @search.text unless (@search.text.empty? or term)
		
		docs = Document.where(:documenttype_id => @type_document, :suppr => false)
		docs = docs.joins(:tiers).where("documents.id=? OR tiers.nom ILIKE ? OR documents.ref ILIKE ? OR documents.ref2 ILIKE ?", term.to_i, "%#{term}%", "%#{term}%", "%#{term}%") unless term.to_s.empty?
		docs = docs.where(filtre) if filtre
		docs = docs.where('extract(year from date_document) = ?', annee) if annee
		docs = docs.order(:date_document => :desc)
		
		remplir docs
	end
	
	def remplir docs

		totalht = totalttc = 0.0
		
		@list_store.clear
		
		docs.each do |d|
			iter = @list_store.append
			iter[0] = d.id
			iter[1] = d.tiers.nom
			iter[2] = d.date_document.to_s
			iter[3] = d.note
			iter[4] = d.documenttype.designation
			iter[5] = d.date_livraison.to_s
			iter[6] = d.utilisateur.firstname + " " + d.utilisateur.lastname
			iter[7] = "%.2f €" % d.montant_ht
			totalht += d.montant_ht
			iter[8] = "%.2f €" % d.montant_ttc
			totalttc += d.montant_ttc
			iter[9] = d.ref
			iter[10] = d.date_document.strftime("%d/%m/%Y")
			if (facture_client?(@type_document) or facture_fournisseur?(@type_document))
				if d.montant_regle.eql?(0.0)
					iter[11] = @pix_impaye
					iter[12] = 0
				elsif d.montant_regle<d.montant_ttc
					iter[11] = @pix_partiel
					iter[12] = 1
				else
					iter[11] = @pix_paye
					iter[12] = 2
				end
			else
				iter[11] = @pix_vide
			end
			iter[13] = d.montant_ht
			iter[14] = d.montant_ttc
			iter[15] = d.ref2.to_s
		end
		
		@list_store.set_sort_column_id(2, order = :descending)
		
		@compteur.text = "Nombre d'éléments dans la liste : #{docs.count}"
		@total_ht.text = "Total HT = #{'%.2f €' % totalht}"
		@total_ttc.text = "Total TTC = #{'%.2f €' % totalttc}"
			
	end
	
	def remplir_filtre type_doc
		@filtre.model.clear
		@filtre.append_text "Tout" #0
		if (facture_client?(type_doc) or facture_fournisseur?(type_doc))
			@filtre.append_text "Impayées" #1
			@filtre.append_text "Payées" #2
		end
	end
	
	def remplir_annee
		
		doc = Document.find_by_sql("SELECT MIN(date_document) AS min, MAX(date_document) AS max FROM documents").first
		if doc.min and doc.max
			min = doc.min.year
			max = doc.max.year
			(min..max).to_a.reverse.each do |a|
				@annee.append_text a.to_s
			end
		else
			@annee.append_text Date.today.year.to_s
		end
		
	end
	
	def clause_filtre
		where = nil
		case @filtre.active
			when 1
				where = "montant_regle<montant_ttc"
			when 2
				where = "montant_regle>=montant_ttc"
		end
		return where
	end
	
	def imprimer
	
		ids = []
		@view.selection.selected_each { |model, path, iter|
			ids << @view.model.get_value(iter, 0)
		}
		
		if ids.count>0
			chemin = @window.config_db.chemin_temp
			f = Edition.new ids, chemin, @window
			if RUBY_PLATFORM.include?("linux") then 
				system "xdg-open","#{f.fichier}"
			else
				if RUBY_PLATFORM.include?("mingw") then 
					system "start","#{f.fichier}"
				else
				
				end
			end
		end	
	end
	
	def facture_client? type_doc
		return type_doc.eql?(4)		
	end
	
	def facture_fournisseur? type_doc
		return type_doc.eql?(8)		
	end
	
	def devis_client? type_doc
		return type_doc.eql?(1)		
	end
	
	def devis_fournisseur? type_doc
		return type_doc.eql?(6)		
	end
	
	def commande_client? type_doc
		return type_doc.eql?(2)		
	end
	
	def commande_fournisseur? type_doc
		return type_doc.eql?(7)		
	end
	
	def bl_client? type_doc
		return type_doc.eql?(3)
	end
	
	def avoir_fournisseur? type_doc
		return type_doc.eql?(5)		
	end
	
end

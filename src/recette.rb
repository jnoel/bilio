# coding: utf-8

class Recette_box < Gtk::Box
	
	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login	
		
		mach = MachineRecette.new :nom => "Presse agrume", :cellule => [0,1], :taille => 2
		mach2 = MachineRecette.new :nom => "Presse agrume", :cellule => [2,1], :taille => 1
		mach3 = MachineRecette.new :nom => "Confiseuse", :cellule => [1,3], :taille => 3
		mach4 = MachineRecette.new :nom => "Mise en bocal", :cellule => [2,6], :taille => 3
		eau = ProduitRecette.new :nom => "Eau", :quantite => 0, :unite => "L", :cellule => [1,0], :taille => 1, :obligatoire => false
		tangor = ProduitRecette.new :nom => "Tangor", :quantite => 0, :unite => "kg", :cellule => [0,0], :taille => 1
		jus_tangor = ProduitRecette.new :nom => "Jus de tangor", :quantite => 0, :unite => "L", :cellule => [1,2], :taille => 1
		citron = ProduitRecette.new :nom => "Citron", :quantite => 0, :unite => "kg", :cellule => [2,0], :taille => 1
		jus_citron = ProduitRecette.new :nom => "Jus de citron", :quantite => 0, :unite => "L", :cellule => [2,2], :taille => 1
		sucre = ProduitRecette.new :nom => "Sucre", :quantite => 0, :unite => "kg", :cellule => [3,0], :taille => 3
		confiture = ProduitRecette.new :nom => "Confiture de tangor", :quantite => 0, :unite => "kg", :cellule => [2,4], :taille => 2
		bocaux = ProduitRecette.new :nom => "Pots vide", :quantite => 0, :unite => "", :cellule => [4,0], :taille => 6
		pots = ProduitRecette.new :nom => "Pots de confiture de tangor", :quantite => 0, :unite => "", :cellule => [3,7], :taille => 1
		@machines = [mach, mach2, mach3, mach4]
		@produits = [tangor, eau, jus_tangor, citron, jus_citron, sucre, confiture, bocaux, pots]
		@motion = nil
		
		@machine_selectionnee = nil
		@produit_selectionne = nil
		
		@nb_cases_x = 10
		@nb_cases_y = 10
		@marge = 25
		
		self.pack_start frame, :expand => true, :fill => true, :padding => 0
		
	end
	
	def frame
		framebox = Gtk::Frame.new "Recette"
		hbox = Gtk::Box.new :horizontal, 2
		hbox.pack_start proprietes, :expand => false, :fill => false, :padding => 2
		hbox.pack_start drawing, :expand => true, :fill => true, :padding => 2
		framebox.add hbox
		return framebox
	end
	
	def proprietes
		table = Gtk::Table.new 2, 5, false
		i=0
		table.attach( Gtk::Label.new("Nom :"), 1, 2, i, i+1, :fill, :fill, 2, 2 )
		@machine_name_entry = Gtk::Entry.new
		table.attach( @machine_name_entry, 2, 3, i, i+1, :fill, :fill, 2, 2 )
		i+=1
		table.attach( Gtk::Label.new("Taille :"), 1, 2, i, i+1, :fill, :fill, 2, 2 )
		@machine_taille_spin = Gtk::SpinButton.new(1, 10, 1)
		table.attach( @machine_taille_spin, 2, 3, i, i+1, :fill, :fill, 2, 2 )
		i+=1
		table.attach( Gtk::Label.new("Oblig :"), 1, 2, i, i+1, :fill, :fill, 2, 2 )
		@produit_obligatoire_check = Gtk::CheckButton.new
		table.attach( @produit_obligatoire_check, 2, 3, i, i+1, :fill, :fill, 2, 2 )
		i+=1
		table.attach( Gtk::Separator.new(:horizontal), 1, 3, i, i+1, :fill, :fill, 2, 2 )
		i+=1
		table.attach( Gtk::Label.new("Qtité :"), 1, 2, i, i+1, :fill, :fill, 2, 2 )
		@produit_qtite_spin = Gtk::SpinButton.new(0, 9999999, 1)
		table.attach( @produit_qtite_spin, 2, 3, i, i+1, :fill, :fill, 2, 2 )
		i+=1
		table.attach( Gtk::Label.new("Temps :"), 1, 2, i, i+1, :fill, :fill, 2, 2 )
		@machine_temps_spin = Gtk::SpinButton.new(0, 9999999, 0.5)
		@machine_temps_spin.digits = 1
		table.attach( @machine_temps_spin, 2, 3, i, i+1, :fill, :fill, 2, 2 )
		
		@machine_name_entry.sensitive = false
		@machine_taille_spin.sensitive = false
		@machine_temps_spin.sensitive = false
		@produit_qtite_spin.sensitive = false
		@produit_obligatoire_check.sensitive = false
		
		@machine_name_entry.signal_connect("changed") do
			if (@machine_selectionnee and @machine_name_entry.sensitive?)
				@machine_selectionnee.nom = @machine_name_entry.text
				on_expose @d
			elsif (@produit_selectionne and @machine_name_entry.sensitive?)
				@produit_selectionne.nom = @machine_name_entry.text
				on_expose @d
			end
		end
		
		@machine_taille_spin.signal_connect("changed") do
			if (@machine_selectionnee and @machine_taille_spin.sensitive?)
				@machine_selectionnee.taille = @machine_taille_spin.value
				on_expose @d
			elsif (@produit_selectionne and @machine_taille_spin.sensitive?)
				@produit_selectionne.taille = @machine_taille_spin.value
				on_expose @d
			end
		end
		
		@produit_qtite_spin.signal_connect("changed") do
			if (@produit_selectionne and @produit_qtite_spin.sensitive?)
				@produit_selectionne.quantite = @produit_qtite_spin.value
				on_expose @d
			end
		end
		
		@machine_temps_spin.signal_connect("changed") do
			if (@machine_selectionnee and @machine_temps_spin.sensitive?)
				@machine_selectionnee.temps = @machine_temps_spin.value
				on_expose @d
			end
		end
		
		@produit_obligatoire_check.signal_connect("toggled") do
			if (@produit_selectionne and @produit_obligatoire_check.sensitive?)
				@produit_selectionne.obligatoire = @produit_obligatoire_check.active?
				on_expose @d
			end
		end
		
		return table
	end
	
	def drawing
		@d = Gtk::DrawingArea.new
		@d.add_events Gdk::Event::BUTTON_PRESS_MASK
		@d.add_events Gdk::Event::POINTER_MOTION_MASK
		@d.add_events Gdk::Event::SCROLL_MASK
		@d.signal_connect "draw" do  
      on_expose @d
    end
    @d.signal_connect "button-press-event" do |widget, event|
			device = @d.window.get_device_position(event.device)
			pointer = [device[1], device[2]]
			ncase = num_case(pointer)
			
			puts "#{pointer} => #{ncase}"
			
			# Est-ce qu'on clique sur une machine ?
			m = sur_machine?(ncase)
			if m
				@machine_selectionnee = m
				@produit_selectionne = nil
				@machine_name_entry.text = @machine_selectionnee.nom
				@machine_taille_spin.value = @machine_selectionnee.taille
				@machine_temps_spin.value = @machine_selectionnee.temps
				@produit_qtite_spin.value = 0
				@machine_name_entry.sensitive = true
				@machine_taille_spin.sensitive = true
				@machine_temps_spin.sensitive = machine_on?(m)
				@produit_qtite_spin.sensitive = false
				@produit_obligatoire_check.sensitive = false
			else
				# Est-ce qu'on clique sur un produit ?
				p = sur_produit?(ncase)
				if p
					@machine_selectionnee = nil
					@machine_temps_spin.sensitive = false
					@produit_selectionne = p
					@machine_name_entry.text = @produit_selectionne.nom
					@produit_qtite_spin.value = @produit_selectionne.quantite
					@machine_taille_spin.value = @produit_selectionne.taille
					@produit_obligatoire_check.active = @produit_selectionne.obligatoire
					@machine_name_entry.sensitive = true
					m = sur_machine?(@produit_selectionne.cellule_dessus)
					m2 = sur_machine?(@produit_selectionne.cellule_dessous)
					if m
						if m2
							@produit_qtite_spin.sensitive = (m.temps>0.0 and !(m2.temps>0.0))
						else
							@produit_qtite_spin.sensitive = m.temps>0.0
						end
					elsif m2
						@produit_qtite_spin.sensitive = !(m2.temps>0.0)
					else
						@produit_qtite_spin.sensitive = true
					end
					@machine_taille_spin.sensitive = true
					@produit_obligatoire_check.sensitive = true
				else
					@machine_selectionnee = nil
					@produit_selectionne = nil
					@machine_name_entry.sensitive = false
					@machine_taille_spin.sensitive = false
					@machine_temps_spin.sensitive = false
					@produit_qtite_spin.sensitive = false
					@produit_obligatoire_check.sensitive = false
					@machine_name_entry.text = ""
					@machine_taille_spin.value = 1
					@machine_temps_spin.value = 0
					@produit_qtite_spin.value = 0
				end
			end
  	end
  	@d.signal_connect "scroll-event" do |widget, event|
  		if event.direction == :up
  			@nb_cases_x -= 1 if @nb_cases_x > 1
  			@nb_cases_y -= 1 if @nb_cases_y > 1
			elsif @nb_cases_x<25
				@nb_cases_x += 1
  			@nb_cases_y += 1
			end
			on_expose @d
  	end
		return @d
	end
	
	def on_expose d
		@cr = d.window.create_cairo_context 
		@largeur_ecran = @d.allocation.width
		@hauteur_ecran = @d.allocation.height
		fond
		#quadrillage
		dessine_objets
		dessine_motion
	end
	
	def largeur_hauteur_case
		return (@largeur_ecran-@marge*2)/@nb_cases_x, (@hauteur_ecran-@marge*2)/@nb_cases_y
	end
	
	def fond
		@cr.rectangle 0, 0, @d.allocation.width, @d.allocation.height
		@cr.set_source_rgb 1, 1, 1 #blanc
		@cr.fill
	end
	
	def quadrillage
		@cr.set_source_rgb 0.8, 0.8, 0.8 #gris
		largeur, hauteur = largeur_hauteur_case
		@cr.set_dash [5], 1
		# Préparer les lignes pointillées horizontales
		(0..@nb_cases_x).to_a.each do |i|
			@cr.move_to i*largeur+@marge, @marge
			@cr.line_to i*largeur+@marge, @hauteur_ecran-@marge		
		end
		# Préparer les lignes pointillées verticales
		(0..@nb_cases_y).to_a.each do |i|
			@cr.move_to @marge, i*hauteur+@marge
			@cr.line_to @largeur_ecran-@marge, i*hauteur+@marge
		end
		# Tracer toutes les lignes pointillées
		@cr.stroke
	end
	
	def num_case pointer
		ncase = nil
		largeur, hauteur = largeur_hauteur_case
		x = (largeur.eql?(0) ? -1 : ((pointer[0]-@marge)/largeur))
		y = (hauteur.eql?(0) ? -1 : ((pointer[1]-@marge)/hauteur))
		if (0..@nb_cases_x-1).to_a.include?(x) and (0..@nb_cases_y-1).to_a.include?(y)
			ncase = [x, y]
		end
		return ncase
	end
	
	def coordonnees ncase
		coordonnees = [nil, nil]
		if ncase
			w, h = largeur_hauteur_case
			coordonnees = [ncase[0]*w+@marge, ncase[1]*h+@marge]
		end
		return coordonnees		
	end
	
	def dessine_objets
		@machines.each do |machine|
			dessine_machine machine
		end
		@produits.each do |produit|
			fleche produit
		end
	end
	
	def dessine_motion
		if @motion
			if @machines.include?(@motion)
				#rectangle @motion, [1, 0, 0, 0.5] 
			else
				rectangle @motion
			end
		end
	end
	
	def rectangle ncase, couleur=[0, 1, 0, 0.2]
		@cr.set_source_rgba couleur[0], couleur[1], couleur[2], couleur[3] #rouge
		w, h = largeur_hauteur_case
		x, y = coordonnees ncase
		@cr.rectangle x, y, w, h
		@cr.fill
	end
	
	def dessine_machine machine
		if machine.temps>0.0
			@cr.set_source_rgb 0.8, 1, 0.8 #rouge
		elsif machine_on?(machine)
			@cr.set_source_rgb 1, 0.8, 0.8 #rouge
		else
			@cr.set_source_rgb 0.8, 0.8, 0.8 #gris
		end
		marge = 5
		w, h = largeur_hauteur_case
		x, y = coordonnees machine.cellule
		if x and y and w and h
			x += marge
			y += marge
			h -= 2*marge
			w *= machine.taille
			w -= 2*marge
			radius = h/5
			degrees = Math::PI / 180.0
			@cr.move_to x, y
			@cr.arc x + w - radius, y + radius, radius, -90 * degrees, 0 * degrees
			@cr.arc x + w - radius, y + h - radius, radius, 0 * degrees, 90 * degrees
			@cr.arc x + radius, y + h - radius, radius, 90 * degrees, 180 * degrees
			@cr.arc x + radius, y + radius, radius, 180 * degrees, 270 * degrees
			@cr.close_path
			@cr.fill
			if machine.temps>0.0
				texte machine.nom, x+w/2, y+h/2-calcule_size(14)/2, w
				texte "#{machine.temps}h", x+w/2, y+h/2+calcule_size(14)/2, w, 12
			else
				texte machine.nom, x+w/2, y+h/2, w
			end
		end
	end
	
	def texte string, x, y, w, size=14, color=[0,0,0]
		size = calcule_size size
		@cr.set_font_size size
		info = @cr.text_extents string
		while (w<info.width and string.size>3)
			string = string[0,string.size-4].to_s+"..."
			info = @cr.text_extents string
		end
		x_texte = x-info.width/2
		y_texte = y+info.height/2
		@cr.set_source_rgb color[0], color[1], color[2]
		@cr.move_to x_texte, y_texte
		@cr.show_text string
	end
	
	def calcule_size size
		calcul = (size*25/10)-(size*@nb_cases_x/10)
		return calcul
	end
	
	def fleche produit
		marge = 5
		w, h = largeur_hauteur_case
		hauteur_case = h
		x, y = coordonnees produit.cellule
		if x and y
			x += 5
			w -= 2*marge
			h *= produit.taille
			if produit.quantite>0
				@cr.set_source_rgb 0.5, 1, 1 #cyan
			else
				@cr.set_source_rgb 0.8, 0.8, 0.8 #gris
			end
			h_rect = h-hauteur_case/3
			@cr.move_to x, y
			@cr.line_to x+w, y
			@cr.line_to x+w, y+h_rect
			@cr.line_to x+w/2, y+h
			@cr.line_to x, y+h_rect
			@cr.line_to x, y
			@cr.fill
			
			texte produit.nom, x+w/2, y+h_rect/2-calcule_size(14)/2, w
			texte "#{produit.quantite.to_i}#{produit.unite}", x+w/2, y+h_rect/2+calcule_size(14)/2, w, 12
		end
	end
	
	def machine_on?(machine)
		on = true
		xs = (machine.cellule[0]..machine.cellule[0]+machine.taille-1).to_a
		# Vérification des produits en entrée
		coords = []
		xs.each do |x|
			coords << [x,machine.cellule[1]-1] if machine.cellule[1]>0
		end
		coords.each do |coord|
			p = sur_produit?(coord)
			if p
				if (p.quantite==0 and p.obligatoire)
					on = false
				end
			end
		end
		# Vérification des produits en sortie
		coords = []
		xs.each do |x|
			coords << [x,machine.cellule[1]+1]
		end
		coords.each do |coord|
			p = sur_produit?(coord)
			if p
				if p.quantite>0
					on = false
				end
			end
		end
		return on
	end
	
	def sur_machine?(cellule)
		m = nil
		if cellule
			@machines.each do |machine|
				m = machine if machine.sur_cellule?(cellule)
			end
		end
		return m
	end
	
	def sur_produit?(cellule)
		p = nil
		if cellule
			@produits.each do |produit|
				p = produit if produit.sur_cellule?(cellule)
			end
		end
		return p
	end
	
end

class MachineRecette
	
	attr_accessor :cellule, :nom, :taille, :temps
	
	def initialize prop
		@nom = prop[:nom]
		@cellule = prop[:cellule]
		@taille = prop[:taille]
		@temps = 0
	end
	
	def sur_cellule?(cellule)
		x = (@cellule[0]..@cellule[0]+@taille-1).to_a.include?(cellule[0])
		y = cellule[1] == @cellule[1]
		return (x and y)
	end
	
	def to_s
		return "Machine #{@nom} sur cellule #{@cellule} de taille #{@taille}"
	end
	
end

class ProduitRecette
	
	attr_accessor :cellule, :nom, :quantite, :unite, :taille, :obligatoire
	
	def initialize prop
		@nom = prop[:nom]
		@cellule = prop[:cellule]
		@quantite = prop[:quantite]
		@unite = prop[:unite]
		@taille = prop[:taille]
		@obligatoire = (prop[:obligatoire].nil? ? true : prop[:obligatoire])
	end
	
	def sur_cellule?(cellule)
		x = cellule[0] == @cellule[0]
		y = (@cellule[1]..@cellule[1]+@taille-1).to_a.include?(cellule[1])
		return (x and y)
	end
	
	def to_s
		return "Produit #{@nom} en quantité #{@quantite}#{@unite} de taille #{@taille}; obligatoire => #{@obligatoire}"
	end
	
	def cellule_dessus
		return [cellule[0], cellule[1]-1]
	end
	
	def cellule_dessous
		return [cellule[0], cellule[1]+@taille]
	end

end

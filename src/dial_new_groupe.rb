# coding: utf-8

class DialGroupe < Gtk::Dialog

	attr_reader :designation, :parent, :couleur
	
	def initialize window, id=nil
		
		titre = (id.nil? ? "Ajouter un nouveau groupe" : "Modifier un groupe")
		
		super(:title => titre, :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])
		
		table = Gtk::Table.new 3, 2, false
		
		label_designation = Gtk::Alignment.new 0, 0.5, 0, 0
		label_designation.add Gtk::Label.new("Désignation:")
		table.attach( label_designation, 0, 1, 0, 1, :fill, :fill, 2, 2 )
		@designation = Gtk::Entry.new 
		table.attach( @designation, 1, 2, 0, 1, :fill, :fill, 2, 2 )
		
		label_parent = Gtk::Alignment.new 0, 0.5, 0, 0
		label_parent.add Gtk::Label.new("Groupe parent:")
		table.attach( label_parent, 0, 1, 1, 2, :fill, :fill, 2, 2 )
		
		parent_model = Gtk::TreeStore.new(Integer, String)
		@parent = Gtk::ComboBox.new :model => parent_model
		renderer_left = Gtk::CellRendererText.new
		renderer_left.xalign = 0
		@parent.pack_start renderer_left, true
		@parent.add_attribute renderer_left, "text", 1
		table.attach( @parent, 1, 2, 1, 2, :fill, :fill, 2, 2 )
		
		label_couleur = Gtk::Alignment.new 0, 0.5, 0, 0
		label_couleur.add Gtk::Label.new("Couleur pour les graphiques:")
		table.attach( label_couleur, 0, 1, 2, 3, :fill, :fill, 2, 2 )
		@couleur = Gtk::Entry.new
		@couleur.sensitive = false
		table.attach( @couleur, 1, 2, 2, 3, :fill, :fill, 2, 2 )
		@couleur_bt = Gtk::Button.new
		@couleur_bt.add Gtk::Image.new(:file => "resources/icons/color.png")
		table.attach( @couleur_bt, 2, 3, 2, 3, :fill, :fill, 2, 2 )
		
		@couleur_bt.signal_connect("clicked") {
			change_couleur
		}
		
		parent_id=0
		unless id.nil?
			g = Articlegroupe.find(id)
			@designation.text = g.designation
			parent_id = g.parent_id
			@couleur.text = g.couleur.to_s
			g_enfants = Articlegroupe.where(:parent_id => id)
			@parent.sensitive = g_enfants.count.eql?(0)
		end
		
		iter = @parent.model.append nil
		iter[0] = 0
		iter[1] = "Aucun"
		index_iter = iter
		groupes = Articlegroupe.where(:parent_id => 0).order(:designation)
		groupes.each { |g|
			unless g.id.eql?(id)
				iter = @parent.model.append nil
				index_iter = iter if g.id.eql?(parent_id)
				iter[0] = g.id
				iter[1] = g.designation	
			end		
		}
		
		@parent.active_iter = index_iter
		
		self.child.pack_start table, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all
	
	end
	
	def change_couleur
		
		dial = Gtk::ColorSelectionDialog.new
		
		response = dial.run
		case response
		  when Gtk::ResponseType::OK
				couleurs = dial.colorsel.current_color.to_a
				@couleur.text = "#{couleurs[0].to_f/65535},#{couleurs[1].to_f/65535},#{couleurs[2].to_f/65535}"
		end 
		dial.destroy
		
	end

end		

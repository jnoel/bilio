# coding: utf-8

class Utilisateur_box< Gtk::Box

	def initialize window
	
		super(:vertical, 3)
		
		set_border_width 10
		
		@window = window
		@login = window.login
		
		@id_user = -1
		
		@firstname = Gtk::Entry.new
		@lastname = Gtk::Entry.new
		@username = Gtk::Entry.new
		@password = Gtk::Entry.new
		@password.visibility = false
		@admin = Gtk::CheckButton.new
		@active = Gtk::CheckButton.new
		@active.active = true
		
		@valider = Gtk::Button.new
		hboxvalider = Gtk::Box.new :horizontal, 2
		hboxvalider.add Gtk::Image.new( :file => "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new "Valider"
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::Box.new :horizontal, 2
		hboxannuler.add Gtk::Image.new( :file => "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new "Annuler"
		@annuler.add hboxannuler
		
		agencement
		
		remplir_tableaux
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { cancel }
	
	end
	
	def agencement
	
		hbox2 = Gtk::Box.new :horizontal, 2
		
		vbox2 = Gtk::Box.new :vertical, 2
		align3 = Gtk::Alignment.new 1, 0, 1, 0
		align3.add vbox2
		vbox2.add Gtk::Label.new( "Prénom :" )
		vbox2.add @firstname
		hbox2.pack_start( align3, :expand => true, :fill => true, :padding => 3  )

		vbox2 = Gtk::Box.new :vertical, 2
		align3 = Gtk::Alignment.new 1, 0, 1, 0
		align3.add vbox2
		vbox2.add Gtk::Label.new( "Nom :" )
		vbox2.add @lastname
		hbox2.pack_start( align3, :expand => true, :fill => true, :padding => 3  )
		
		hbox3 = Gtk::Box.new :horizontal, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, :expand => true, :fill => true, :padding => 3  )
		hbox3.pack_start( @valider, :expand => true, :fill => true, :padding => 3  )
		align1.add hbox3
		
		vboxframe = Gtk::Box.new :vertical, 2
		vboxframe.pack_start hbox2, :expand => false, :fill => false, :padding => 3 
		
		hboxusername = Gtk::Box.new :horizontal, 2
		hboxusername.pack_start( Gtk::Label.new( "Nom d'utilisateur :" ), :expand => false, :fill => false, :padding => 3  )
		hboxusername.pack_start( @username, :expand => false, :fill => false, :padding => 3  )
		
		vboxframe.pack_start hboxusername, :expand => false, :fill => false, :padding => 3 
		
		hboxmdp = Gtk::Box.new :horizontal, 2
		hboxmdp.pack_start( Gtk::Label.new( "Mot de passe :" ), :expand => false, :fill => false, :padding => 3  )
		hboxmdp.pack_start( @password, :expand => false, :fill => false, :padding => 3  )
		
		vboxframe.pack_start hboxmdp, :expand => false, :fill => false, :padding => 3 
		
		hboxadmin = Gtk::Box.new :horizontal, 2
		hboxadmin.pack_start( Gtk::Label.new( "Administrateur ? " ), :expand => false, :fill => false, :padding => 3  )
		hboxadmin.pack_start( @admin, :expand => false, :fill => false, :padding => 3  )
		
		vboxframe.pack_start hboxadmin, :expand => false, :fill => false, :padding => 3 
		
		hboxactif = Gtk::Box.new :horizontal, 2
		hboxactif.pack_start( Gtk::Label.new( "Actif ? " ), :expand => false, :fill => false, :padding => 3 )
		hboxactif.pack_start( @active, :expand => false, :fill => false, :padding => 3 )
		
		vboxframe.pack_start hboxactif, :expand => false, :fill => false, :padding => 3
		vboxframe.pack_start tableaux_droits, :expand => true, :fill => true, :padding => 3
		
		@frame = Gtk::Frame.new "Nouvel utilisateur"
		@frame.add vboxframe
		
		pack_start @frame, :expand => true, :fill => true, :padding => 3 
		pack_start align1, :expand => false, :fill => false, :padding => 3 
	
	end
	
	def tableaux_droits
		hbox = Gtk::Box.new :horizontal, 5
		hbox.pack_start tableau_droits_dispo, :expand => true, :fill => true, :padding => 5
		vbox = Gtk::Box.new :vertical, 2
		button_add = Gtk::Button.new :label => ">>"
		button_remove = Gtk::Button.new :label => "<<"
		vbox.pack_start button_add, :expand => false, :fill => false, :padding => 3
		vbox.pack_start button_remove, :expand => false, :fill => false, :padding => 3
		align = Gtk::Alignment.new 1, 0.5, 1, 0.5
		align.add vbox
		hbox.pack_start align, :expand => false, :fill => false, :padding => 3
		hbox.pack_start tableau_droits_utilisateur, :expand => true, :fill => true, :padding => 5
		
		button_add.signal_connect( "clicked" ) { droit_add }
		button_remove.signal_connect( "clicked" ) { droit_remove }
		
		return hbox
	end
	
	def tableau_droits_dispo
		@tableau_droits_dispo = Gtk::TreeView.new(Gtk::ListStore.new(String))
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.yalign = 0.5
		renderer_left.xalign = 0
		
		col = Gtk::TreeViewColumn.new("Droits disponibles", renderer_left, :text => 0)
		@tableau_droits_dispo.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:never, :automatic)
  	scroll.add @tableau_droits_dispo
		
		return scroll
	end
	
	def tableau_droits_utilisateur
		@tableau_droits_utilisateur = Gtk::TreeView.new(Gtk::ListStore.new(String))
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.yalign = 0.5
		renderer_left.xalign = 0
		
		col = Gtk::TreeViewColumn.new("Droits de l'utilisateur", renderer_left, :text => 0)
		@tableau_droits_utilisateur.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
  	scroll.set_policy(:never, :automatic)
  	scroll.add @tableau_droits_utilisateur
		
		return scroll
	end
	
	def validate
		
		if ( !@firstname.text.empty? and !@lastname.text.empty? and !@username.text.empty? and ( !@password.text.empty? or @id_user>0 ) ) then
			
			@utilisateur.id = @id_user if @id_user>0
			@utilisateur.firstname = @firstname.text
			@utilisateur.lastname = @lastname.text
			@utilisateur.username = @username.text
			if (@id_user>0 and @password.text.empty?)
				# on ne touche pas au mot de passe déjà enregistré
			else
				@utilisateur.password = BCrypt::Password.create(@password.text)
			end
			@utilisateur.admin = @admin.active?
			@utilisateur.active = @active.active?
			@utilisateur.droits = get_droits
			
			res = @utilisateur.save
			
			quit 1 if res
			
		else
			erreur = ""
			erreur += "Le prénom n'est pas renseigné.\n" if @firstname.text.empty?
			erreur += "Le nom n'est pas renseigné.\n" if @lastname.text.empty?
			erreur += "Le nom d'utilisateur n'est pas renseigné.\n" if @username.text.empty?
			erreur += "Le mot de passe n'est pas renseigné.\n" if ( @password.text.empty? and @id_user<0 )
			dialog = Gtk::MessageDialog.new @window, Gtk::Dialog::MODAL, Gtk::MessageDialog::ERROR, Gtk::MessageDialog::BUTTONS_CLOSE, erreur
			dialog.title = "Erreur"
			dialog.signal_connect('response') { dialog.destroy }
    		dialog.run
		end
	
	end
	
	def cancel
	
		quit
	
	end
	
	def quit statut=nil
	
		@id_user = -1
		@firstname.text = ""
		@lastname.text = ""
		@username.text = ""
		@admin.active = false
		@active.active = true
		
		@window.liste_users.remplir_users unless statut.nil?
		@window.affiche @window.liste_users
	
	end
	
	def change id=0
		@id_user = id
		@utilisateur = nil
		if @id_user>0 then
			
			@utilisateur = Utilisateur.find(@id_user)		
			if @utilisateur then
				@firstname.text = @utilisateur.firstname
				@lastname.text = @utilisateur.lastname
				@username.text = @utilisateur.username
				@admin.active = @utilisateur.admin
				@active.active = @utilisateur.active
				remplir_tableaux @utilisateur.droits.to_s.split(',')
			else
				@window.message_erreur utilisateur.error
			end
		else
			@utilisateur = Utilisateur.new
			remplir_tableaux
		end
		
		@frame.label = (@id_user>0 ? "Modifier utilisateur" : "Nouvel utilisateur")
	end
	
	def remplir_tableaux droits=[]
		@droits_dispo = ["Tableau de bord", "Articles", "Production", "Tiers", "Interventions", "Achats", "Ventes", "Financier"]
		@droits_dispo -= droits
		@droits_utilisateur = droits
		@tableau_droits_dispo.model.clear
		remplir_tableau(@tableau_droits_dispo, @droits_dispo)
		@tableau_droits_utilisateur.model.clear
		remplir_tableau(@tableau_droits_utilisateur, @droits_utilisateur)
	end
	
	def remplir_tableau tableau, liste
		liste.each do |d|
			iter = tableau.model.append
			iter[0] = d
		end
	end
	 	
	def droit_add
		droits_sel = []
		@tableau_droits_dispo.selection.selected_each { |model, path, iter|
			droits_sel << @tableau_droits_dispo.model.get_value(iter, 0)
			@tableau_droits_dispo.model.remove(iter)
		}
		
		remplir_tableau(@tableau_droits_utilisateur, droits_sel)
	end
	
	def droit_remove
		droits_sel = []
		@tableau_droits_utilisateur.selection.selected_each { |model, path, iter|
			droits_sel << @tableau_droits_utilisateur.model.get_value(iter, 0)
			@tableau_droits_utilisateur.model.remove(iter)
		}
		
		remplir_tableau(@tableau_droits_dispo, droits_sel)
	end
	
	def get_droits
		droits = []
		@tableau_droits_utilisateur.model.each { |model, path, iter|
			droits << @tableau_droits_utilisateur.model.get_value(iter, 0)
		}
		
		return droits.join(",")
	end
	
end

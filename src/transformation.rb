# coding: utf-8

class Transformation_box < Gtk::VBox

	def initialize window
	
		super(false, 3)
		
		set_border_width 10
				
		@window = window
		@login = window.login
		
		@valider = Gtk::Button.new
		hboxvalider = Gtk::HBox.new false, 2
		hboxvalider.add Gtk::Image.new( "./resources/icons/ok.png" )
		hboxvalider.add Gtk::Label.new "Valider"
		@valider.add hboxvalider
		@annuler = Gtk::Button.new
		hboxannuler = Gtk::HBox.new false, 2
		hboxannuler.add Gtk::Image.new( "./resources/icons/cancel.png" )
		hboxannuler.add Gtk::Label.new "Annuler"
		@annuler.add hboxannuler
		
		agencement
		
		@valider.signal_connect( "clicked" ) { validate }
		@annuler.signal_connect( "clicked" ) { quit }
	
	end
	
	def agencement
	
		vbox = Gtk::VBox.new false, 3
		@frame = Gtk::Frame.new
		@frame.label = "Transformation de produits - Module expérimental"
		vbox.pack_start( @frame, true, true, 3 )
		
		vbox_corps = Gtk::VBox.new false, 3
		@frame.add vbox_corps
		vbox_corps.pack_start( en_tete, false, false, 3 )		
		vbox_corps.pack_start( corps, true, true, 3 )		
		
		hbox3 = Gtk::HBox.new false, 2
		align1 = Gtk::Alignment.new 1, 1, 0, 0
		hbox3.pack_start( @annuler, true, true, 3 )
		hbox3.pack_start( @valider, true, true, 3 )
		align1.add hbox3
		
		vbox.pack_start( align1, false, false, 3 )
		
		self.add vbox
	
	end
	
	def en_tete
	
		box = Gtk::VBox.new false, 3
		
		box.add Gtk::Image.new "./resources/images/transformation.png"
		
		hbox1 = Gtk::HBox.new false, 3
		hbox1.pack_start( Gtk::Label.new("Produit à réaliser"), false, false, 3 )
		@produit = Gtk::ComboBox.new 
		
		@produit.signal_connect( "changed" ) { changement_transfo @produit.active }
		
		remplir_produit
		hbox1.pack_start( @produit, false, true, 3 )
		
		box.add hbox1
		#box.add corps
		box
	
	end
	
	def remplir_produit
		
		@produit.model.clear
		
		@produit.append_text "Confiture d'Ananas"
		@produit.append_text "Confiture de Mangue"
		@produit.append_text "Jus de Goyavier"
		@produit.append_text "Jus de Tangor"
		
	end
	
	def corps
		
		table = Gtk::Table.new 2, 3, false
		
		table.attach( Gtk::Label.new("Matières premières :"), 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( Gtk::Label.new("Produits finits :"), 2, 3, 0, 1, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( Gtk::Image.new("./resources/images/fleche.png"), 1, 2, 0, 3, Gtk::FILL, Gtk::FILL, 5, 5 )
		table.attach( table_mp, 0, 1, 1, 2, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL, 5, 5 )
		table.attach( table_pf, 2, 3, 1, 2, Gtk::EXPAND|Gtk::FILL, Gtk::EXPAND|Gtk::FILL, 5, 5 )
		
		table
	
	end
	
	def table_mp
		# list_store (code, article, qtite)
		@list_mp = Gtk::TreeStore.new(String, String, Float)
		@view_mp = Gtk::TreeView.new(@list_mp)
		@view_mp.signal_connect ("row-activated") { |view, path, column|
			#
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Code", renderer_left, :text => 0)
		col.sort_column_id = 0
		col.resizable = true
		@view_mp.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Article", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		col.expand = true
		@view_mp.append_column(col)
		
		# Colonne pour la qtite :
		renderer_qtite = Gtk::CellRendererSpin.new
		renderer_qtite.adjustment = Gtk::Adjustment.new(0, 0, 999999, 0.1, 10, 0)
		renderer_qtite.digits = 3
		renderer_qtite.xalign = 1
		renderer_qtite.mode = Gtk::CellRendererText::MODE_EDITABLE
		renderer_qtite.editable = true
		renderer_qtite.signal_connect('edited') { |spin, data, text|
			@view_mp.model.get_iter(data)[2] = text.to_f
		} 
		col = Gtk::TreeViewColumn.new("Quantité utilisée", renderer_qtite, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		@view_mp.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
	  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
	  	scroll.add @view_mp
	  	
	  	scroll
	end
	
	def table_pf
		# list_store (code, article, qtite)
		@list_pf = Gtk::TreeStore.new(String, String, Float)
		@view_pf = Gtk::TreeView.new(@list_pf)
		@view_pf.signal_connect ("row-activated") { |view, path, column|
			#
		}
		
		# Create a renderer with the background property set
		renderer_left = Gtk::CellRendererText.new
		renderer_left.background = "white"
		renderer_left.xalign = 0
		renderer_left.yalign = 0
		
		# Create a renderer with the background property set
		renderer_right = Gtk::CellRendererText.new
		renderer_right.xalign = 1
		renderer_right.yalign = 0
		
		col = Gtk::TreeViewColumn.new("Code", renderer_left, :text => 0)
		col.sort_column_id = 0
		col.resizable = true
		@view_pf.append_column(col)
		
		col = Gtk::TreeViewColumn.new("Article", renderer_left, :text => 1)
		col.sort_column_id = 1
		col.resizable = true
		col.expand = true
		@view_pf.append_column(col)
		
		# Colonne pour la qtite :
		renderer_qtite = Gtk::CellRendererSpin.new
		renderer_qtite.adjustment = Gtk::Adjustment.new(0, 0, 999999, 0.1, 10, 0)
		renderer_qtite.digits = 3
		renderer_qtite.xalign = 1
		renderer_qtite.mode = Gtk::CellRendererText::MODE_EDITABLE
		renderer_qtite.editable = true
		renderer_qtite.signal_connect('edited') { |spin, data, text|
			@view_pf.model.get_iter(data)[2] = text.to_f
		} 
		col = Gtk::TreeViewColumn.new("Quantité produite", renderer_qtite, :text => 2)
		col.sort_column_id = 2
		col.resizable = true
		@view_pf.append_column(col)
		
		scroll = Gtk::ScrolledWindow.new
	  	scroll.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_AUTOMATIC)
	  	scroll.add @view_pf
	  	
	  	scroll
	end
	
	def changement_transfo index
		
		@list_mp.clear
		@list_pf.clear
		
		case index
			when 0
				iter = @list_mp.append nil
				iter[0] = 'ANABRUT'
				iter[1] = 'Ananas entier'
				iter = @list_mp.append nil
				iter[0] = 'SUCRE112'
				iter[1] = 'Sucre de canne'
				iter = @list_mp.append nil
				iter[0] = 'JUSCI'
				iter[1] = 'Jus de citron'
				
				iter = @list_pf.append nil
				iter[0] = 'CONF_ANANAS100'
				iter[1] = "Pot de 100g de confiture d'ananas"
				iter = @list_pf.append nil
				iter[0] = 'CONF_ANANAS150'
				iter[1] = "Pot de 150g de confiture d'ananas"
				
			when 1
				iter = @list_mp.append nil
				iter[0] = 'MANGBRUT'
				iter[1] = 'Mangue entière'
				iter = @list_mp.append nil
				iter[0] = 'SUCRE112'
				iter[1] = 'Sucre de canne'
				iter = @list_mp.append nil
				iter[0] = 'JUSCI'
				iter[1] = 'Jus de citron'
				
				iter = @list_pf.append nil
				iter[0] = 'CONF_MANG100'
				iter[1] = "Pot de 100g de confiture de mangue"
				iter = @list_pf.append nil
				iter[0] = 'CONF_MANG150'
				iter[1] = "Pot de 150g de confiture de mangue"
			
			when 2
				iter = @list_mp.append nil
				iter[0] = 'GOYAVIER'
				iter[1] = 'Goyaviers bruts'
				
				iter = @list_pf.append nil
				iter[0] = 'JUS_GOYA'
				iter[1] = "Jus de goyavier (en L)"
				iter = @list_pf.append nil
				iter[0] = 'PULP_GOYA'
				iter[1] = "Pulpe de goyavier (en g)"
			
			when 3
				iter = @list_mp.append nil
				iter[0] = 'TANGOR'
				iter[1] = 'Tangors brutes'
				
				iter = @list_pf.append nil
				iter[0] = 'JUS_TANG'
				iter[1] = "Jus de tangors (en L)"
				iter = @list_pf.append nil
				iter[0] = 'PULP_TANG'
				iter[1] = "Pulpe de tangors (en g)"
			
		end
		
	end
	
	def validate
		
	
	end
	
	def focus
		@annuler.grab_focus
	end
	
	def quit statut=nil
		
		@window.affiche @window.liste_articles
		@window.liste_articles.focus
	
	end
	
end

# coding: utf-8

class EditionResultat < Prawn::Document
	
	def initialize graph, tableau, tmp, window
	
		super(	:page_layout => :landscape,
						:right_margin => 15,
						:page_size => 'A4'
					)
		
		bounding_box([bounds.left, bounds.top], :width => bounds.right) do
			societe = Societe.first
			logo = window.config_db.conf["chemin_documents"]+"/societe/"+societe.logo
			image logo, :height => 70 if File.exist?(logo)
			move_down 20
			table(tableau, :cell_style => { :border_width => 0,
																			:padding => 1,
																			:size => 10 }) do |table|
	  			table.column(0).align = :left
	  			table.column(1..12).align = :right
	  			table.column(1).background_color = table.column(4).background_color = table.column(7).background_color = table.column(10).background_color = "ffe7e7"
	  			table.column(2).background_color = table.column(5).background_color = table.column(8).background_color = table.column(11).background_color = "e7ffe7"
	  			table.column(3).background_color = table.column(6).background_color = table.column(9).background_color = table.column(12).background_color = "eeeeee"
	  			table.row(0).align = :center
			end
		end
		
		bounding_box [bounds.left, cursor], :width  => bounds.right do		
			image graph, :width => bounds.right if File.exist?(graph)
		end
		
		rendu tmp
	
	end
	
	def rendu chemin_temp
	
		render_file "#{chemin_temp}"
	
	end
	
end

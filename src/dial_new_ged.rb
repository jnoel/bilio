# coding: utf-8

class DialNewGed < Gtk::Dialog

	attr_reader :nom
	
	def initialize window
		
		super(:title => "Nouveau document", :parent => window, :flags => :destroy_with_parent, :buttons => [[ Gtk::Stock::CANCEL, :cancel ], [ Gtk::Stock::OK, :ok ]])
		
		#set_default_size 900, 300
		
		table = Gtk::Table.new 3, 2, false
		
		label_nom = Gtk::Alignment.new 0, 0.5, 0, 0
		label_nom.add Gtk::Label.new("Nom et extension du fichier:")
		table.attach( label_nom, 0, 1, 0, 1, :fill, :fill, 2, 2 )
		@nom = Gtk::Entry.new 
		table.attach( @nom, 1, 2, 0, 1, :fill, :fill, 2, 2 )
		
		@nom.signal_connect( "activate" ) {
			response(Gtk::ResponseType::OK)
		}
		
		self.child.pack_start table, :expand => true, :fill => true, :padding => 3
		
		self.child.show_all
	
	end

end		

#!/usr/bin/env ruby
# coding: utf-8

f = File.new('bilio.sh', 'w')
f.write("#!/bin/bash\n")
f.write("cd #{File.join(Dir.getwd,'src')}\n")
f.write("ruby bilio.rbw\n")
f.chmod(0770)
f.close

f = File.new(File.join(Dir.home,'.local/share/applications/bilio.desktop'), 'w')
f.write("#!/usr/bin/env xdg-open\n\n")
f.write("[Desktop Entry]\n")
f.write("Encoding=UTF-8\n")
f.write("Name=Bilio\n")
f.write("Name[fr]=Bilio\n")
f.write("Name[fr_FR]=Bilio\n")
f.write("Exec=#{File.join(Dir.getwd,'bilio.sh')}\n")
f.write("Icon=#{File.join(Dir.getwd,'src/resources/icons/icon.png')}\n")
f.write("Terminal=false\n")
f.write("Type=Application\n")
f.write("StartupNotify=true\n")
f.write("Categories=Office\n")
f.chmod(0770)
f.close

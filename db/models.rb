# coding: utf-8

class Tiers < ActiveRecord::Base
	belongs_to :typetiers
	belongs_to :tarif
	has_many :documents
	has_many :paiements
	has_many :gedtiers
end

class Typetiers < ActiveRecord::Base
	has_many :tiers
end

class Document < ActiveRecord::Base
	belongs_to :tiers
	belongs_to :documenttype
	belongs_to :utilisateur
	belongs_to :conditionpaiement
	has_many :documentlignes
	has_many :paiementdocuments
	has_many :geddocuments
end

class Documenttype < ActiveRecord::Base
	has_many :documents
end

class Documentligne < ActiveRecord::Base
	belongs_to :document
	belongs_to :article
	belongs_to :tva
end

class Article < ActiveRecord::Base
	belongs_to :articlegroupe
	belongs_to :tva
	belongs_to :rayon
	has_many :documentlignes
	has_many :articletarifs
	has_many :articlestocks
	has_many :gedarticles
end

class Articlegroupe < ActiveRecord::Base
	has_many :articles
end

class Articletarif < ActiveRecord::Base
	belongs_to :tarif
	belongs_to :article
end

class Tarif < ActiveRecord::Base
	has_many :tiers
	has_many :articletarifs
	
	def create_articletarif
		tarifrevient = Tarif.where(:cr => true).first
		articles = Article.all
		articles.each do |a|
			cr = Articletarif.where(:tarif_id => tarifrevient.id, :article_id => a.id).first
			if cr
				pu = (cr.pu * (1+self.marge/100)).round
			else
				pu = 0.0
			end 
			Articletarif.create :tarif_id => self.id, :article_id => a.id, :marge => self.marge, :pu => pu
		end
	end
	
	def maj designation, marge
		self.designation = designation
		self.marge = marge
		self.save
	end
	
	def supprimer
		Articletarif.where(:tarif_id => self.id).destroy_all
		tarif = Tarif.where(:vente => true).order(:id).first
		Tiers.where(:tarif_id => self.id).update_all(:tarif_id => tarif.id)
		self.destroy
	end
	
end

class Articlestock < ActiveRecord::Base
	belongs_to :stock
	belongs_to :article
end

class Stock < ActiveRecord::Base
	has_many :articlestocks
end

class Tva < ActiveRecord::Base
	has_many :articles
	has_many :documentligne
	
	def maj designation, taux, np
		self.designation = designation
		self.taux = taux
		self.np = np
		self.save
	end
end

class Rayon < ActiveRecord::Base
	has_many :articles
end

class Utilisateur < ActiveRecord::Base
	has_many :documents
end

class Societe < ActiveRecord::Base

end

class Paiement < ActiveRecord::Base
	belongs_to :tiers
	belongs_to :paiementtype
	belongs_to :compte
	has_many :paiementdocuments
end

class Paiementtype < ActiveRecord::Base
	has_many :paiements
end

class Paiementdocument < ActiveRecord::Base
	belongs_to :paiement
	belongs_to :document
end

class Compte < ActiveRecord::Base
	has_many :paiements
	has_many :ged_comptes
end

class Gedtiers < ActiveRecord::Base
	belongs_to :tiers
end

class Geddocument < ActiveRecord::Base
	belongs_to :document
end

class Gedarticle < ActiveRecord::Base
	belongs_to :article
end

class Gedcompte < ActiveRecord::Base
	belongs_to :compte
end

class Conditionpaiement < ActiveRecord::Base
	has_many :documents
end

class Intervention < ActiveRecord::Base
	belongs_to :tiers
	belongs_to :utilisateur
	has_many :interventionlignes
end

class Interventionligne < ActiveRecord::Base
	belongs_to :intervention
end

class Option < ActiveRecord::Base

end

class Schema_migration < ActiveRecord::Base
	
end

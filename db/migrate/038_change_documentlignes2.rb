# coding: utf-8

class ChangeDocumentlignes2 < ActiveRecord::Migration
  def self.up
  	change_table :documentlignes do |t|
  		t.boolean :suppr, :default => false
  	end
  end

  def self.down
		change_table :documentlignes do |t|
			t.remove :suppr
		end
  end
end

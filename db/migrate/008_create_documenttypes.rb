# coding: utf-8

class CreateDocumenttypes < ActiveRecord::Migration
  def self.up
    
    create_table :documenttypes do |t|
      t.string :designation, :null => false
      t.boolean :vente, :null => false
      t.string :code
      t.integer :dernier_num, :default => 0
      t.timestamps
    end
    
    Documenttype.create :designation => "Devis Client", :vente => true, :code => "DC"
    Documenttype.create :designation => "Commande Client", :vente => true, :code => "CC"
    Documenttype.create :designation => "BL Client", :vente => true, :code => "BC"
    Documenttype.create :designation => "Facture Client", :vente => true, :code => "FC"
    Documenttype.create :designation => "Avoir Client", :vente => true, :code => "AC"
    Documenttype.create :designation => "Devis Fournisseur", :vente => false, :code => "DF"
    Documenttype.create :designation => "Commande Fournisseur", :vente => false, :code => "CF"
    Documenttype.create :designation => "Facture Fournisseur", :vente => false, :code => "FF"
    
  end

  def self.down
    drop_table :documenttypes
  end
end


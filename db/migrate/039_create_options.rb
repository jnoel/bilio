# coding: utf-8

class CreateOptions < ActiveRecord::Migration
  
  def self.up
   
    create_table :options do |t|
      t.string :cle, :uniq => true
      t.string :valeur
      t.text :description
      t.timestamps
    end

    Option.create :cle => "stock_negatif", :valeur => "ask", :description => "Autorise-t-on les stocks négatifs ?\nOptions possibles : yes, no, ask\nOption par défaut : ask"
      
  end

  def self.down
    drop_table :options
  end
  
end


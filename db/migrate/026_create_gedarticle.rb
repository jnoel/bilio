# coding: utf-8

class CreateGedarticle < ActiveRecord::Migration
  def self.up
    
    create_table :gedarticles do |t| 
	  t.integer :article_id
	  t.string :fichier
	  t.text :note
      t.timestamps
    end
        
  end

  def self.down
	 drop_table :gedarticles
  end
end


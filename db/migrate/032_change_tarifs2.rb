# coding: utf-8

class ChangeTarifs2 < ActiveRecord::Migration
  Tarif.all.each { |tarif|
  	if tarif.marge.nil?
  		tarif.marge = 1.0 
  		tarif.save
  	end
  }
end

# coding: utf-8

class ChangeSocietes2 < ActiveRecord::Migration
  def self.up
   
    change_table :societes do |t|
	  t.string :logo
	end
    
  end

  def self.down
  	 change_table :societes do |t|
      t.remove :logo
    end
  end
end


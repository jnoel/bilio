# coding: utf-8

class ChangeDocuments4 < ActiveRecord::Migration
  def self.up
  	change_column :documents, :ref, :string, {:null => false, :unique => true}
  end

  def self.down
	change_column :documents, :ref, :string, {:null => false, :unique => false}
  end
end

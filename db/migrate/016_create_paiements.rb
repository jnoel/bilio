# coding: utf-8

class CreatePaiements < ActiveRecord::Migration
  def self.up
    
    create_table :paiements do |t| 
      t.integer :paiementtype_id
      t.integer :tiers_id
      t.integer :compte_id
      t.float :montant
      t.string :numero
      t.string :emetteur
      t.string :banque
      t.text :note    
      t.date :date_paiement
      t.date :date_comptable
      t.boolean :fournisseur, :default => false
      t.boolean :suppr, :default => false
      t.timestamps
    end
    
    create_table :paiementtypes do |t| 
	  t.string :designation
      t.boolean :suppr, :default => false
      t.timestamps
    end
    
    create_table :paiementdocuments do |t| 
	  t.integer :paiement_id
	  t.integer :document_id
	  t.float :montant
      t.boolean :suppr, :default => false
      t.timestamps
    end
    
    Paiementtype.create :designation => "Espèces"
    Paiementtype.create :designation => "Carte bancaire"
    Paiementtype.create :designation => "Chèque"
	Paiementtype.create :designation => "Virement"
    Paiementtype.create :designation => "TIP"
    Paiementtype.create :designation => "Prélévement"
    
  end

  def self.down
	 drop_table :paiements
	 drop_table :paiementtypes
	 drop_table :paiementdocuments
  end
end


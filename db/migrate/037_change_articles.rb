# coding: utf-8

class ChangeArticles < ActiveRecord::Migration
  def self.up
  	change_table :articles do |t|
			t.boolean :suppr, :default => false
		end
  end

  def self.down
  	change_table :articles do |t|
			t.remove :suppr
		end
  end
end

# coding: utf-8

class ChangeArticlegroupes < ActiveRecord::Migration
  
  def self.up 
  
		change_table :articlegroupes do |t|
			t.string :couleur
		end
		   
  end

  def self.down
  
  	change_table :articlegroupes do |t|
      t.remove :couleur
    end
    
  end
  
end


# coding: utf-8

class ChangeComptes2 < ActiveRecord::Migration

  def self.up
    
    change_table :comptes do |t| 
			t.boolean :defaut, :default => false
    end
    
  end

  def self.down
  
  	change_table :comptes do |t|
			t.remove :defaut
		end
		
  end
  
end


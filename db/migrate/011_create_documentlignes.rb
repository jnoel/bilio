# coding: utf-8

class CreateDocumentlignes < ActiveRecord::Migration
  def self.up
    
    create_table :documentlignes do |t|
      t.integer :document_id, :null => false
      t.integer :article_id
      t.string :code
      t.text :designation
      t.string :lot
      t.text :commentaires
      t.integer :qtite
      t.integer :tva_id
      t.integer :colisage
      t.float :pu
      t.integer :remise
      t.float :total_ligne
      t.timestamps
    end
    
  end

  def self.down
    drop_table :documentlignes
  end
end


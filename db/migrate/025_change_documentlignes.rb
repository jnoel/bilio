# coding: utf-8

class ChangeDocumentlignes < ActiveRecord::Migration
  def self.up
  	change_table :documentlignes do |t|
	  t.change :qtite, :float
	end
  end

  def self.down
  	change_table :documentlignes do |t|
	  t.change :qtite, :integer
	end
  end
end

# coding: utf-8

class ChangeComptes < ActiveRecord::Migration

  def self.up
    
    change_table :comptes do |t| 
			t.float :solde_ouverture, :default => 0.0
			t.float :solde, :default => 0.0
    end
    
  end

  def self.down
  
  	change_table :comptes do |t|
			t.remove :solde_ouverture
			t.remove :solde
		end
		
  end
  
end


# coding: utf-8

class ChangeTiers < ActiveRecord::Migration
  def self.up
  	change_table :tiers do |t|
			t.string :mail2
		end
  end

  def self.down
  	change_table :tiers do |t|
			t.remove :mail2
		end
  end
end

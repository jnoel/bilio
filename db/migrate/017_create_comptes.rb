# coding: utf-8

class CreateComptes < ActiveRecord::Migration
  def self.up
    
    create_table :comptes do |t| 
	  t.string :designation
	  t.string :banque
	  t.string :code_banque
	  t.string :code_guichet
	  t.string :num_compte
	  t.string :cle_rib
	  t.string :bic
	  t.string :iban
	  t.string :domiciliation
	  t.string :proprietaire
	  t.text :adresse_proprietaire
	  t.string :site
	  t.text :note
      t.boolean :suppr, :default => false
      t.timestamps
    end
    
    Compte.create :designation => "Compte par défaut"
    
  end

  def self.down
	 drop_table :comptes
  end
end


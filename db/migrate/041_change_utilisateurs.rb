# coding: utf-8

class ChangeUtilisateurs < ActiveRecord::Migration
  def self.up
  	change_table :utilisateurs do |t|
			t.string :droits
		end
  end

  def self.down
  	change_table :utilisateurs do |t|
			t.remove :droits
		end
  end
end

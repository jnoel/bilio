# coding: utf-8

class ChangePaiements < ActiveRecord::Migration
  def self.up
    
    change_table :paiements do |t| 
	  t.string :pointage
    end
    
  end

  def self.down
  	change_table :paiements do |t|
		 t.remove :pointage
	end
  end
end


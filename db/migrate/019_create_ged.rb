# coding: utf-8

class CreateGed < ActiveRecord::Migration
  def self.up
    
    create_table :gedtiers do |t| 
	  t.integer :tiers_id
	  t.string :fichier
	  t.text :note
      t.timestamps
    end
    
    create_table :geddocuments do |t| 
	  t.integer :document_id
	  t.string :fichier
	  t.text :note
      t.timestamps
    end
    
  end

  def self.down
	 drop_table :gedtiers
	 drop_table :geddocuments
  end
end


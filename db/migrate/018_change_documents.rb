# coding: utf-8

class ChangeDocuments < ActiveRecord::Migration
  def self.up
  	change_table :documents do |t|
	  	t.float :montant_regle, :default => 0.0
		end
  end

  def self.down
  	change_table :documents do |t|
	  	t.remove :montant_regle
		end
  end
end

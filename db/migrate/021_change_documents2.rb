# coding: utf-8

class ChangeDocuments2 < ActiveRecord::Migration
  def self.up
  	change_table :documents do |t|
	  t.string :ref2
	end
  end

  def self.down
  	change_table :documents do |t|
	  t.remove :ref2
	end
  end
end

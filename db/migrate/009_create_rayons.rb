# coding: utf-8

class CreateRayons < ActiveRecord::Migration
  def self.up
    
    create_table :rayons do |t|
      t.string :nom, :null => false
      t.string :description
      t.timestamps
    end
    
  end

  def self.down
    drop_table :rayons
  end
end


# coding: utf-8

class CreateTarifs < ActiveRecord::Migration
  def self.up
    
    create_table :tarifs do |t|
      t.string :designation, :null => false
      t.boolean :vente, :default => true
      t.boolean :default, :default => false
      t.boolean :pa, :default => false
      t.boolean :cr, :default => false
      t.timestamps
    end
    
    Tarif.create :designation => "Normal", :default => true
    Tarif.create :designation => "Prix d'achat", :vente => false, :pa => true
    Tarif.create :designation => "Coût de revient", :vente => false, :cr => true
    
  end

  def self.down
    drop_table :tarifs
  end
end


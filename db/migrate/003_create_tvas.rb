# coding: utf-8

class CreateTvas < ActiveRecord::Migration
  def self.up
    
    create_table :tvas do |t|
      t.string :designation, :null => false
      t.float :taux, :null => false
      t.boolean :np, :default => false
      t.timestamps
    end
    
    Tva.create :designation => "0 %", :taux => 0
    
  end

  def self.down
    drop_table :tvas
  end
end


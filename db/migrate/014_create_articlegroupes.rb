# coding: utf-8

class CreateArticlegroupes < ActiveRecord::Migration
  def self.up
    
    create_table :articlegroupes do |t|
      t.string :designation, :null => false
      t.integer :parent_id, :default => 0
      t.timestamps
    end
    
  end

  def self.down
    drop_table :articlegroupes
  end
end


# coding: utf-8

class ChangeSocietes < ActiveRecord::Migration
  def self.up
   
    change_table :societes do |t|
	  t.remove :banque
	  t.remove :banque_compte
	  t.remove :banque_code
	  t.remove :banque_guichet
	  t.remove :banque_cle
	  t.remove :banque_domiciliation
	  t.remove :banque_iban
	  t.remove :banque_bic
	end
    
  end

  def self.down
  	 change_table :societes do |t|
      t.string :banque
      t.string :banque_compte
      t.string :banque_code
      t.string :banque_guichet
      t.string :banque_cle
      t.string :banque_domiciliation
      t.string :banque_iban
      t.string :banque_bic
    end
  end
end


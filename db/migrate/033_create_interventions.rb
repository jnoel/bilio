# coding: utf-8

class CreateInterventions < ActiveRecord::Migration
  def self.up
    
    create_table :interventions do |t|
      t.integer :tiers_id, :null => false
      t.date :date
      t.float :duree
      t.integer :utilisateur_id, :null => false
      t.string :signature
      t.boolean :suppr, :default => false
      t.timestamps
    end
    
    create_table :interventionlignes do |t|
      t.integer :intervention_id, :null => false
      t.float :duree
      t.string :commentaire
      t.timestamps
    end
    
  end

  def self.down
    drop_table :interventions
    drop_table :interventionlignes
  end
end


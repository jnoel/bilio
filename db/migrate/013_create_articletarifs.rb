# coding: utf-8

class CreateArticletarifs < ActiveRecord::Migration
  def self.up
    
    create_table :articletarifs do |t|
      t.integer :tarif_id, :null => false
      t.integer :article_id, :null => false
      t.float :pu
      t.float :marge
      t.timestamps
    end
    
  end

  def self.down
    drop_table :articletarifs
  end
end


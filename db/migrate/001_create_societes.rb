# coding: utf-8

class CreateSocietes < ActiveRecord::Migration
  def self.up
   
    create_table :societes do |t|
      t.string :nom, :null => false
      t.text :adresse
      t.integer :capital
      t.string :tel
      t.string :fax
      t.string :mail
      t.string :site
      t.string :banque
      t.string :banque_compte
      t.string :banque_code
      t.string :banque_guichet
      t.string :banque_cle
      t.string :banque_domiciliation
      t.string :banque_iban
      t.string :banque_bic
      t.string :siret
      t.string :rcs
      t.string :naf
      t.string :status
      t.timestamps
    end
    
    Societe.create :nom => "Société Test"
    
  end

  def self.down
    drop_table :societes
  end
end


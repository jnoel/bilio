# coding: utf-8

class CreateArticles < ActiveRecord::Migration
  def self.up
    
    create_table :articles do |t|
      t.string :code, :null => false
      t.string :designation, :null => false
      t.text :description
      t.integer :articlegroupe_id
      t.string :gencode
      t.string :gencode2
      t.text :note
      t.boolean :lot, :default => false
      t.boolean :vente, :default => true
      t.boolean :achat, :default => true
      t.boolean :service, :default => false
      t.integer :colisage, :default => 1
      t.integer :tva_id, :default => 1
      t.boolean :refaire_etiquette, :default => false
      t.integer :tiers_id
      t.integer :rayon_id
      t.date :date_modif
      t.timestamps
    end
    
  end

  def self.down
    drop_table :articles
  end
end


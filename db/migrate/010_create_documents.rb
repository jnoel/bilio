# coding: utf-8

class CreateDocuments < ActiveRecord::Migration
  def self.up
    
    create_table :documents do |t|
      t.string :ref, :null => false
      t.integer :tiers_id, :null => false
      t.integer :tarif_id, :null => false
      t.integer :documenttype_id, :null => false
      t.date :date_document
      t.date :date_livraison
      t.float :montant_ht
      t.float :montant_ttc
      t.integer :num_paiement
      t.string :num_transaction
      t.text :note
      t.text :note_priv
      t.boolean :transfere
      t.string :provenance_ref
      t.integer :utilisateur_id, :null => false
      t.boolean :suppr, :default => false
      t.timestamps
    end
    
  end

  def self.down
    drop_table :documents
  end
end


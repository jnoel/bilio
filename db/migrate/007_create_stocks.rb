# coding: utf-8

class CreateStocks < ActiveRecord::Migration
  def self.up
    
    create_table :stocks do |t|
      t.string :designation, :null => false
      t.boolean :default, :default => false
      t.timestamps
    end
    
    Stock.create :designation => "Dépôt Principal", :default => true
    
  end

  def self.down
    drop_table :stocks
  end
end


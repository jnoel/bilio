# coding: utf-8

class CreateGedcompte < ActiveRecord::Migration
  def self.up
    
    create_table :gedcomptes do |t| 
	  t.integer :compte_id
	  t.string :fichier
	  t.text :note
      t.timestamps
    end
        
  end

  def self.down
	 drop_table :gedcomptes
  end
end


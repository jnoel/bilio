# coding: utf-8

class ChangeTarifs < ActiveRecord::Migration
  def self.up
  	change_table :tarifs do |t|
	  	t.float :marge, :default => 1.0
		end
  end

  def self.down
  	change_table :tarifs do |t|
	 		t.remove :marge
		end
  end
end

# coding: utf-8

class ChangeDocuments3 < ActiveRecord::Migration
  def self.up
  	change_table :documents do |t|
	  t.date :date_echeance
	end
  end

  def self.down
  	change_table :documents do |t|
	  t.remove :date_echeance
	end
  end
end

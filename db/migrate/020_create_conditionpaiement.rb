# coding: utf-8

class CreateConditionpaiement < ActiveRecord::Migration
  def self.up
    
    create_table :conditionpaiements do |t| 
	  t.string :code
	  t.string :designation
	  t.integer :nb_jours, :default => 0
	  t.boolean :fin_mois, :default => false
	  t.boolean :actif, :default => true
      t.timestamps
    end
    
    change_table :documents do |t|
	  t.integer :conditionpaiement_id
    end
    
    Conditionpaiement.create :code => "LIV", :designation => "A la livraison"
    Conditionpaiement.create :code => "FAC", :designation => "A réception de facture"
    Conditionpaiement.create :code => "30J", :designation => "Réglement à 30 jours", :nb_jours => 30
    Conditionpaiement.create :code => "30JF", :designation => "Réglement à 30 jours fin de mois", :nb_jours => 30, :fin_mois => true
    Conditionpaiement.create :code => "60J", :designation => "Réglement à 60 jours", :nb_jours => 60
    Conditionpaiement.create :code => "60JF", :designation => "Réglement à 60 jours fin de mois", :nb_jours => 60, :fin_mois => true
    Conditionpaiement.create :code => "90J", :designation => "Réglement à 90 jours", :nb_jours => 90
    Conditionpaiement.create :code => "90JF", :designation => "Réglement à 90 jours fin de mois", :nb_jours => 90, :fin_mois => true
    Conditionpaiement.create :code => "2X", :designation => "Réglement en 2 fois sans frais", :nb_jours => 30
    Conditionpaiement.create :code => "3X", :designation => "Réglement en 3 fois sans frais", :nb_jours => 60
    
  end

  def self.down
	 drop_table :conditionpaiements
	 change_table :documents do |t|
	  t.remove :conditionpaiement_id
	end
  end
end


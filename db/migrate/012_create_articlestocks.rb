# coding: utf-8

class CreateArticlestocks < ActiveRecord::Migration
  def self.up
    
    create_table :articlestocks do |t|
      t.integer :stock_id, :null => false
      t.integer :article_id, :null => false
      t.string :lot
      t.float :qtite
      t.timestamps
    end
    
  end

  def self.down
    drop_table :articlestocks
  end
end

